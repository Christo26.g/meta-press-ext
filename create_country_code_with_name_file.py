"""
description : Script for parse and create country code with name of the country JSON file 
author : PORTET Marin
date : 06/05/2021
"""
import sys, os, json

FILE_COUNTRY_CODE_WITH_NAME = "country_code_with_name.json"
FOLDER_JSON = "json"

def parse(file):
    data = []
    try :
        path = os.path.realpath(file)
        with open(path, "r") as f:
            lines = f.readlines()
        for line in lines:
            if line.startswith("#") :
                continue
            if line == "":
                continue
            tz = line
            data.append(tz)
    except Exception as ex:
        print(ex)
    return data


def create_country_code_with_name_json(data):
    country_code = {}
    for d in data:
        tmp = d.split("\t")
        key = tmp[0].lower()
        val = tmp[1].replace("\n", "")
        country_code[key] = val
    write_in_json_file(country_code, f"{FOLDER_JSON}/{FILE_COUNTRY_CODE_WITH_NAME}")

def write_in_json_file(data, path):
    try:
        js = json.dumps(data, indent=2)
        with open(path, "w") as file:
            file.write(js)
    except Exception as ex:
        print(ex)

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        raise Exception("Error : Need file in argument")
    data = parse(sys.argv[1])
    create_country_code_with_name_json(data)