"""
description : Script for parse and create timezone JSON file 
author : PORTET Marin
date : 30/04/2021
"""
import sys, os, json, re

FILE_TIMEZONES = "timezones.json"
FILE_ZONE_TAB = "zone.tab.json"
FOLDER_JSON = "json"

def parse(file):
    data = []
    try :
        path = os.path.realpath(file)
        with open(path, "r") as f:
            lines = f.readlines()
        for line in lines:
            if line.startswith("#") :
                continue
            if line == "":
                continue
            tz = line
            data.append(tz)
    except Exception as ex:
        print(ex)
    return data

def create_timezones_json(data):
    timezones = {}
    reg_key = re.compile('^.{2}$')
    reg_val = re.compile("^.*/.*$")
    for d in data:
        tmp = d.split("\t")
        key = [ x.lower() for x in tmp if reg_key.match(x) != None][0]
        val = [ x.replace("\n", "") for x in tmp if reg_val.match(x) != None][0]
        if key in timezones.keys() :
            timezones[key].append(val)
        else:
            timezones[key] = [val]
    write_in_json_file(timezones, f"{FOLDER_JSON}/{FILE_TIMEZONES}")

def create_zone_tab_json(data):
    timezones = []
    reg_val = re.compile("^.*/.*$")
    for d in data:
        tmp = d.split("\t")
        val = [ x.replace("\n", "") for x in tmp if reg_val.match(x) != None][0]
        timezones.append(val)
    write_in_json_file(timezones, f"{FOLDER_JSON}/{FILE_ZONE_TAB}")

def write_in_json_file(data, path):
    try:
        js = json.dumps(data, indent=2)
        with open(path, "w") as file:
            file.write(js)
    except Exception as ex:
        print(ex)

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        raise Exception("Error : Need file in argument")
    data = parse(sys.argv[1])
    create_timezones_json(data)
    create_zone_tab_json(data)
