// name    : setting_page.js
// SPDX-FileCopyrightText: 2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
/* globals browser CodeMirror */
import * as mµ from './mp_utils.js'
import * as µ from './utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'

mµ.set_theme();
/**
 * All the sources customs sources modification and addition related.
 * @namespace new_update_sources
 */

(async () => {
	'use strict'
	//Exemple pour récupérer le timezone avec le country_code d'une source
	//let res = await get_timezone("us")
	//console.log(res)
	const userLang = await mµ.get_wanted_locale()
	await g.xgettext_html()
	/*const mp_i18n = */await g.gettext_html_auto(userLang)
	//
	// custom_src
	//
	var provided_sources_text = await fetch('json/sources.json')
	provided_sources_text = await provided_sources_text.text()
	var custom_src_codemirror
	document.getElementById('provided_sources').textContent = provided_sources_text
	/* var provided_src_codemirror = */ CodeMirror.fromTextArea(
		document.getElementById('provided_sources'), {
			mode: 'application/json',
			// mode: {name:'javascript', json:true},
			indentWithTabs: true,
			screenReaderLabel: 'CodeMirror',
			readOnly: 'nocursor'
		}
	)
	browser.storage.sync.get('custom_src').then(
		load_custom_src, err => {console.error(`Loading custom_src: ${err}`)}
	)
	/**
	 * Load the custom sources in the HTML document.
	 * @memberof load_sources
	 * @param {Object} stored_data The stored customs sources
	 */
	function load_custom_src(stored_data){
		if (typeof(stored_data) === 'object' && typeof(stored_data.custom_src) === 'string' &&
				stored_data.custom_src && stored_data.custom_src !== '{}') {
			document.getElementById('custom_sources').textContent = stored_data.custom_src
			// document.getElementById('reload_hint').style.display = 'inline'
		} else {
			document.getElementById('custom_sources').textContent =
				document.getElementById('default_custom_sources').textContent
		}
		custom_src_codemirror = CodeMirror.fromTextArea(
			document.getElementById('custom_sources'), {
				mode: 'application/json',
				screenReaderLabel: 'CodeMirror',
				indentWithTabs: true,
				extraKeys: {Tab: false} // avoids keyboard trap preserving 'tab-key' navigation
			}
		)
		const STATUS_CURRENT_LINE = document.getElementById('ln_nb')
		const STATUS_CURRENT_COL = document.getElementById('col_nb')
		custom_src_codemirror.on('cursorActivity', () => {
			const cursor = custom_src_codemirror.getCursor()
			STATUS_CURRENT_LINE.textContent = cursor.line + 1
			STATUS_CURRENT_COL.textContent = cursor.ch + 1
		})
	}
	document.getElementById('save_custom_sources').addEventListener('click', () => {
		custom_src_codemirror.save()
		const custom_src_value = document.getElementById('custom_sources').value
		if (custom_src_value !== '')
			try {
				JSON.parse(custom_src_value)
			} catch (exc) {
				alert(`Error parsing user custom source (${exc}).`)
				return
			}
		browser.storage.sync.set({custom_src: custom_src_value})
		document.getElementById('reload_hint').style.display = 'inline'
		document.getElementById('reload_hint').style['font-weight'] = 'bold'
		mµ.exec_in_background('build_src')
	})
	document.getElementById('reset_custom_sources').addEventListener('click', () => {
		browser.storage.sync.set({custom_src: ''})
		mµ.exec_in_background('build_src')
	})

	const cur_querystring = new URL(window.location).searchParams
	const update_src = cur_querystring.get("update_src")
	if (update_src === '1')
		update_favicon_rss()
	/**
	 * Update all the sources favicon with {@link get_favicon} and the headline_rss.
	 * @memberof new_update_sources
	 * @async
	 */
	async function update_favicon_rss() {
		let src = await mµ.exec_in_background('get_prov_src,')
		let raw_src = await mµ.exec_in_background('get_raw_src')
		let format_order = await mµ.exec_in_background('get_test_integrity')
		for (let i of Object.keys(src)) {
			let fav = µ.get_favicon(i)
			if (fav !== '' && fav !== src[i]["favicon_url"])
				raw_src[i]["favicon_url"]= fav
			let rss = µ.get_rss(i)
			if (rss !== '' && rss !== src[i]["headline_rss"])
				raw_src[i]["headline_rss"]= rss

			if (raw_src[i]['headline_rss']) {
				let rss_url = ['/feed', '/rss']
				for (const j of rss_url) {
					if (mµ.test_url(src[i], i + j, "headline_rss", { '200': true }) === 200)
						raw_src[i]["headline_rss"] = i + j
				}
			}
		}
		let json_str = JSON.stringify(mµ.reformat_sources(raw_src, format_order["format_order"]), null, "	")
		µ.upload_file_to_user("sources-2.json", json_str)
	}
})()

