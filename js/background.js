// name	  : background.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
// date   : 2017-06, 2020-10
/* globals browser */
/* exported background_timezoned_date */

//browser.storage.sync.clear()  // in case of schedule search panic, clear browser storage

/**
 * Background sources loading / building.
 * @namespace load_sources
 */

/**
 * All the function for test (format, unit).
 * @namespace test
 */

/**
 * Schedulded search.
 * @namespace sch_search
 */

/**
 * Id list of the search tab opened by the metapress extension  button.
 * @type {Array}
 */
const list_tabs = []

/**
 * Create an tab with browser.tabs.create and stock her id in list_tabs.
 * @function create_tab
 * @param {Object} props tabs.create parameter
 * @returns The new tab create
 */
async function create_tab(props){
	let tab = await browser.tabs.create(props)
	list_tabs.push(tab.id)
	return tab
}
/**
 * Open the welcome.html for new user or for an update.
 * @function announce_updates
 * @param {string} stored_version The current version
 */
async function announce_updates (stored_version) {
	if (typeof(stored_version['version']) === 'undefined') {
		create_tab({'url': '/welcome.html'})
	}
	if (stored_version['version'] === '1.7.1') { /* to avoid scheduled search panic */
		browser.alarms.clearAll()
		browser.storage.sync.set({sch_sea: []})
	}
	let manifest = await fetch('manifest.json')
	manifest = await manifest.json()
	browser.storage.sync.set({version: manifest.version})
	var do_announce_updates = false
	if (do_announce_updates) {
		if (stored_version['version'] != manifest.version)
			create_tab({'url': 'https://www.meta-press.es/category/journal.html'})
	}
}

/**
 * Shedule the new automated search.
 * @function sch_sea_maj_date
 * @memberof sch_search
 * @param {Date} date The date of the ancient search
 * @param {string} frq The frequeencies of the search sch_s_stop sch_s_hourly sch_s_daily
 * sch_s_weekly sch_s_monthly sch_s_quaterly sch_s_half-yearly sch_s_annual
 * @returns The date of execute search.
 */
function sch_sea_maj_date(date, frq) {
	var cur_date = new Date()
	let d = date
	if(cur_date >= date && frq !== 'sch_s_stop') {
		if(frq === 'sch_s_hourly') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+3600000))
			}
		}
		if(frq === 'sch_s_daily') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			d = new Date(d.setHours(date.getHours()))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+86400000))
			}
		}
		if(frq === 'sch_s_weekly') {
			d = cur_date
			d = new Date(d.setMinutes(date.getMinutes()))
			d = new Date(d.setHours(date.getHours()))
			let mod = (7 - cur_date.getDay() + date.getDay())%7
			d = new Date(d.setTime(d.getTime()+(86400000*mod)))
			if(d <= cur_date) {
				d = new Date(d.setTime(d.getTime()+(86400000*7)))
			}
		}
		if(frq === 'sch_s_monthly' || frq === 'sch_s_quaterly' || frq === 'sch_s_half-yearly')
		{
			d = new Date(d.setMonth(cur_date.getMonth()))
			d = new Date(d.setFullYear(cur_date.getFullYear()))
			if(frq === 'sch_s_monthly') {
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 1)}
			}
			if(frq === 'sch_s_quaterly') {
				if(!(d.getMonth() - date.getMonth())%3) {
					let mod = (3 - (d.getMonth() - date.getMonth()))%3
					d = d.setMonth(d.getMonth() + mod)
				}
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 3)}
			}
			if(frq === 'sch_s_half-yearly') {
				if(!(d.getMonth() - date.getMonth())%6) {
					let mod = (6 - (d.getMonth() - date.getMonth()))%6
					d = d.setMonth(d.getMonth() + mod)
				}
				if(d < cur_date) { d = date.setMonth(date.getMonth() + 6)}
			}
			if(d.getMonth() < cur_date.getMonth()) {
				d = new Date(d)
				d = d.setFullYear(cur_date.getFullYear()+1)
			}
		}
		if(frq === 'sch_s_annual') {
			d = date.setFullYear(cur_date.getFullYear())
			if(d < cur_date) { d = date.setFullYear(date.getFullYear() + 1)}
		}
	}
	return new Date(d)
}
/**
 * Load the schedule search from the browser storage.
 * @function init_table_sch_s
 * @memberof sch_search
 * @param {Object} browser_str The browser storage
 * @returns {Array} the list of shedule search
 */
function init_table_sch_s(browser_str) {
	var table_sch_s = []//init value of table_sch_s
	var keys_elt_browser_str = Object.keys(browser_str)//get all keys of object in browser storage
	var entries_elt_browser_str = Object.entries(browser_str)//get all entries in browser storage
	for(let i=0; i<keys_elt_browser_str.length;i++) {// for all keys
		var regex = /sch_sea__/
		if(keys_elt_browser_str[i].match(regex)) {//if keys match with 'sch_sea__'
			let id = keys_elt_browser_str[i].split('__')[1]
			table_sch_s[id] = entries_elt_browser_str[i]//add entries in table_sch_s
		}
	}
	return table_sch_s
}
/**
 * Save existing schedulded search in the browser storage.
 * @function modif_sch_s_storage
 * @memberof sch_search
 * @param {string} id_sch_s The id of shedulded search
 * @param {Object} value_sch_s The new value of the search
 */
function modif_sch_s_storage(id_sch_s, value_sch_s) {
	var modif_sch_s = {}
	modif_sch_s[id_sch_s] = value_sch_s
	browser.storage.sync.set(modif_sch_s)
}
var table_sch_s
browser.storage.sync.get().then(getElt)
/**
 * Load and add the alarms for schedulded search.
 * @function getElt
 * @memberof sch_search
 * @example browser.storage.sync.get().then(getElt)
 * @param {Object} elt The browser storage
 */
function getElt(elt) {
	var stored_sch_sea = init_table_sch_s(elt)
	if(stored_sch_sea) {create_alarm(stored_sch_sea)}
}
/**
 * Launch a search and shedulded a new one if needed.
 * @function launch_sch_s
 * @memberof sch_search
 * @async
 * @param {Object} sch_s The search to launch
 */
async function launch_sch_s(sch_s) {
	sch_s = sch_s.name ? sch_s.name : sch_s
	let local_url = new URL(window.location).origin
	let new_url = new URL(local_url +'/index.html' + sch_s)
	let next_run = new_url.searchParams.get('next_run')
	let run_freq = new_url.searchParams.get('run_freq')
	let index = new_url.searchParams.get('id_sch_s')
	let last_run = new Date().toUTCString()
	next_run = await sch_sea_maj_date(new Date(next_run), run_freq)
	next_run = next_run.toUTCString()
	new_url.searchParams.set('last_run', last_run)
	new_url.searchParams.set('next_run', next_run)
	modif_sch_s_storage(index, new_url.search)
	new_url.searchParams.set('submit', '1')
	//new_url.searchParams.set('last_res', 0)
	create_tab({
		'url': new_url.toString()
	})
	create_alarm([new_url.search])
}
/**
 * Create new alarm for the given(s) shedulded search.
 * @function create_alarm
 * @memberof sch_search
 * @param {(Array|string)} sch_s The schedulded search(s)
 */
function create_alarm(sch_s) {
	table_sch_s = sch_s  // eslint-disable-line no-unused-vars
	for(let sch_sea of sch_s) {
		if(Array.isArray(sch_sea)){sch_sea=sch_sea[1]}
		let local_url = new URL(window.location).origin
		var new_url = new URL(local_url +'/index.html' + sch_sea)
		var next_run = new_url.searchParams.get('next_run')
		var run_freq = new_url.searchParams.get('run_freq')
		var next_run_date = new Date(next_run)
		if(run_freq !== 'sch_s_stop') {
			next_run_date = next_run_date.getTime()
			browser.alarms.create(sch_sea, {when: next_run_date})
			browser.alarms.onAlarm.addListener(launch_sch_s)
		}
	}
	console_debug_alarms(bool_debug)
}

/**
 * Debug flag if true print debug log.
 * @memberof sch_search
 * @constant
 * @type {boolean}
 */
const bool_debug = false
/**
 * Log the alarm in the extension console if display is true.
 * @function console_debug_alarms
 * @async
 * @memberof sch_search
 * @param {boolean} bool display flag
 */
async function console_debug_alarms(bool) {
	if(bool) {
		var alarms
		await browser.alarms.getAll().then(elt => {alarms = elt})
		console.log(alarms)
		for(let i=alarms.length;i--;) {
			let date = new Date(alarms[i].scheduledTime)
			console.log("L'alarme ",i," se lancera le ",date)
		}
	}
}
var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 4, useGrouping: 0})
/* Lié a background_timezoned_date : setting_page.js */
/* Transforme une date en modifiant sa time zone
	nav_to_tz : booléen pour savoir on ajoute la timezone ou si on l'enlève
*/
/**
 * Return the date in the navigator timezone if nav_to_tz = false
 * if not return the from the given timezone.
 * @function timezoned_date_promise
 * @param {string} dt_str the date
 * @param {string} tz the timezone 
 * @param {boolean} nav_to_tz The direction of the conversion
 * @returns {Date} the date converted
 */
function timezoned_date_promise (dt_str, tz='UTC', nav_to_tz) {
	var dt = dt_str ? new Date(dt_str) : new Date()
	if (isNaN(dt)) throw new Error(`${dt_str} is an invalid Date`)
	if (tz === 'UTC' || tz === 'GMT') return dt
	let parsed_tz = parseInt(tz,10)
	if(isNaN(parsed_tz)) {
		var dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)
		var dt_UTC = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
		var dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		var int_offset, date_tz
		if(nav_to_tz) {
			date_tz = dt_UTC
			int_offset = parseInt(
				-(dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
		} else {
			date_tz = dt_orig
			int_offset = parseInt((dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100))
		}
		var tz_offset = intlNum.format(int_offset)
		var tz_repr = int_offset >= 0 ? '+'+tz_offset : tz_offset
		return new Date(date_tz/*_UTC*/.toISOString().replace(/\.\d{3}Z/, tz_repr))
	}
	else { return new Date(dt) }
}
/**
 * Extends extended_src for n.
 * @function extend_source
 * @memberof load_sources
 * @param {Object} n The source that inherits 
 * @param {Object} extended_src The mother source
 * @returns {Object} The n source builds
 */
function extend_source(n, extended_src) {
	extended_src = JSON.parse(JSON.stringify(extended_src))
	let extended_tags = Object.assign(extended_src.tags || {}, n.tags)
	Object.assign(extended_src, n)
	n = extended_src
	n.tags = extended_tags
	return n
}

/**
 * Reload all the open search tab for update the sources object
 * @function reload_src
 * @memberof load_sources
 * @returns The new sources object update
 */
function reload_src() {
	let src_objects = build_src()
	for(let i = list_tabs.length; i--;)
		browser.tabs.reload(list_tabs[i])
	return src_objects
}

/**
 * Return the file to json sources without build.
 * @function get_raw_src
 * @memberof load_sources
 * @returns {Object} The raw sources
 */
async function get_raw_src() {
	src = await fetch('json/sources.json')
	src = await src.json()
	return src
}

/**
 * Load and build the test integrity files.
 * @function build_test_integrity
 * @memberof test
 */
async function build_test_integrity() {
	test_integrity = await fetch('json/test_integrity.json')
	test_integrity = await test_integrity.json()
	// xml_src edge case
	let xml_src_ = test_integrity["xml_type"]
	let test = xml_src_["test"]
	const index = test.indexOf("oneofvalues");
	if (index > -1) {
		test.splice(index, 1);
	}
	test.push("key")
	xml_src_["test"] = test
	xml_src_["test_object"] = XML_SRC
	// class list
	test_integrity["tags"] = {}
	for (let k of Object.keys(test_integrity)) {
		let attr = test_integrity[k]
		if (attr["test_re_url"]) {
			if (!attr["test"])
				attr["test"] = []
			attr["test"].push("re")
			attr["test_re"] = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&\\/\\/=]*)"
		}
		if (attr["url_code"]) {
			let val = attr["url_code"]
			attr["url_code"] = {}
			if (typeof(val) === "number")
			val = [val]
			for (const i of val) {
				attr["url_code"][i] = true
			}
		}
		if (attr["conflicts"]) {
			for (let i of attr["conflicts"]) {
				if (!test_integrity[i])
					test_integrity[i] = {}
				if (!test_integrity[i]["conflicts"])
					test_integrity[i]["conflicts"] = []
				test_integrity[i]["conflicts"].push(k)
			}
		}
		if (attr["test_object_file"]) {
			let obj = await fetch(attr["test_object_file"]);
			obj = await obj.json()
			attr["test_object"] = obj
		}
		if (attr["test_oneofvalues_file"]) {
			// get
			let obj = await fetch(attr["test_oneofvalues_file"]);
			obj = await obj.json()
			attr["test_object"] = {}
			// remove oneofvalues
			let test = attr["test"]
			const index = test.indexOf("oneofvalues");
			if (index > -1) {
				test.splice(index, 1);
			}
			test.push("key")
			attr["test"] = test
			// create object
			for (const i of obj) {
				attr["test_object"][i] = true
			}
		}
	}
	test_integrity
}

/**
 * Return a copy of test integrity object.
 * @function get_test_integrity
 * @memberof test
 * @returns {Object} The test_integrity build object
 */
function get_test_integrity() {
	let new_integ = {}
	for (let t of Object.keys(test_integrity)) {
		new_integ[t] = test_integrity[t]
	}
	return new_integ
}
const DEFAULT_SEARCH_TERMS = {
	'one word': ['Esperanto','Biden', 'Quadrature du Net'],
	'many words': ['Quadrature du net','Watergate','Gilets Jaunes','Jaunes Gilets'],
	'approx': ['Esperanto','Quadrature du Net']
}
/**
 * Load and build the sources object.
 * @function build_src
 * @memberof load_sources
 * @returns {Object} The sources object
 */
async function build_src() {
	build_sources = await fetch('json/sources.json')
	build_sources = await build_sources.json()
	stored_data = await browser.storage.sync.get('custom_src')
	// ex load_prov_src
	sources_keys = Object.keys(build_sources)
	let deleted_src = {}
	if (typeof(stored_data) === 'object' && typeof(stored_data.custom_src) === 'string' &&
		stored_data.custom_src.length > 0) {
		try {
			let custom_src_obj = JSON.parse(stored_data.custom_src)
			for (const i of Object.keys(custom_src_obj)) {
				if (build_sources[i]) {
					custom_src_obj[i]["overwrite"] = true
				}
				custom_src_obj[i]["custom"] = true
			}
			build_sources = Object.assign(build_sources, custom_src_obj)
		} catch (exc) { alert(`Error parsing user custom source (${exc}).`) }
	}
	/* * * current_source_selection defined * * */
	let collections = await get_stored_collections()
	collections.provided = {}
	for (let k of sources_keys) {
		// if (!k.startsWith('http')) continue  // was used to avoid false RSS and ATOM
		let n = build_sources[k]
		if (typeof(n) === 'undefined') continue
		if (typeof(n.extends) !== 'undefined') { // Manage sources to extend
			let extended_src = typeof(build_sources[n.extends]) !== 'undefined' ? 
				build_sources[n.extends] : deleted_src[n.extends]
			if (typeof(extended_src) != 'undefined') {
				n = extend_source(n, extended_src)
			} else {
				console.error(`${n.headline_url} has a extends broken source ${n.extends}. 
				Too bad... :(`)
			}
		}
		if (n.tags.tech.includes('broken')) {
			deleted_src[k] = build_sources[k]
			delete build_sources[k]
			sources_keys = Object.keys(build_sources)
			continue
		}
		if (n.tags.collection) {
			let v = n.tags.collection
			if (v && typeof(v) === 'object' && typeof(v.length) !== 'undefined')
				for (const i of n.tags.collection) {
					if (!collections.provided[i])
						collections.provided[i] = []
					collections.provided[i].push(k)
				}
			else
				if (!collections.provided[v])
					collections.provided[v] = []
				collections.provided[v].push(k)
		}
		let n_type = n.type
		if ("XML" === n_type) {
			n = extend_source(n, XML_SRC[n.xml_type])
			// n.tags.tech.push('RSS')
		}
		let n_tech = n.tags.tech
		if (n.search_url.startsWith('https')) // Dynamically add the HTTPS src tag
			if (!n_tech.includes('HTTPS'))
				n_tech.push('HTTPS')
		let type_src = n.tags.src_type
		if (n.r_img || n.r_img_src) { // Dynamically add the Illustrated src tag
			if (!type_src.includes('Illustrated'))
				if(typeof(type_src) === 'object')
					type_src.push('Illustrated')
				else
					type_src = [type_src, 'Illustrated']
		}
		build_sources[k] = n
		let src_positive_sch_terms = n.positive_test_search_term
		let src_tested_query
		if (typeof(src_positive_sch_terms) !== 'undefined') {
			if (Array.isArray(src_positive_sch_terms)) {
				src_tested_query = src_positive_sch_terms
				let default_sch_terms = get_default_terms(n_tech) //Marin
				let max_length = Math.max(src_tested_query.length, default_sch_terms.length)
				for (let s = 0; s < max_length; s++) {
					if (typeof (src_tested_query[s]) === "undefined" || src_tested_query[s] === ''){
						src_tested_query[s] = default_sch_terms[s]
					}
				}
			}
		} else
			src_tested_query = get_default_terms(n_tech)
		n.positive_test_search_term = src_tested_query
	}
	browser.storage.sync.set({collections: collections})
	return build_sources
}
function get_default_terms(src_tags_tech) {
	if(src_tags_tech.includes('many words')) {
		return DEFAULT_SEARCH_TERMS['many words']
	} else if (src_tags_tech.includes('one word')) {
		return DEFAULT_SEARCH_TERMS['one word']
	} else {
		return DEFAULT_SEARCH_TERMS['approx']
	}
}
/**
 * Create and returns an copy of sources Object or just the src_name part of sources
 * @function get_prov_src
 * @memberof load_sources
 * @param {string} src_name The name of the source to returns
 * @returns {Object} The sources object
 */
function get_prov_src(src_names) {  // eslint-disable-line no-unused-vars
	var prv = {}
	if(src_names === "") {
		for (let t of Object.keys(build_sources)) {
			prv[t] = build_sources[t]
		}
	} else {
		if(typeof(src_names) === 'string')
			src_names = [src_names]
		for (let t of Object.keys(build_sources)) {
			for(let src_name of src_names){
				if(build_sources[t].tags.name === src_name) {
					prv[t] = build_sources[t]
					//let ext = prv[t].extends
					//if(ext) {prv[ext] = provided_sources[ext]}
				}
			}
		}
	}
	return prv
}

/**
 * Get the browser data for entry 'collections'.
 * @function
 * @memberof load_sources
 * @returns The collection of source for search
 */
async function get_stored_collections() {
	let getting = browser.storage.sync.get()
	let browser_data
	let default_val = {
		"provided" : {},
		"created": {}
	}
	await getting.then(elt=>{browser_data = elt['collections']})
	if(typeof(browser_data) === 'undefined' || browser_data === null
		|| browser_data === '') {
		let obj_str = {}
		obj_str['collections'] = default_val
		browser.storage.sync.set(obj_str)
		return default_val
	} else {
		return browser_data
	}
}

/**
 * Return a copy of the {@link XML_SRC} object.
 * @function get_XML_SRC
 * @memberof load_sources
 * @async
 * @returns {Object} A copy of XML_SRC
 */
async function get_XML_SRC() {
	result = await fetch('json/test_integrity.json')
	result = result = await result.json()
	return result['rss']
}

/**
 * Load the old test source results in {@link test_meta-press.es.json}.
 * @function load_test_src_res
 * @memberof test
 * @async
 */
async function load_test_src_res(){
	test_source_results = await fetch('json/test_meta-press.es.json')
	test_source_results = await test_source_results.json()
}

/**
 * Return a copy of test sources for the given name.
 * @function get_test_src
 * @memberof test
 * @param {string} src_name The name of the test source
 * @returns {Object} the specific test source
 */
function get_test_src(src_name) {
	var test_src = {}
	test_src = test_source_results[src_name]
	return test_src
}
/**
 * Execute in function call with a message.
 * @param {Object} data name of the function and her params in array
 * @param {Object} sender The sender information
 * @returns The result of the function or false
 */
async function handle_function_call(data, sender) {
	// console.log(`request : ${data.function} ${data.params}, sender ${sender.tab.id}`)
	if (typeof(data.function) === 'string' && window[data.function]) {
		let res = window[data.function].apply(null, data.params)
		if (typeof(res) !== "undefined" && typeof(res.then) !== "undefined")
			res = await res
		return res
	}
	return false
}
// the message listener
browser.runtime.onMessage.addListener(handle_function_call);

/* Announces updates : opens the official link when new version is installed */
browser.storage.sync.get('version').then(announce_updates,
	() => {console.error('Could not get "version" from browser storage')}
)
build_src()
load_test_src_res()
/**
 * The var who contain the builded sources user and app given.
 * @memberof load_sources
 * @type {Object}
 */
var build_sources
/**
 * The var who contain the treatment description for RSS format.
 * @memberof load_sources
 * @type {Object}
 */
var XML_SRC
/**
 * The var who contain the precedent test sources result in json.
 * @memberof test
 * @type {Object}
 */
var test_source_results
/**
 * The var who contain the description of test integrity (format).
 * @memberof test
 * @type {Object}
 */
var test_integrity

(async () => {
	XML_SRC = await get_XML_SRC()
	var start_loading_sources = new Date()
	build_sources = await build_src()
	build_test_integrity()
	console.log(`json/sources.json loading time : ${new Date() - start_loading_sources} ms`)
	browser.browserAction.onClicked.addListener(() => create_tab({'url': '/index.html'}))
})()
