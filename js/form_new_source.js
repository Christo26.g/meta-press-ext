import * as mµ from './mp_utils.js'
import * as µ from './utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'
import * as rjson from './deps/renderjson.js'

var test_integrity = await fetch('json/test_integrity.json')
test_integrity = test_integrity.json()
const cur_querystring = new URL(window.location).searchParams
var name_src = cur_querystring.get('src')
var select_multiple_opt = {
	removeItemButton: true,
	resetScrollPosition: false,
	placeholder: true,
	placeholderValue: '*',
	duplicateItemsAllowed: false,
	searchResultLimit: 8
}
var sources = await mµ.exec_in_background('build_src')
var default_val = {
	key : "",
	tags: {
		name : ['', false],
		lang : ['', false],
		country : ['', false],
		tz : ['', false],
		themes : [[], false],
		tech : [[], false],
		src_type : [[], false],
		res_type : [[], false]
	},
	headline_url : ['', false],
	favicon_url : ['', false],
	search_url: ['', false],
	positive_test_search_term: [[''], false],
	search_url_web: ['', false],
	body: ['', false],
	results: ['', false],
	r_h1: ['', false, null, null, null],
	r_url: ['', false, null, null, null],
	r_dt: ['', false, [], null, null],
	r_txt: ['', false, null, null, null],
	r_img: ['', false, null, null, null],
	r_img_src: ['', true, null, null, null],
	r_img_alt: ['', true, null, null, null],
	r_by: ['', false, null, null, null],
	res_nb: ['', false, null, null, null],
}
let element = document.getElementById('tags_name')
let s = [{value: '', label: 'Choose a source to modify'}]
for(let t of Object.keys(sources)){
	s.push({value: sources[t].tags.name, label: `${t} - ${sources[t].tags.name}`})
}
let choices_src = new Choices(element,
	Object.assign({choices: s}, select_multiple_opt))
//sources_objs = {src : sources_objs[src]}
element.addEventListener('change', async() => {
	let src = await µ.get_select_value(element)
	let url = new URL(window.location)
	url.searchParams.set('src', src)
	location.replace(url)
})
let sources_objs
if(name_src && name_src !== ''){
	choices_src.setChoiceByValue(name_src)
	sources_objs = await mµ.exec_in_background('get_prov_src', [name_src])
	let key = Object.keys(sources_objs)[0]
	if(typeof(sources_objs[key]) !== 'undefined'){
		default_val.key = key
		for(let k of Object.keys(sources_objs[key])){
			if(k !== 'tags'){
				if(sources_objs[key][k]){
					if(default_val[k]){
						default_val[k][0] = sources_objs[key][k]
						default_val[k][1] = true
					}
					if(k.includes('_re')){
						let i = k.split('_re')[0]
						default_val[i][2] = sources_objs[key][k]
					}
					if(k.includes('_attr')){
						let i = k.split('_attr')[0]
						default_val[i][3] = sources_objs[key][k]
					}
					if(k.includes('_xpath')){
						let i = k.split('_xpath')[0]
						default_val[i][4] = sources_objs[key][k]
					}
					if(k.includes('_fmt')){
						let i = k.split('_fmt')[0]
						default_val[i][2] = default_val[i][2].push(sources_objs[key][k])
					}
				}
			} else {
				for(let k_tags of Object.keys(sources_objs[key][k])){
					if(sources_objs[key][k][k_tags]){
						if(default_val[k][k_tags]){
							if(!µ.is_array(sources_objs[key][k][k_tags]) && µ.is_array(default_val[k][k_tags][0]))
								default_val[k][k_tags][0] = [sources_objs[key][k][k_tags]]
							else
								default_val[k][k_tags][0] = sources_objs[key][k][k_tags]
							default_val[k][k_tags][1] = true
						}
					}
				}
			}
		}
	}
	if(µ.is_array(default_val.positive_test_search_term)){
		default_val.positive_test_search_term[0] = default_val.positive_test_search_term[0][0]
	}
	if(default_val.search_url[0].includes('{}')){
		let term = default_val.positive_test_search_term[0].replace(new RegExp(' ', 'g'),'+')
		default_val.search_url[0] = default_val.search_url[0].replace('{}', term).replace('{#}', 10).replace('{+}', default_val.positive_test_search_term[0].replace(' ', '+'))
	}
}
var form_default = document.getElementById('form_new_source').outerHTML;
(async () => {
	'use strict'
	const userLang = await mµ.get_wanted_locale()
	await g.xgettext_html()
	const mp_i18n = await g.gettext_html_auto(userLang)	
    // Formulaire 
    var fns_start, fns_timezone, fns_selector, fns_tags, fns_global
	reset_fns()
	await form_init()
	document.getElementById('reset_form').addEventListener('click', async() => {
		document.getElementById('form_new_source').outerHTML = form_default
		if(default_val.key != ''){
			let url = new URL(window.location)
			url = url.origin + url.location
			location.replace(url)
		}
		reset_fns()
		form_display('part_start_form_id', true)
		form_display('part_timezone_id', false)
		form_display('part_selector_id', false)
		form_display('part_tags_id', false)
		await form_init()
		fns_start.doc.fieldset.classList.remove('form_fieldset_hide')
	})
	function reset_fns(){
		fns_start = µ.copy_of_object(default_fns_start)
		fns_timezone = µ.copy_of_object(default_fns_timezone)
		fns_selector = µ.copy_of_object(default_fns_selector)
		fns_tags = µ.copy_of_object(default_fns_tags)
		part_selector_reset()
		/*if(timezone_choices!==null)
			fns_timezone.choices = timezone_choices
		if(tags_choices!==null)
			tags_choices = tags_choices*/
		form_display_gauge_progress_bar(0)
		fns_tags.text.MSG_TEXT_TITLE_TECH_SEARCH_SPEED = (time) =>  {
			return `We have determined the speed average for this source is ${time} secondes.\nAccording to you this time is...` 
		}
	}
	//form_handler()
    //Construction du formulaire
    /**
     * Init the fns_global object and start a dynamic form.
     */
	async function form_init() {
		fns_global = {
			icons: {
				triangular : "&#8227;",
				question: "&#63;",
				cross: "&#215;",
				check: "&#10003;",
				pen : "&#9998;"
			},
			test_integrity : null,
			month_nb_json: null,
			request_raw: null,
			virtual_doc: null,
			source_json : null,
			lang_code_json : null,
			country_code_json : null,
			timezone_json : null,
			zone_timezone_json : null,
			new_source_user : {},
			new_source_tags_user : {},
			current_step: 1,
			percent : 0,
			load : false,
			fns_parts: [
				fns_start,
				fns_timezone,
				fns_selector,
				fns_tags
			]
		}
		if(default_val.key !== ''){
			fns_global.new_source_user = sources[default_val.key]
			fns_global.new_source_tags_user = sources[default_val.key].tags
		}
		fns_global.test_integrity = await mµ.exec_in_background('get_test_integrity')
		fns_global.source_json = await mµ.exec_in_background('get_raw_src')
		fns_global.lang_code_json = await fetch('json/lang_names.json')
		fns_global.lang_code_json = await fns_global.lang_code_json.json()
		fns_global.country_code_json = await fetch('json/country_code_with_name.json')
		fns_global.country_code_json = await fns_global.country_code_json.json()
		fns_global.timezone_json = await fetch("json/timezones.json")
		fns_global.timezone_json = await fns_global.timezone_json.json()
		fns_global.zone_timezone_json = await fetch("json/timezones.json")
		fns_global.zone_timezone_json = await fns_global.zone_timezone_json.json()
		fns_global.month_nb_json = await fetch('js/month_nb/month_nb.json')
		fns_global.month_nb_json = await fns_global.month_nb_json.json()
		part_start_form_init()
	}
    /**
     * Add a value to property in object.
     * @param {object} obj the object.
     * @param {string} key the name of propety. If doesn't exist, is created.
     * @param {*} val the value to add.
     */
    function add_property(obj, key, val){
        obj[`${key}`]= val
    }
    /**
     * Remove a property to object.
     * @param {object} obj the object.
     * @param {string} key the name of property to remove.
     */
    function remove_property(obj, key){
        if( typeof(obj[`${key}`]) !== "undefined")
            delete obj[`${key}`]
    }
    /**
     * Add CSS class a node.
     * @param {string} id id of the node in DOM.
     * @param {string} class_name name of a class you want add.
     */
    function form_addclass(id, class_name) {
        try {
            let el = form_get(`${id}`)
            if(!el.classList.contains(class_name))
                el.classList.add(class_name)
        } catch(err) {
            console.error(err)
        }
    }
    /**
     * Remove CSS class a node.
     * @param {string} id id of the node in DOM.
     * @param {string} class_name name of a class you want remove.
     */
    function form_removeclass(id, class_name) {
        try {
            let el = form_get(`${id}`)
            el.classList.remove(class_name)
        } catch(err) {
            console.error(err)
        }
    }
    /**
     * Search a nodes by a class name and remove this class for all
     * nodes find.
     * @param {string} class_name name of a class you want remove.
     */
    function form_search_and_removeclass(class_name) {
        try {
            let els = document.getElementsByClassName(`${class_name}`)
            for(let el of els)
                form_removeclass(el.id, class_name)
        } catch(err) {

        }
    }
    /**
     * Insert inner node a text element.
     * @param {string} id id of the node.
     * @param {string} msg text to insert inner a node.
     */
    function form_msg(id, msg) {
        try {
            let el = form_get(`${id}`)
            el.innerHTML = msg
        } catch(err) {
            console.error(err)
        }
    }
    /**
     * A toggle to show a tips.
     * @param {HTMLElement} display button node of tips text.
     * @param {HTMLElement} tips text node.
     * @param {string} msg text of the tips.
     */
    function form_tips(display, tips, msg) {
        form_msg(tips.id, msg)
        if(tips.style.display === "none" || tips.style.display === "") {
            form_msg(display.id, `${fns_global.icons.cross}`)
            form_display(tips.id, true)
        } else {
            form_msg(display.id,  `${fns_global.icons.question}`)
            form_display(tips.id, false)
        }
    }
    /**
     * Show or hide a node. 
     * @param {string} id id of the node.
     * @param {boolean} show true to render visible and false to render invisible a node.
     * @param {string} type type of display (by default "inline").
     */
    function form_display(id, show, type="inline"){
        try {
            let el = form_get(`${id}`)
            if(show) {
                el.style.display = type
            } else {
                el.style.display = "none"
            }
        } catch(err) {
            console.error(err)
        }
    }
    /**
     * Know if a node exist in the DOM.
     * @param {string} id id of the node.
     * @returns {boolean} true if exist and false is not.
     */
    function form_is_exist(id) {
        try {
            let el = document.getElementById(`${id}`)
            if(el !== null)
                return true
            return false
        } catch(err) {
            return false
        }
    }
    /**
     * Get a node and return them.
     * Also if node is not find a error is thrown.
     * @param {string} id id of the node.
     * @returns {HTMLElement} the node to found.
     */
    function form_get(id) {
        try {
            let el = document.getElementById(id)
            if(el === null)
                throw new Error(`Element with id "${id}" not found.`)
            return el
        } catch(err) {
            console.error(err) // Prints also the call trace 
        }
    }
    /**
     * Add a value to attribut in a node.
     * Also you have a possibility to trigger a event but is optionnal.
     * @param {string} id id of the node.
     * @param {*} value value to set.
     * @param {string} key attribut of the node.
     * @param {string} event name of the event. If not defined, no event is called.
     */
    function form_set(id, value, key, event=undefined) {
        try {
            let el = form_get(`${id}`)
            el[`${key}`] = value
            if(typeof(event) !== "undefined")
                el.dispatchEvent(new Event(`${event}`));
        } catch(err) {
            console.error(err)
        }
    }
    /**
     * Process to check all required input are valid for a part of dynamic form.
     * @param {object} fns_part fns object link to part of dynamic form. 
     * @param {string | HTMLElement} btn_next id or node of button for the next part.
     */
    function form_part_validator(fns_part, btn_next) {
        let valid = true
        for(let d of Object.values(fns_part.data)) {
            if(d.type === "required"){
                //console.log(d, d.test_passed)
                valid = valid && d.test_passed
            }
        }
        fns_part.completed = valid
        form_display(typeof(btn_next) === "string" ? btn_next : btn_next.id, fns_part.completed ? true : false)
        form_update_progress_bar()
    }
    /**
     * Get a input data of a form part and return them.
     * @param {object} fns_part a object "fns_" of form part.
     * @param {string} input name of the input form part.
     * @returns {object} return a data object.
     */
    function form_part_get_data(fns_part, input) {
        try {
            let data = fns_part.data[`${input}`]
            if(typeof(data) === "undefined")
                throw new Error(`Data with key "${input}" doesn't exist.`)
            return data
        } catch(err) {
            console.error(err)
        }
    }
    /**
     * Start a process when a error is throw for a input in dynamic form.
     * @param {string} id_input id of the input node.
     * @param {string} id_err id of the node where you show text error.
     * @param {string} msg the text error.
     * @param {string} required if it is true the class add to input is the class for required input. Else it is false is the class for warning input
     */
    function form_input_err(id_input, id_err, msg, required=true) {
        form_addclass(id_input, required ? "form_input_required" : "form_input_warning")
        form_msg(id_err, msg)
    }
    /**
     * Start a process for update a progress bar.
     */
    function form_update_progress_bar() {
        fns_global.percent = form_calcul_gauge_progress_bar()
        form_display_gauge_progress_bar(fns_global.percent)
    }
    /**
     * Show a gauge in the HTML.
     * @param {number} gauge in percent for a gauge
     */
    function form_display_gauge_progress_bar(gauge) {
        document.getElementById("progress_bar_gauge").style.width = gauge + "%"
        gauge == 0 ? document.getElementById("progress_bar_text").textContent = "" 
        : document.getElementById("progress_bar_text").textContent = gauge + "%"
    }
    /**
     * Calcul a gauge.
     * @returns {number} in percent for a gauge
     */
    function form_calcul_gauge_progress_bar() {
        let gauge = 0
        for(let fns_part of Object.values(fns_global.fns_parts)) {
            for(let d of Object.values(fns_part.data)) {
                if(d.type === "required")
                    gauge += d.test_passed ? ((1 / fns_global.fns_parts.length) * 100) / form_count_required_input(fns_part) : 0
            }
        }
        return Math.ceil(gauge)
    }
    /**
     * Count a number of input is required in fns_part object.
     * It is used for calcul a gauge bar.
     * @see {@link form_calcul_gauge_progress_bar}  
     * @param {object} fns_part a fns_part object for dynamic form.
     * @returns {number} number of input is required.
     */
    function form_count_required_input(fns_part) {
        let count = 0
        for(let d of Object.values(fns_part.data)) {
            if(d.type === "required")
                count += 1
        }
        return count
    }
    /**
     * Create a toggle event for a form part.
     * @param {object} fns_part fns object corresponding to a form part. 
     */
    function form_toggle_event(fns_part) {
        fns_part.doc.legend.onclick = (evt) => {
            form_toggle(fns_part.doc.fieldset)
        }
    }
    /**
     * Show or hide a part of dynamic form.
     * @param {HTMLElement} fieldset a fieldset node.
     */
    function form_toggle(fieldset) {
        let icon = fieldset.querySelector("legend > span")
        icon.classList.toggle("form_icon_fieldset_show")
        icon.classList.toggle("form_icon_fieldset_hide")
        fieldset.classList.toggle("form_fieldset_hide")
    }
    /**
     * @description Build a template with parameters and return a object with some information about the built template.
     * 
     * The object returned contains this information.
     * @property {array} nodes list of nodes builds.
     * @property {number} nb_nodes number of nodes builds.
     * @property {HTMLElement} dom reference to default root div.
     * @property {array} params list of params you have give in argument of this function.
     * 
     * Node is created with parameters. 
     * All nodes created have a root node div as a parent by default. 
     * But you can change that with the "parent" property.
     * 
     * @example
     *      let template = [
     *          {node : "div", id:"container_form_id" classList : ["container"]},
     *              {node : "p", id:"question_id", classList: ["question"], textContent : "It is a question", parent : 0},
     *              {node : "div", id:"container_btn_id" classList : ["container"], parent : 0},
     *                  {node : "button", id:"btn_ok", classList : ["btn"], textContent : "Ok !", parent: 2},
     *                  {node : "button", id:"btn_no", classList : ["btn"], textContent : "No...", parent: 2}
     *      ]
     *      let builder = form_builder(template)
     *      document.getElementById("insert_here").innerHTML = builder.dom.firstChild
     * 
     * @description The node "container_form_id" have a parent the root node div by default. 
     * The nodes "question_id" and "container_btn_id" have a parent the node "container_form_id".
     * The nodes "btn_ok" and "btn_no" have a parent the node "container_btn_id".
     * 
     * In HTML that give :
     * @example
     *      <div id="container_form_id" classList="container">
     *          <p id="question_id"  classList="question">It is a question</p>
     *          <div id="container_btn_id" classList="container">
     *              <button id="btn_ok" classList="btn">Ok !</button>
     *              <button id="btn_no" classList="btn">No...</button>
     *          </div>
     *      </div>
     * 
     * @param {array} params List of parameters for build template.
     * @param {array} params[].node The tag of node ("div", "a", "p", etc...). This property is required.
     * @param {array} params[].parent Index of parent in the list params. Index start in 0. If it is not defined the node have a root div as parent by default.
     * @param {array} params[].condition Condition boolean to created node.
     * @param {array} params[].all_other_HTML_attributs All others HTML attributes are supported. 
     * @returns {object} object with some information about the template build.
     */
    function form_builder(params) {
        let root = document.createElement("div")
        let black_list_key = ["node", "parent", "condition"]
        let nodes = []
        for(let param of params) {
            if(typeof(param.node) === "undefined")
                throw new Exception("'node' parameter is not defined.")
            if(typeof(param.condition) !== "undefined" && !param.condition) 
                continue
						if(document.getElementById(param.id) !== null) {
							document.getElementById(param.id).remove()
						}
            let node = document.createElement(param.node)
            //console.log(param)
            for( let [k, v] of  Object.entries(param)){
                if(black_list_key.includes(k))
                    continue
                try {
                    if(k === "classList")
                        v.forEach( (cls) => { node.classList.add(cls) })
                    else if(k === "textContent")
                        node.textContent = v
                    else if(k === "innerHTML")
                        node.innerHTML = v
                    else
                        node.setAttribute(k, v)
                } catch(err) {
                    console.error(err)
                }
            }
            nodes.push(node)
        }
        let index = 0
        for(let param of params) {
            if(typeof(param.condition) === "undefined" || param.condition) {
                if(typeof(param.parent) === "undefined") {
                    root.appendChild(nodes[index])
                } else {
                    let parent = nodes[param.parent]
                    if(typeof(parent) === "undefined")
                        root.appendChild(nodes[index])
                    else
                        parent.appendChild(nodes[index])
                }
                index += 1
            }
        }
        let obj = {
            nodes: nodes,
            nb_nodes: nodes.length,
            dom : root,
            params : params
        }
        return obj
    }
    
    /**
     * Insert a node in the DOM.
     * @param {HTMLElement} el node to insert in DOM.
     * @param {HTMLElement} parent node parent.
     * @param {HTMLElement} ref child of parent to insert before the element. If is null the element is append.
     */
    function form_insert(el, parent, ref=null) {
        try {
            ref === null ? parent.appendChild(el) : parent.insertBefore(el, ref)
        } catch (err) {
            console.error(err)
        }
    }
    /**
     * Remove a node in the DOM.
     * @param {string} id id of the node to remove. 
     */
    function form_remove(id) { 
        try {
            let el = document.getElementById(id)
            if(el !== null)
                el.parentNode.removeChild(el)
        } catch (err) {
            console.error(err)
        }
    }
    /**
     * Init the start part of dynamic form.
     */
    function part_start_form_init() {
    	//console.log('part_start_form_init')
        fns_start.doc.fieldset = form_get("part_start_form_id")
        fns_start.doc.legend = form_get("legend_part_start_form")
        fns_start.doc.btn_next = form_get("part_start_form_btn_next")
        form_msg("legend_part_start_form_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_start_form_title", `${fns_start.title}`)
        part_start_form_builder()
        form_display(fns_start.doc.fieldset.id, true, "block")
        part_start_form_event()
    }
    /**
     * Build a template for a start part of dynamic form.
     */
    function part_start_form_builder() {
        let temp_name = [
            {node: "div", id: "name_id"},
                {node: "div", classList:["input_container"], parent:0},
                    {node: "input", id: "name_input", classList: ["form_input_text"], 
                    placeholder: "Name of the new source...", title: "Ex : New Source", type:"text", 
                    value:  fns_start.data.tags_name.value, parent:1},
                    {node: "text", textContent: " "},
                    {node: "p", id: "name_tips", classList: ["form_input_tips"], parent:1},
                    {node: "text", textContent: " "},
                    {node : "button", id:"name_display_tips", classList:["a", "btn"], innerHTML: `${fns_global.icons.question}`, parent:1},
                {node: "p", id:"name_err", classList: ["form_input_error"], parent:0}
        ]
        let temp_headline = [
            {node: "div", id: "headline_url_id"},
                {node: "div", classList:["input_container"], parent:0},
                    {node: "input", id: "headline_url_input", classList: ["form_input_text"],
                    placeholder: "Adress (https://...)", title: "Ex : https://www.custom-source.eu", 
                    type:"text", value:  fns_start.data.headline.value, parent:1},
                    {node: "text", textContent: " "},
                    {node: "p", id: "headline_url_tips", classList: ["form_input_tips"], parent:1},
                    {node: "text", textContent: " "},
                    {node : "button", id:"headline_url_display_tips", classList:["a", "btn"], innerHTML: `${fns_global.icons.question}`, parent:1},
                {node: "p", id:"headline_url_err", classList: ["form_input_error"], parent:0}
        ]
        let temp_favicon = [
            {node: "div", id: "favicon_url_id"},
                {node: "div", classList:["input_container"], parent:0},
                    {node: "input", id: "favicon_url_input", classList: ["form_input_text", "form_warning"], 
                    placeholder: "Write a url of favicon (optionnal)...", title: "Ex : https://www.custom-source.eu/custom/favicon.png", 
                    type:"text", value: fns_start.data.favicon.value, parent:1},
                    {node: "text", textContent: " "},
                    {node: "img", id: "favicon_url_img", classList: ["form_favicon"], style:"display: none;", parent:1},
                    {node: "text", textContent: " "},
                    {node: "p", id: "favicon_url_tips", classList: ["form_input_tips"], parent:1},
                    {node: "text", textContent: " "},
                    {node : "button", id:"favicon_url_display_tips", classList:["a", "btn"], innerHTML: `${fns_global.icons.question}`, parent:1},
                {node: "p", id:"favicon_url_err", classList: ["form_input_error"], parent:0}
        ]
        let b_name = form_builder(temp_name)
        let b_headline = form_builder(temp_headline)
        let b_favicon = form_builder(temp_favicon)
        form_insert(b_name.dom.firstChild, fns_start.doc.fieldset, fns_start.doc.btn_next)
        form_insert(b_headline.dom.firstChild, fns_start.doc.fieldset, fns_start.doc.btn_next)
        form_insert(b_favicon.dom.firstChild, fns_start.doc.fieldset, fns_start.doc.btn_next)
        fns_start.doc.tags_name = form_get("name_id")
        fns_start.doc.headline = form_get("headline_url_id")
        fns_start.doc.favicon = form_get("favicon_url_id")
    }
    /**
     * Old name : part_start_form_err
     */
    function part_start_form_err(input_err, update_data){
		form_input_err(input_err[0], input_err[1], input_err[2],input_err[3])
		part_start_form_update_data(update_data[0], update_data[1], update_data[2])
		form_part_validator(fns_start, fns_start.doc.btn_next)
		return
    }
    /**
     * Create a event for a start part of dynamic form.
     */
    function part_start_form_event() {
        mµ.load_src_in_reserved_name(fns_global.source_json)
        form_toggle_event(fns_start)
        if(default_val.key != ''){
			form_part_validator(fns_start, fns_start.doc.btn_next)
        }
        form_get("name_input").addEventListener("input", (evt) => {
            let value = µ.triw(evt.target.value)
            if(value === ""){
				part_start_form_err(["name_input", "name_err", fns_start.text.MSG_ERROR_VALUE_EMPTY], ["tags_name", false, value])
            }
            let res = mµ.test_name_unicity(value)
            if(!res && default_val.key === '') {
				part_start_form_err(["name_input", "name_err", fns_start.text.MSG_ERROR_NAME_ALREADY_EXIST], ["tags_name", false, value])
            } else {
				form_removeclass("name_input", "form_input_required")
				form_msg("name_err", "")
            }
            add_property(fns_global.new_source_tags_user, "name", value)
            part_start_form_update_data("tags_name", true, value)
            form_part_validator(fns_start, fns_start.doc.btn_next)
        })
        form_get("name_display_tips").onclick = (evt) => {
            let display = form_get("name_display_tips")
            let tips = form_get("name_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_TAGS_NAME)
         }
        form_get("headline_url_input").addEventListener("input", async (evt) => {
            let value = µ.triw(evt.target.value)
            if(value === "") {
				part_start_form_err(["headline_url_input", "headline_url_err",  fns_start.text.MSG_ERROR_VALUE_EMPTY], ["headline", false, value])
            }
            await mµ.request_permissions(["<all_urls>"])
            let res = mµ.test_attribute(
                {}, 
                fns_global.test_integrity, 
                {headline_url : value, tags : {name: fns_global.new_source_tags_user["name"]}}, 
                "headline_url"
            )
            if(!res[0]) {
				part_start_form_err(["headline_url_input", "headline_url_err",  fns_start.text.MSG_ERROR_URL_NOT_FOUND], ["headline", false, value])
            }
            form_removeclass("headline_url_input", "form_input_required")
            form_msg("headline_url_err", "")
            add_property(fns_global.new_source_user, "headline_url", value)
            part_start_form_try_get_favicon(value)
            part_start_form_update_data("headline", true, value)
            form_part_validator(fns_start, fns_start.doc.btn_next)
        })
        form_get("headline_url_display_tips").onclick = (evt) => {
            let display = form_get("headline_url_display_tips")
            let tips = form_get("headline_url_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_HEADLINE_URL)
         }
        form_get("favicon_url_input").addEventListener("input", async (evt) => {
            let value =  µ.triw(evt.target.value)
            if(value === "") {
                form_display("favicon_url_img", false)
                form_removeclass("favicon_url_input", "form_input_required")
                part_start_form_update_data("favicon", false, value)
                form_part_validator(fns_start, fns_start.doc.btn_next)
                return
            }
            let res = mµ.test_attribute(
                {}, 
                fns_global.test_integrity, 
                {favicon_url : value, tags : {name: fns_global.new_source_tags_user["name"]}}, 
                "favicon_url"
            )
            if(res[1][0]) {
                form_display("favicon_url_img", false)
                part_start_form_err(["favicon_url_input", "favicon_url_err", fns_start.text.MSG_ERROR_FAVICON_NOT_FOUND, false], ["favicon", false, value])
            }
            form_removeclass("favicon_url_input", "form_input_warning")
            form_msg("favicon_url_err", "")
            form_get("favicon_url_img").src = value
            form_display("favicon_url_img", true)
            add_property(fns_global.new_source_user, "favicon_url", value)
            part_start_form_update_data("favicon", true, value)
        })
        form_get("favicon_url_display_tips").onclick = (evt) => {
            let display = form_get("favicon_url_display_tips")
            let tips = form_get("favicon_url_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_FAVICON)
         }
        form_get("part_start_form_btn").onclick = (evt) => {
            if(fns_start.completed && !fns_start.passed) {
                part_start_form_next_part()
            }
        }
    }
    /**
     * Start a process to init a next part after start part of dynamic form. 
     */
    function part_start_form_next_part() {
        fns_start.passed = true
        form_toggle(fns_start.doc.fieldset)
        part_timezone_init()
    }
    /**
     * Update a data in fns_start. 
     * @param {string} input the name of the input
     * @param {boolean} res_test the result of tests for the input
     * @param {*} value the value of the input
     */
    function part_start_form_update_data(input, res_test, value) {
        fns_start.data[`${input}`].value = value
        fns_start.data[`${input}`].test_passed = res_test
    }
    /**
     * Try to get a favicon from the specified url.
     * @param {string} url url to get favicon. 
     */
    function part_start_form_try_get_favicon(url) {
        let favicon = µ.get_favicon(url)
        if(favicon !== "") {
            if(form_get("favicon_url_input").value === "")
                form_set("favicon_url_input", favicon, "value", "input")
        }
    }
    /**
     * Init the timezone part of dynamic form.
     */
    function part_timezone_init() {
        fns_timezone.doc.fieldset = document.getElementById("part_timezone_id")
        fns_timezone.doc.legend = document.getElementById("legend_part_timezone")
        fns_timezone.doc.tags_lang = document.getElementById("lang_id")
        fns_timezone.doc.tags_country = document.getElementById("country_id")
        fns_timezone.doc.tags_tz = document.getElementById("tz_id")
        fns_timezone.doc.check_all_timezone = document.getElementById("check_all_tz_id")
        fns_timezone.doc.btn_next = document.getElementById("part_timezone_btn_next")
        form_msg("legend_part_timezone_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_timezone_title", `${fns_timezone.title}`)
        part_timezone_create_lang_choice()
        part_timezone_create_country_choice()
        part_timezone_create_timezone_choice()
        form_set("check_all_tz", false, "checked")
        form_display(fns_timezone.doc.fieldset.id, true, "block")
        part_timezone_event()
        let url = fns_global.new_source_user["headline_url"]
        if(url) 
            part_timezone_try_get_lang(url)
    }
    /**
     * Create a choice list for language of the source with choice.js.
     */
    function part_timezone_create_lang_choice(){
        let select = fns_timezone.doc.tags_lang.querySelector("#lang_select")
        for(let lang of Object.entries(fns_global.lang_code_json)) {
            if(lang[1] !== "error") {
                let option = document.createElement('option')
                option.text = `${lang[1]}`
                option.value = lang[0]
                select.add(option)
            }
        }
        let default_option = document.createElement('option')
        default_option.text = 'Choose a lang of source'
        default_option.value = ""
        select.add(default_option, 0)
        let selected_value
        if(default_val.key != '')
			selected_value = default_val.tags.lang[0]
        else
			selected_value = ''
        let select_opt = {
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : false,
            searchFields: ['label'],
            classNames: {
                containerInner: "choices__inner form_required"
            }
        }
	    fns_timezone.choices.lang = new Choices(select, Object.assign(select_opt))
        fns_timezone.choices.lang.setChoiceByValue(selected_value)
    }
    /**
     * Create a choice list for country of the source with choice.js.
     */
    function part_timezone_create_country_choice(){
        let select = document.getElementById('country_select')
        for(let country of Object.entries(fns_global.country_code_json)) {
            let option = document.createElement('option')
            option.text = `${country[1]}`
            option.value = country[0]
            select.add(option)
        }
        let default_option = document.createElement('option')
        default_option.text = 'Choose a country of source'
        default_option.value = ""
        select.add(default_option, 0)
        let selected_value
        if(default_val.key != '')
			selected_value = default_val.tags.country[0]
        else
			selected_value = ''
        let select_opt = {
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : false,
            searchFields: ['label'],
            classNames: {
                containerInner: "choices__inner form_required"
            }
        }
	    fns_timezone.choices.country = new Choices(select, Object.assign(select_opt))
        fns_timezone.choices.country.setChoiceByValue(selected_value)
    }
    /**
     * Create a choice list for timezone of the source with choice.js.
     */
    function part_timezone_create_timezone_choice() {//Peut etre a factoriser avec part_timezone_create_country_choice et part_timezone_create_lang_choice
        let select = document.getElementById('tz_select')
        for(let cn of Object.entries(fns_global.zone_timezone_json)) {
            let option = document.createElement('option')
            option.text = `${cn[1]}`
            option.value = cn[1]
            select.add(option)
        }
        let default_option = document.createElement('option')
        default_option.text = 'Choose a timezone of source'
        default_option.value = ""
        select.add(default_option, 0)
        let selected_value
        if(default_val.key != '')
        	selected_value = default_val.tags.tz[0]
        else
			selected_value = ''
        let select_opt = {
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : false,
            searchFields: ['label'],
            classNames: {
                containerInner: "choices__inner form_required"
            }
        }
	    fns_timezone.choices.tz = new Choices(select, Object.assign(select_opt))
        fns_timezone.choices.tz.setChoiceByValue(selected_value)
    }
    /**
     * old name : part_timezone_err
     */
    function part_timezone_err(err, msg_err, el){
		if(!el.classList.contains("form_input_required")) //Choice.js element don't have an id so you don't can use "form_addclass".
            el.classList.add("form_input_required")
        form_msg(err, msg_err)
        form_part_validator(fns_timezone, fns_timezone.doc.btn_next)
        return
    }
    function event_timezone(input_name,evt) {
		let val = evt.detail.value
        let el = fns_timezone.choices[input_name].containerInner.element
        let msg_err
        fns_timezone.data[`tags_${input_name}`].value = val
        fns_timezone.data[`tags_${input_name}`].test_passed = val !== ""
        if(!fns_timezone.data[`tags_${input_name}`].test_passed) {
            msg_err = fns_timezone.text.MSG_ERROR_VALUE_EMPTY
        }
        let obj = {tags : {name: fns_global.new_source_tags_user["name"]}}
        obj[input_name] = val
        let res = mµ.test_attribute(
            {},
            fns_global.test_integrity,
            obj,
            `tags.${input_name}`
        )
        if(!res[0]) {
            msg_err = fns_timezone.text.MSG_ERROR_VALUE_IS_INCORRECT
        }
		part_timezone_err(`${input_name}_err`, msg_err, el)
        fns_timezone.choices[input_name].containerInner.element.classList.remove("form_input_required")
        form_msg(`${input_name}_err`, "")
        add_property(fns_global.new_source_tags_user, input_name, val)
        if(input_name === 'lang'){
			if(fns_timezone.choices.country.getValue().value === "")
				fns_timezone.choices.country.setChoiceByValue(val)
		} else if(input_name === 'country')
			part_timezone_update_timezone_choice(val)
        form_part_validator(fns_timezone, fns_timezone.doc.btn_next)
    }
    /**
     * Create a event for a timezone part of dynamic form.
     */
    function part_timezone_event() {
    	if(default_val.key != '')
			form_part_validator(fns_timezone, fns_timezone.doc.btn_next)

		form_toggle(fns_timezone.doc.fieldset)
        form_toggle_event(fns_timezone)
        fns_timezone.choices.lang.passedElement.element.addEventListener("addItem", (evt) => {
            event_timezone("lang",evt)
        })
        fns_timezone.choices.country.passedElement.element.addEventListener("addItem", (evt) => {
            event_timezone("country",evt)
		})
        fns_timezone.choices.tz.passedElement.element.addEventListener("addItem", (evt) => {
			event_timezone("tz",evt)
        })
        form_get("check_all_tz").onclick = (evt) => {
            evt.target.checked ? part_timezone_update_timezone_choice() : 
            part_timezone_update_timezone_choice(fns_timezone.choices.country.getValue().value)
         }
        form_get("part_timezone_btn").onclick = (evt) => {
            if(fns_timezone.completed) {
                form_toggle(fns_timezone.doc.fieldset)
                part_selector_init()
            }
        }
    }
    /**
     * Try determine language to the source from the specified url.
     * @param {string} url URL to try determine language.
     */
    function part_timezone_try_get_lang(url) { 
        let re = new RegExp('^[a-z]{2,3}/?$');
        let part = url.split(".")
        let find = re.exec(part[part.length - 1])
        let ignore = ["com", "org"]
        if(find) {
            find = find[0]
            if(find.endsWith("/"))
                find = find.replace("/", "")
            if(Object.keys(fns_global.lang_code_json).includes(find) && !ignore.includes(find))
                fns_timezone.choices.lang.setChoiceByValue(find)
        } 
    }
    /**
     * Set a timezone choice list with the country code.
     * @param {number | string} country_code country code of the country
     */
    function part_timezone_update_timezone_choice(country_code) {
        let json_tz
        if(country_code === undefined)
            json_tz = fns_global.zone_timezone_json
        else 
            json_tz = fns_global.timezone_json[`${country_code}`]
        let choices = [{label: 'Choose a timezone of source', value: "", placeholder: true}]
        for(let v of Object.values(json_tz)){
            if(v instanceof Array)
                v = v[0]
            choices.push({label : v, value: v})
        }
        fns_timezone.choices.tz.clearStore()
        fns_timezone.choices.tz.setChoices(choices, "value", "label", false)
        fns_timezone.choices.tz.setChoiceByValue(choices.filter((el) => {return el.value !== ""})[0].value)
    }
    /**
     * Init the selector part of dynamic form.
     */
    function part_selector_init() {
        fns_selector.doc.fieldset = document.getElementById("part_selector_id")
        fns_selector.doc.legend = document.getElementById("legend_part_selector")
        fns_selector.doc.search_url = document.getElementById("search_url_id")
        fns_selector.doc.sub_dom = document.getElementById("sub_dom")
        fns_selector.doc.search_url.querySelector("#search_url_input").value = fns_selector.data.search_url.value
        fns_selector.doc.search_url.querySelector("#positive_test_search_term_input").value = fns_selector.data.positive_test_search_term.value
        fns_selector.doc.search_url.querySelector("#search_url_web_input").value = fns_selector.data.search_url_web.value
        form_msg("legend_part_selector_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_selector_title", `${fns_selector.title} - ${fns_selector.mode.toUpperCase()}`)
        form_display(fns_selector.doc.fieldset.id, true, "block")
        part_selector_event()
    }
    function validation_part_selector(val, input, input_err) {
        fns_selector.data[input].value = val
        if(val === "") {
            form_input_err(`${input}_input`, input_err, fns_selector.text.MSG_ERROR_VALUE_EMPTY)
            fns_selector.data[input].test_passed = false
            form_part_validator(fns_selector, "finish_selection_part")
            return
        }
        if(input === 'positive_test_search_term') {
		    let match = part_selector_try_find_search_terms(fns_selector.data.search_url.value, val)
			if(match === null && fns_selector.mode !== "post") {
				form_input_err(`${input}_input`, input_err, fns_selector.text.MSG_ERROR_BAD_SEARCH_TERM)
				fns_selector.data[input].test_passed = false
				form_display("search_url_method_post", true)
				form_get("search_url_method_post").onclick = (evt) => {
					part_selector_update_mode("post")
					form_msg("legend_part_selector_title", `${fns_selector.title} - ${fns_selector.mode.toUpperCase()}`)
					form_get("positive_test_search_term_input").dispatchEvent(new Event("input"))
					form_get("search_url_btn").dispatchEvent(new Event("click"))
				}
				form_part_validator(fns_selector, "finish_selection_part")
				return
			}
			form_display("search_url_method_post", false)
        }
        form_removeclass(`${input}_input`, "form_input_required")
        form_msg(input_err, "")
        fns_selector.data[input].test_passed = true
        form_part_validator(fns_selector, "finish_selection_part")
    }
    /**
     * Create a event for a selector part of dynamic form.
     */
    function part_selector_event() {
    		form_toggle(fns_selector.doc.fieldset)
        form_toggle_event(fns_selector)
        form_get("search_url_input").addEventListener("input", (evt) => {
            let val = µ.triw(evt.target.value)
            validation_part_selector(val, "search_url", "search_url_err")
        })
        form_get("positive_test_search_term_input").addEventListener("input", (evt) => {
            let val = µ.triw(evt.target.value)
            validation_part_selector(val, "positive_test_search_term", "search_url_err")
        })
        form_get("search_url_btn").onclick = async (evt) => {
			if(default_val.key && default_val.key !== ''){
				validation_part_selector(fns_selector.data.search_url.value, "search_url", "search_url_err")
				validation_part_selector(fns_selector.data.positive_test_search_term.value, "positive_test_search_term", "search_url_err")
			}
            if(fns_selector.data.search_url.test_passed && fns_selector.data.positive_test_search_term.test_passed) {
                //part_selector_reset()
                let searchUrl = fns_selector.data.search_url.value 
                let terms = fns_selector.data.positive_test_search_term.value
                add_property(fns_global.new_source_user, "positive_test_search_term", terms)
                if(fns_selector.data.search_url.hidden_value !== searchUrl.replace(terms, "{}")) {
                    fns_selector.data.search_url.hidden_value = searchUrl.replace(terms, "{}")
                    add_property(fns_global.new_source_user, "search_url", fns_selector.data.search_url.hidden_value)
                    await part_selector_fetch(searchUrl)
                    if( !form_is_exist("mode_selection") ) {
                        let template = [
                            {node : "div", id:"mode_selection", classList : ["input_container"], style: "margin-top: 0.5em;"},
                                {node : "p", id: "text_type_content_detected", classList : ["form_text_info"],
                                textContent : fns_selector.text.MSG_TEXT_CONTENT_TYPE_HTML, 
                                condition: fns_selector.request.content_type === "html", parent: 0},
                                {node : "p", id: "text_type_content_detected", classList : ["form_text_info"],
                                textContent : fns_selector.text.MSG_TEXT_CONTENT_TYPE_JSON, 
                                condition: fns_selector.request.content_type === "json", parent: 0},
                                {node : "span", id: "mode_title", 
                                textContent : fns_selector.text.MSG_TEXT_MODE_TITLE, parent : 0},
                                {node : "button", id:"mode_css", classList : ["a", "btn", "form_selection_input_btn"], 
                                textContent: fns_selector.text.MSG_TEXT_BTN_METHOD_CSS, parent: 0},
                                {node : "button", id:"mode_json", classList : ["a", "btn", "form_selection_input_btn"], 
                                textContent: fns_selector.text.MSG_TEXT_BTN_METHOD_JSON, parent: 0},
                                {node : "button", id:"mode_rss", classList : ["a", "btn", "form_selection_input_btn"], 
                                textContent: fns_selector.text.MSG_TEXT_BTN_METHOD_RSS, parent: 0},
                        ]
                        let builder = form_builder(template)
                        let el = builder.dom.firstChild
                        form_insert(el, fns_selector.doc.fieldset, form_get("sub_dom"))
                        form_get("mode_css").onclick = (evt) => {
                            part_selector_update_mode(fns_selector.mode === "post" ? "post" : "get" )
                            form_search_and_removeclass("form_mode_selected")
                            form_addclass(evt.target.id, "form_mode_selected")
                            form_msg("legend_part_selector_title", `${fns_selector.title} - ${fns_selector.mode.toUpperCase()}`)
                            remove_property(fns_global.new_source_user, `search_url_web`)
                            form_display("search_url_web_input", false)
                            part_selector_before_sub_dom()
                        }
                        form_get("mode_json").onclick = (evt) => {
                            part_selector_update_mode(fns_selector.mode === "post" ? "post" : "json" )
                            form_search_and_removeclass("form_mode_selected")
                            form_addclass(evt.target.id, "form_mode_selected")
                            form_msg("legend_part_selector_title", `${fns_selector.title} - ${fns_selector.mode.toUpperCase()}`)
                            form_display("search_url_web_input", true)
                            part_selector_before_sub_dom()
                        }
                    }
                    fns_selector.request.content_type === "html" ? 
                    form_addclass("mode_css", "form_input_warning") : form_addclass("mode_json", "form_input_warning") 
                } else {
                    let hiddenSearch = fns_selector.data.search_url.hidden_value 
                    searchUrl = hiddenSearch.replace("{}", terms)
                    form_set("search_url_input", searchUrl, "value", "input")
                    form_get("positive_test_search_term_input").dispatchEvent(new Event("input"))
                    await part_selector_fetch(searchUrl)
                    part_selector_before_sub_dom()
                }
            }
        }
        form_get("finish_selection_part_btn").onclick = (evt) => {
            form_toggle(fns_selector.doc.fieldset)
            part_tags_init()
         }
    }
    /**
     *  Add the method to the new source.
     * @param {string} mode methode for the source (GET | POST | JSON | RSS) 
     */
    function part_selector_update_mode(mode) {
        if(mode === "get") {
            remove_property(fns_global.new_source_user, "method")
        } else {
            add_property(fns_global.new_source_user, "method", mode.toUpperCase())
        }
        fns_selector.mode = mode.toLowerCase()
    }
    /**
     * Process to init all sub dom have need.
     * @async
     */
    async function part_selector_before_sub_dom() {
        fns_selector.request.content_type === "html" ? 
        form_removeclass("mode_css", "form_input_warning") : form_removeclass("mode_json", "form_input_warning") 
        form_remove("text_type_content_detected")
        form_display("sub_dom", false)
        part_selector_reset()
        let url = fns_selector.data.search_url.value
        let headline = fns_start.data.headline.value
        fns_selector.mode === "post" ? await part_selector_fetch(headline) : await part_selector_fetch(url)
        if(!await part_selector_try_get_virtual_doc()) {
            let template = [
                {node: "p", id: "request_error", textContent: "Sorry but this method trigger a error. Please try other method", 
                style:"color: red; margin:auto;"}
            ]
            let builder = form_builder(template)
            form_insert(builder.dom.firstChild, fns_selector.doc.fieldset, form_get("sub_dom"))
            return
        } else {
            form_remove("request_error")
        }
        if(fns_selector.mode === "get" ||  fns_selector.mode === "post") {
            let body = fns_global.virtual_doc.body
            body = new DOMParser().parseFromString( body.innerHTML, "text/html")
            body = body.children[0].children[1]
            part_selector_depth_clean_virtual_doc(body.childNodes)
            part_selector_depth_clean_virtual_doc(body.childNodes)
            part_selector_child_have_text_content(body.childNodes)
            part_selector_set_sub_dom(body)
        } else if(fns_selector.mode === "json") {
            let search_url_web = µ.dry_triw(form_get("search_url_web_input").value)
            if(search_url_web === "") {
                add_property(fns_global.new_source_user, `search_url_web`, fns_start.data.headline.value)
                form_set("search_url_web_input", fns_start.data.headline.value, "value")
            } else {
                add_property(fns_global.new_source_user, `search_url_web`, search_url_web)
            }
            let json = fns_global.virtual_doc
            part_selector_set_sub_dom(json)
        }
        form_display("sub_dom", true, "block")
        fns_selector[`current_index`] = 0
        let test_terms = part_selector_build_list_test_terms()
        part_selector_try_determine_is_fast(fns_selector.data.search_url.hidden_value, test_terms).then(
            (response) => {
                console.log(response)
                fns_tags.time_fast = response
            }
        )
        part_selector_build_input()
    }
    /**
     * Reset the selector part from the dynamic form.
     */
    function part_selector_reset() {
        fns_selector[`current_index`] = 0
        fns_selector[`old_index`] = null
        fns_selector[`completed`] = false
        for(let input of fns_selector.form_input) {
            if(form_is_exist(`${input}_id`)) {
                let child = form_get(`${input}_id`)
                form_remove(child.id)
                let data = fns_selector.data[`${input}`]
                if(input === "r_dt") {
                    let count = 0
                    for(let dt of data.regex) {
                        remove_property(fns_global.new_source_user, `${input}_fmt_${count}`)
                        count += 1
                    }
                }
                part_selector_reset_data(input)
                remove_property(fns_global.new_source_user, `${input}`)
                remove_property(fns_global.new_source_user, `${input}_re`)
                remove_property(fns_global.new_source_user, `${input}_attr`)
                remove_property(fns_global.new_source_user, `${input}_xpath`) 
            }
        }
    }
    /**
     * Reset the data to selector part from the dynamic part.
     * @param {string} input the name of input.
     */
    function part_selector_reset_data(input) {
        let data = fns_selector.data[`${input}`]
        data.value = ""
        if(typeof(data.regex) !== "undefined")
            data.type_regex === "string" ? data.regex = null : data.regex = []
        if(typeof(data.attr) !== "undefined")
            data.attr = null
        if (typeof(data.sub) !== "undefined") {
            data.test_passed = true
            data.skip = true
        } else {
            data.test_passed = false
        }
    }
    /**
     * Process try to find a term from the specified url.
     * @param {string} url url to search terms.
     * @param {string} val search terms.
     * @returns 
     */
    function part_selector_try_find_search_terms(url, val) {
        let separator = ["=", "/"]
        let spaces = ["+", "%20"]
        try {
            let url = new URL(url)
            let tmp = url.search.split("=")
            let term = tmp[tmp.length - 1]
            let match = null
            for(let space of spaces) {
                let tmp_val = new RegExp(`${val.replaceAll(" ", `${space === '+' ? `\\${space}` : space}`)}`)
                match = term.match(tmp_val)
                if(match !== [] && match !== null)
                    return match
            }
            return null
        } catch(err) {}
        for(let sep of separator) {
            let tmp = url.split(sep)
            let match = null
            for(let term of tmp) {
                for(let space of spaces) {
                    let tmp_val = new RegExp(`${val.replaceAll(" ", `${space === '+' ? `\\${space}` : space}`)}`)
                    match = term.match(tmp_val)
                    if(match !== [] && match !== null)
                        break
                }
                if(match === [] || match === null)
                    continue
                return match
            }
        }
        return null
    }
    /**
     * Perform a request on the specified url.
     * @async
     * @param {string} url url to perform a request.
     * @param {string} method method for the request (GET | POST). GET by default.
     * @param {string} body body of the request. null by default.
     */
    async function part_selector_fetch(url, method="GET", body=null) {
        console.log(method, body)
        let raw = await fetch(url, {
            method: typeof(method) !== "undefined" ? method : 'GET',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
            body : typeof(body) !== "undefined" ? body : null
        })
        fns_selector.request.raw = raw
        fns_selector.request.content_type = await part_selector_get_response_content_type()
    }
    /**
     * Return the type of responce from a request.
     * @async
     * @returns {string} le type of responce.
     */
    async function part_selector_get_response_content_type() {
        for(let info of fns_selector.request.raw.headers.entries()) {
            if(info[0] === "content-type") {
                if(info[1] === "application/json")
                    return "json"
                else if(info[1].match(/text\/html/) !== null)
                    return "html"
            }
        }
        return "html"
    }
    /**
     * Create a body with a data from form and return it.
     * @async
     * @param {FormData} data FormData object from the form.
     * @returns {string} build body for the request.
     */
    async function part_selector_build_post_request(data) {
        let body = ""
        for(let [key, value] of data.entries())
            body === "" ? body += `${key}=${value}` : body += `&${key}=${value}`
        return body
    }
    /**
     * Try to get a virtual document with the request.
     * @async
     * @see {@link part_selector_fetch}
     * @returns {boolean} true or false to indicate is a succes or not.
     */
    async function part_selector_try_get_virtual_doc() {
        if(fns_selector.mode === "get" ||  fns_selector.mode === "post") {
            try {
                let raw_text = await fns_selector.request.raw.text()
                fns_global.virtual_doc = new DOMParser().parseFromString(raw_text, 'text/html')
            } catch(err) {
                return false
            }
        } else if (fns_selector.mode === "json") {
            try {
                let raw_json = await fns_selector.request.raw.json()
                fns_global.virtual_doc = raw_json
            } catch(err) {
                return false
            }
        }
        return true
    }
    /**
     * Clean the virtual document.
     * @param {NodeList} childs List of node of body from the virtual document.
     */
    function part_selector_depth_clean_virtual_doc(childs){
        let tags_to_drop = ["meta", "link", "svg", "style", "script", "header", "footer", "iframe", "embed", "object"]
        let reg = new RegExp(/on.*/)
        for(let c of childs){
            if(tags_to_drop.includes(c.localName)) {
                console.log(c)
                c.parentNode.removeChild(c)
            } else if(c.childNodes.length > 0)
                part_selector_depth_clean_virtual_doc(c.childNodes)
            if(c.localName === "a") {
                c.disabled = true
                //c.removeAttribute("href")// remove href pose problème avec JSON/form, car _attr est href de base dans la sources mais une fois remove le formulaire ne le retrouve pas et plante -> a voir si ca pose problème avec le formulaire de base
            }
            /** 
            else if(c.localName === "script"){
                c.removeAttribute("src")
                c.textContent = ""
            }*/ else if(c.localName === "input" || c.localName === "button"){
                c.removeAttribute("type")
                if(c.localName === "button")
                    c.disabled = true
            }
            if(typeof(c.attributes) !== "undefined")
                for(let attr of c.attributes)
                    if(attr.localName.match(reg) !== [])
                        c.removeAttribute(attr)
        }
    }
    /**
     * Process to add a class to parent div node for show him in the sub dom.
     * @param {HTMLElement} node a node. 
     */
    function part_selector_add_class_in_first_container(node, frame_trigger) {
        /**
        while(node.localName !== "body") {
            node = node.parentNode
            if(node.children.length > 9)
                frame_trigger = true
            if(fns_selector.sub_dom.container_node.includes(node.localName) 
                && node.childNodes.length > 1 
                //&& !node.classList.contains(`${fns_selector.sub_dom.class_show_div}`)
                && frame_trigger
            ) {
                node.classList.add(fns_selector.sub_dom.class_show_div)
                break
            }
        } */

        node = node.parentNode
        if(node.children.length > 4)
            frame_trigger = true
        if(fns_selector.sub_dom.container_node.includes(node.localName) 
            && node.childNodes.length > 1 
            //&& !node.classList.contains(`${fns_selector.sub_dom.class_show_div}`)
            && frame_trigger
        ) {
            node.classList.add(fns_selector.sub_dom.class_show_div)
        }
        return frame_trigger
    }
    /**
     * Process to know who child have text content.
     * @param {NodeList} childs List of node.
     */
    function part_selector_child_have_text_content(childs) {
        let count = 0
        let frame_trigger = true
        while(count < childs.length) {
            let child = childs[count]
            if(child.nodeName === "#comment" || child.nodeName === "#text") {
                count += 1 
                continue
            }
            if(µ.dry_triw(child.textContent) !== "" && !fns_selector.sub_dom.container_node.includes(child.localName) )
                frame_trigger = part_selector_add_class_in_first_container(child, frame_trigger)
            if(child.childNodes.length > 0)
                childs = [...childs, ...child.childNodes]
            count += 1
        }
    }
    /**
     * Try to determine is a source is fast to responce from the specified url.
     * @async
     * @param {string} url url to the source.
     * @param {array} terms list of terms for test the source.
     * @returns {number} the time average of responces.
     */
    async function part_selector_try_determine_is_fast(url, terms) {
        let times = []
        for(let term of terms) {
            let testUrl = url.replace("{}", term) 
            let start = new Date()
            await part_selector_fetch(testUrl, fns_selector.mode)
            let end = new Date()
            times.push(end - start)
        }
        let average = times.reduce( (preVal, nextVal) => { return preVal + nextVal}) / times.length
        return (average / 1000).toFixed(3)
    }
    /**
     * Build a list of most positive term search use from file source.json
     * @returns {array} Lists of terms
     */
    function part_selector_build_list_test_terms() {
        let terms = []
        for(let [key, val] of Object.entries(fns_global.source_json)) {
            if(typeof(val.positive_test_search_term) !== "undefined") {
                for(let positive_term of val.positive_test_search_term) {
                    if(positive_term === "")
                        continue
                    let exist = terms.filter( (t) => {return t.name === positive_term})
                    if(exist.length !== 0) {
                        exist[0].count += 1
                    } else {
                        let new_term = {name : positive_term, count : 1}
                        terms.push(new_term)
                    }
                }
            }
        }
        return terms.sort( (a, b) => {
            if(a.count > b.count)
                return -1
            else if(a.count < b.count)
                return 1
            return 0
        }).slice(0, 5).map((val) => {return val.name})
    }
    /**
     * Process to build a input for selector sub dom in selector part from dynamic form.
     */
    function part_selector_build_input() {
    	fns_selector.old_index = null
        if(fns_selector.current_index < fns_selector.form_input.length) {
            let input = fns_selector.form_input[fns_selector.current_index]
            let data = form_part_get_data(fns_selector, input)
            if(!data.methode.includes(fns_selector.mode)) {
                data.test_passed = true
                fns_selector.current_index += 1
                part_selector_build_input()
                return
            }
            if(typeof(data.skip) !== "undefined" && data.skip) {
                fns_selector.current_index += 1
                part_selector_build_input()
                return
            }
            let template  = null
            if(data.type === "required") {
                template = [
                    {node: "div", id:`${input}_id`, name:`${input}`, classList: ["form_selection_container"], style:"display: none"},
                        {node:"div", classList: [`input_container`], parent:0},
                            {node: "label", id:`${input}_text`, parent:1},
                                {node: "span", style:"font-weight: bold;", 
                                textContent:fns_selector.text[`MSG_TEXT_TITLE_${input.toUpperCase()}`],
                                classList: [`${data.css_class}_text`], parent: 2},
                                {node: "text", textContent:" : ", parent: 2},
                                {node: "span", id:`${input}_final`, classList: ["form_selection_text"], 
                                parent: 2},
                            {node: "input", id: `${input}_input`, 
                            placeholder:fns_selector.text[`MSG_TEXT_PLACEHOLDER_${input.toUpperCase()}`], 
                            title:"Ex : https://www.custom-source.eu", type:"text", 
                            classList: ["form_input_text"], value: data.value, parent:1},
                            {node:"button", id:`${input}_btn_confirm`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_CONFIRM, parent:1},
                            {node:"button", id:`${input}_btn_back`, classList: ["btn", "a", "form_selection_input_btn"], 
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_BACK, parent:1},
                            {node:"button", id:`${input}_btn_show`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_SHOW, parent:1},
                            {node:"button", id:`${input}_btn_not_define`, classList: ["btn", "a", "form_selection_input_btn"],  
                            textContent: fns_selector.text.MSG_TEXT_BTN_NOT_DEFINE, 
                            condition: fns_selector.mode === "json" && input === "results", parent:1},
                            {node:"button", id:`${input}_btn_attr`, classList: ["btn", "a", "form_selection_input_btn"],  
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_ATTR, parent:1},
                            {node: "span", id:`${input}_final_attr`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"button", id:`${input}_btn_reg`, classList: ["btn", "a", "form_selection_input_btn"],  
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_REG, parent:1},
                            {node: "span", id:`${input}_final_reg`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"p", id:`${input}_tips`, classList: ["form_input_tips"], parent:1},
                            {node:"button", id:`${input}_display_tips`, classList: ["btn", "a", "form_selection_input_btn"], parent:1},
                        {node:"p", id:`${input}_err`, classList: ["form_input_error"], parent:0}
                ]
            } else {
                template = [
                    {node: "div", id:`${input}_id`, name:`${input}`, classList: ["form_selection_container"], style:"display: none"},
                        {node:"div", classList: [`input_container`], parent:0},
                            {node: "label", id:`${input}_text`, parent:1},
                                {node: "span", style:"font-weight: bold;", 
                                textContent:fns_selector.text[`MSG_TEXT_TITLE_${input.toUpperCase()}`],
                                classList: [`${data.css_class}_text`], parent: 2},
                                {node: "text", textContent:" : ", parent: 2},
                                {node: "span", id:`${input}_final`, classList: ["form_selection_text"], 
                                parent: 2},
                            {node: "input", id: `${input}_input`, 
                            placeholder:fns_selector.text[`MSG_TEXT_PLACEHOLDER_${input.toUpperCase()}`], 
                            title:"Ex : https://www.custom-source.eu", type:"text", classList: ["form_input_text"], 
                            value: data.value, parent:1},
                            {node:"button", id:`${input}_btn_confirm`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_CONFIRM, parent:1},
                            {node:"button", id:`${input}_btn_back`, classList: ["btn", "a", "form_selection_input_btn"], 
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_BACK, parent:1},
                            {node:"button", id:`${input}_btn_show`, classList: ["btn", "a", "form_selection_input_btn"], 
                            textContent: fns_selector.text.MSG_TEXT_BTN_SHOW, parent:1},
                            {node:"button", id:`${input}_btn_attr`, classList: ["btn", "a", "form_selection_input_btn"],  
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_ATTR, parent:1},
                            {node: "span", id:`${input}_final_attr`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"button", id:`${input}_btn_reg`, classList: ["btn", "a", "form_selection_input_btn"], 
                            style: "display: none;", textContent: fns_selector.text.MSG_TEXT_BTN_REG, parent:1},
                            {node: "span", id:`${input}_final_reg`, classList: ["form_selection_text"], 
                            parent: 1},
                            {node:"button", id:`${input}_btn_disable`, classList: ["btn", "a", "form_selection_input_btn"], 
                            innerHTML: fns_global.icons.cross, parent:1},
                            {node:"p", id:`${input}_tips`, classList: ["form_input_tips"], parent:1},
                            {node:"button", id:`${input}_display_tips`, classList: ["btn", "a", "form_selection_input_btn"], parent:1},
                        {node:"p", id:`${input}_err`, classList: ["form_input_error"], parent:0}
                ]
            }
            let builder = form_builder(template)
            let child = builder.dom.firstChild
            let sub_dom = form_get("sub_dom")
            form_insert(child, fns_selector.doc.fieldset, sub_dom )
            if(data.type === "optionnal") {
                form_get(`${input}_btn_disable`).onclick = (evt) => {
                    let comp = document.createElement("div")
                    comp.innerHTML = fns_selector.text.MSG_TEXT_BTN_BACK
                    part_selector_destroy_regex(input)
                    part_selector_destroy_attr(input)
                    if(evt.target.innerHTML !== comp.innerHTML) {
                        evt.target.innerHTML = fns_selector.text.MSG_TEXT_BTN_BACK
                        form_display(`${input}_input`, false)
                        form_display(`${input}_btn_confirm`, false)
                        form_display(`${input}_btn_show`, false)
                        form_display(`${input}_btn_back`, false)
                        form_display(`${input}_btn_reg`, false)
                        form_display(`${input}_btn_attr`, false)
                        form_msg(`${input}_final`, fns_selector.text.MSG_TEXT_INPUT_DISABLED)
                        data.test_passed = false
                        data.is_passed = true
                        remove_property(fns_global.new_source_user, `${input}`)
                        if(fns_selector.old_index !== null) {
                            part_selector_input_focus()
                        } else {
                            form_removeclass(`${input}_input`, "form_input_required")
                            form_msg(`${input}_err`, "")
                            data.test_passed = false
                            part_selector_clear_selection()
                            part_selector_build_input()
                            form_part_validator(fns_selector, "finish_selection_part")
                        }
                    } else {
                        evt.target.innerHTML = fns_global.icons.cross
                        form_display(`${input}_input`, true)
                        if(data.test_passed) {
                            form_display(`${input}_btn_back`, true)
                            form_display(`${input}_btn_reg`, true)
                        } else {
                            form_display(`${input}_btn_confirm`, true)
                        }
                        form_display(`${input}_btn_show`, true)
                        form_msg(`${input}_final`, "")
                        data.test_passed = false
                        data.is_passed = default_val[input][0] === ''
                        part_selector_input_focus()
                    }
                }
            }
            if(default_val.key != '' && data.is_passed){
				fns_selector.current_index += 1
				form_get(`${input}_btn_disable`).click()
            }
            fns_selector.doc[`${input}`] = form_get(`${input}_id`)
            //let doc = fns_selector.doc[`${input}`] 
            if(data.type === "optionnal")
                form_addclass(`${input}_input`, `form_warning`)
            if(typeof(data.sub) !== "undefined") {
                form_addclass(`${input}_id`, 'form_selection_container_sub')
            }
            if(data.value !== "") {
                let el = part_selector_get_sub_dom_element(data.value)[0]
                if(typeof(el) !== "undefined") {
                    el.classList.add(`${data.css_class}`)
                }
            }
			let re = '^.*(#[^#]*)$'
			let value = µ.regextract(re, data.value)
			form_set(`${input}_input`, value, "value")
			form_msg(`${input}_display_tips`, `${fns_global.icons.question}`)
            part_selector_new_sub_dom_listener(input, data)
            if(fns_selector.mode === "json") {
                let old_input = fns_selector.form_input[fns_selector.current_index - 1]
                if(typeof(old_input) !== "undefined")  {
                    let old_data = form_part_get_data(fns_selector, old_input)
                    if(old_data.value !== "" && old_data.value !== "\"\"")
                        part_selector_json_show(old_data.value)
                }
            }
            form_get(`${input}_display_tips`).onclick = (evt) => {
                let display = form_get(`${input}_display_tips`)
                let tips = form_get(`${input}_tips`)
                form_tips(display, tips, fns_selector.text[`MSG_TIPS_${input.toUpperCase()}`] )
            }
            form_get(`${input}_input`).addEventListener("input", (evt) => {
                part_selector_destroy_attr(input)
                part_selector_destroy_regex(input)
                form_search_and_removeclass("form_input_required")
                form_msg(`${input}_err`, "")
                data.value = evt.target.value
            })
            form_get(`${input}_btn_back`).onclick = (evt) => {
                part_selector_clear_selection()
                form_removeclass(`${input}_btn_reg`, "form_input_required")
                form_msg(`${input}_err`, "")
                part_selector_reset_data(input)
                part_selector_input_focus()
                form_msg(`${input}_final`, "")
                //form_set(`${input}_final`, "", "textContent")
                form_display(`${input}_input`, true)
                form_display(`${input}_btn_confirm`, true)
                form_display(`${input}_btn_back`, false)
                fns_selector.mode === "json" && input === "results" ? form_display(`${input}_btn_not_define`, true) : null
                form_display(`${input}_btn_attr`, false)
                form_display(`${input}_btn_reg`, false)
                form_part_validator(fns_selector, "finish_selection_part")
            }
            form_get(`${input}_btn_show`).onclick = (evt) => {
                part_selector_clear_selection()
                let path = µ.triw(data.value)
                if(path === "") {
                    form_input_err(`${input}_input`, `${input}_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                    data.test_passed = false
                    form_part_validator(fns_selector, "finish_selection_part")
                    return
                }
                if(fns_selector.mode === "get" || fns_selector.mode === "post") {
                    let el = form_get("sub_dom").querySelector(`${path}`)
                    if(el.id === "")
                        el.setAttribute("id", fns_selector.sub_dom.id_show_element)
                    el.classList.add(`${data.css_class}`)
                    window.location.hash = el.id
                    el.id ===  fns_selector.sub_dom.id_show_element ? el.removeAttribute("id") : null
                } else if(fns_selector.mode === "json") {
                    let el = part_selector_json_show(path)
                    el = el.parentNode
                    let keys = el.querySelectorAll(".key")
                    path = path.split(".").reverse()[0]
                    for(let key of keys) {
                        if(key.textContent === `"${path}"`) {
                            key.classList.add(`${data.css_class}`)
                            break
                        }
                    }
                }
                form_removeclass(`${input}_input`, "form_input_required")
                form_msg(`${input}_err`, "")
                data.test_passed = true
            }
            if(fns_selector.mode === "json" && input === "results") {
                form_get(`${input}_btn_not_define`).addEventListener( "click", (evt) => {
                    data.value = '""'
                    data.test_passed = true
                    form_removeclass(`${input}_input`, "form_input_required")
                    form_msg(`${input}_err`, "")
                    form_display(`${input}_input`, false)
                    form_display(`${input}_btn_confirm`, false)
                    form_display(`${input}_btn_back`, true)
                    fns_selector.mode === "json" && input === "results" ? form_display(`${input}_btn_not_define`, false) : null
                    typeof(data.attr) !== "undefined" && fns_selector.mode !== "json" ? form_display(`${input}_btn_attr`, true) : null
                    typeof(data.attr) !== "undefined" ? form_display(`${input}_btn_reg`, true) : null
                    form_msg(`${input}_final`, `Results is not defined`)
                    if(fns_selector.old_index === null && form_part_get_data(fns_selector, fns_selector.form_input[fns_selector.current_index - 1]).test_passed )
                        part_selector_build_input()
                    else
                        part_selector_input_focus()
                    add_property(fns_global.new_source_user, input, data.value)
                    form_part_validator(fns_selector, "finish_selection_part")
                })
            }
            if(typeof(data.regex) !== "undefined") {
                form_get(`${input}_btn_reg`).onclick = (evt) => {
                    let val = part_selector_sub_dom_get_element_text(input).value
                    if(val === "" || val === null) {
                        part_selector_destroy_regex(input)
                        form_input_err(`${input}_btn_reg`,`${input}_err`,   fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
                        return
                    }
                    form_removeclass(`${input}_btn_reg`, "form_input_required")
                    form_msg(`${input}_err`, "")
                    if(!form_is_exist(`${input}_reg`)) 
                        part_selector_init_regex(input, data, val)                    
                    else
                        part_selector_destroy_regex(input)
                }
            }
            if(typeof(data.attr) !== "undefined" && fns_selector.mode !== "json") {
                form_get(`${input}_btn_attr`).onclick = (evt) => {
                    form_removeclass(`${input}_btn_reg`, "form_input_required")
                    form_msg(`${input}_err`, "")
                    if(!form_is_exist(`${input}_attr`)) {
                        let contents = part_selector_get_content(data)
                        if(contents !== [])
                            part_selector_init_attr(contents, input)
                    } else {
                        part_selector_destroy_attr(input)
                    }
                }
            }
            form_get(`${input}_btn_confirm`).onclick = (evt) => {
                form_removeclass(`${input}_input`, "form_input_required")
                form_msg(`${input}_err`, "")
                let val = form_get(`${input}_input`).value
                if(val === "") {
                    form_input_err(`${input}_input`, `${input}_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                    data.test_passed = false
                    form_part_validator(fns_selector, "finish_selection_part")
                    return
                }
                if(data.type_content === "link" && fns_selector.mode !== "json") {
                    let el = form_get("sub_dom").querySelector(val)
                    if(el.localName !== "a") {
                        form_input_err(`${input}_input`, `${input}_err`, fns_selector.text.MSG_ERROR_IS_NOT_LINK)
                        data.test_passed = false
                        form_part_validator(fns_selector, "finish_selection_part")
                        return
                    }
                }
                let el
                if(fns_selector.mode === "get" ||  fns_selector.mode === "post")
                    el = form_get("sub_dom").querySelector(val)
                if(fns_selector.mode === "json") {
                    el = fns_global.virtual_doc
                    let path = val.split(".")
                    for(let p of path) {
                        if(el.length === undefined)
                            el = el[`${p}`]
                        else
                            el = el[0][`${p}`]
                        if(typeof(el) === "undefined")
                            break   
                    }
                }
                if(el === null  || typeof(el) === "undefined") {
                    form_input_err(`${input}_input`, `${input}_err`, fns_selector.text.MSG_ERROR_NOT_ELEMENT_FOUND )
                    data.test_passed = false
                    form_part_validator(fns_selector, "finish_selection_part")
                    return
                }
                data.value = val
                if(typeof(data.attr) !== "undefined" && input !== "r_img" && fns_selector.mode !== "json") {
                    let text = part_selector_sub_dom_get_element_text(input).value
                    if(text === "" || text === null) {
                        let contents = part_selector_get_content(data)
                        if(contents !== [])
                            part_selector_init_attr(contents, input)
                        form_input_err(`${input}_btn_attr`, `${input}_err`, fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
                        data.test_passed = false
                        form_part_validator(fns_selector, "finish_selection_part")
                        return
                    }
                    form_removeclass(`${input}_btn_attr`, "form_input_required") 
                    part_selector_destroy_attr(input)
                }
                if(input === "results" && fns_selector.mode !== "json") {
                    for(let fns_input of fns_selector.form_input) {
                        if(fns_input !== "body" && fns_input !== "results") {
                            let d = fns_selector.data[`${fns_input}`]
                            for( let node of d.nodes) {
                                if(node === "")
                                    continue
                                let el = form_get("sub_dom").querySelector(`${data.value} ${node}`)
                                if(el) {
                                    d.value = part_selector_get_css_path(el, data.css_class)
                                    break
                                }
                            }
                        }
                    }
                }
                if(input === "r_dt" && fns_selector.mode !== "json") {
                    part_selector_destroy_regex(input)
                    let obj = part_selector_build_fmt_date_obj(data)
                    let path = data.value
                    let dates = part_selector_get_sub_dom_element(path)
                    for(let date of dates) {
                        if(date.localName === "time") {
                            let datetime = date.getAttribute("datetime")
                            if(typeof(datetime) !== "undefined") {
                                add_property(fns_global.new_source_user, "r_dt_attr", "datetime")
                                break
                            }
                        }
                        let preview = date.textContent
                        try {
                            mµ.parse_dt_str(
                                preview, 
                                fns_timezone.data.tags_tz.value,
                                1, 
                                obj.fmts, 
                                fns_global.month_nb_json)
                        } catch(err) {
                            part_selector_init_regex(input, data, preview)
                            form_addclass(`${input}_reg_input`, "form_input_required")
                            form_addclass(`${input}_reg_input_token`, "form_input_required")
                            form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_DATE_FOUND)
                            return
                        }
                    }
                }
                if(input === "r_img" && fns_selector.mode !== "json") {
                    let path = data.value
                    let img = part_selector_get_sub_dom_element(path)[0]
                    if(typeof(img) !== "undefined") {
                        let src = img.src
                        let alt = img.alt
                        if(src === "") {
                            fns_selector.data.r_img_src.skip = false
                            fns_selector.data.r_img_src.test_passed = false
                            fns_selector.data.r_img_src.value = data.value
                        }
                        if(alt === "") {
                            fns_selector.data.r_img_alt.skip = false
                            fns_selector.data.r_img_alt.test_passed = false
                            fns_selector.data.r_img_alt.value = data.value
                        }
                    }
                }
                if(input === "res_nb") {
                    let val = part_selector_sub_dom_get_element_text(input).value
                    if(val === "" || val === null) {
                        form_input_err(`${input}_btn_attr`,`${input}_err`, fns_selector.text.MSG_ERROR_TEXT_CONTENT_EMPTY)
                        return
                    }
                    console.log("TYPEOF", val, typeof(val))
                    if(typeof(val) !== "number") {
                        try {
                            let reg = new RegExp("\s*")
                            let res = val.match(reg)
                            console.log("RES", val, reg, res)
                            if(res !== null && res !== []) {
                                reg = data.regex
                                if(reg === null)
                                    throw new Error(`No regex found for ${input}`)
                                res = µ.regextract(µ.drop_escaped_quotes(reg[0]), val, reg[1])
                                console.log("RES 2", val, reg, res)
                                if(res === val)
                                    throw new Error(`Parse don't work for ${input}`)
                            }
                        } catch ( err ) {
                            form_get(`${input}_btn_reg`).dispatchEvent(new Event("click"))
                            form_input_err(`${input}_btn_reg`, `${input}_err`,   fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
                            return
                        }
                    }
                    form_removeclass(`${input}_btn_reg`, "form_input_required")
                    form_msg(`${input}_err`, "") 
                }
                if(input === "body") {
                    let path = data.value
                    let body = fns_global.virtual_doc.body
                    let el = body.querySelector(`${path}`)
                    el.value = form_get(`positive_test_search_term_input`).value
                    while(el.localName !== "form" && el.id !== "sub_dom")
                        el= el.parentNode
                    el.addEventListener("submit", (evt) => {
                        evt.preventDefault()
                        return false
                    })
                    el.addEventListener("formdata", async (evt) => {
                        let url = el.action
                        console.log("BEFORE URL", url)
                        if(url.startsWith("moz-extension")) {
                            let domain_ext = µ.domain_part(url)
                            let headline = fns_start.data.headline.value
                            url = url.replace(domain_ext, headline)
                        }
                        console.log("AFTER URL", url)
                        let data = evt.formData
                        let body = await part_selector_build_post_request(data)
                        console.log("BODY", body)
                        await part_selector_fetch(url, "POST", body)
                        await part_selector_try_get_virtual_doc()
                        body = fns_global.virtual_doc.body
                        body = new DOMParser().parseFromString( body.innerHTML, "text/html")
                        body = body.children[0].children[1]
                        part_selector_depth_clean_virtual_doc(body.childNodes)
                        part_selector_depth_clean_virtual_doc(body.childNodes)
                        part_selector_child_have_text_content(body.childNodes)
                        part_selector_set_sub_dom(body)
                    })
                    el.submit()
                }
                form_display(`${input}_input`, false)
                form_display(`${input}_btn_confirm`, false)
                form_display(`${input}_btn_back`, true)
                fns_selector.mode === "json" && input === "results" ? form_display(`${input}_btn_not_define`, false) : null
                typeof(data.attr) !== "undefined" && fns_selector.mode !== "json" ? form_display(`${input}_btn_attr`, true) : null
                typeof(data.attr) !== "undefined" ? form_display(`${input}_btn_reg`, true) : null
                form_removeclass(`${input}_input`, "form_input_required")
                form_msg(`${input}_err`, "")
                part_selector_destroy_regex(input)
                part_selector_destroy_attr(input)
                data.test_passed = true
                part_selector_clear_selection()
                part_selector_update_data_form(input)
                if((fns_selector.old_index === null || fns_selector.old_index === fns_selector.current_index) && form_part_get_data(fns_selector, fns_selector.form_input[fns_selector.current_index - 1]).test_passed)
                {
                    part_selector_build_input()
                }
                else
                    part_selector_input_focus()
                add_property(fns_global.new_source_user, input, data.value)
                form_part_validator(fns_selector, "finish_selection_part")
            }
            form_display(`${input}_id`, true, "inline-block")
            fns_selector.current_index += 1
        }

    }
    /**
     * Update the input data in fns_selector.
     * @param {string} input name of the input 
     */
    function part_selector_update_data_form(input) {
        let data = fns_selector.data[`${input}`]
        if(input === "r_url") {
            let path = data.value
            let elements = part_selector_get_sub_dom_element(`${path}`)
            console.log(elements)
        }
        if(typeof(data) !== "undefined" && data.value !== "") {
            let content = part_selector_sub_dom_get_element_text(input)
            if(input === "results") {
                let path = data.value
                let elements = part_selector_get_sub_dom_element(`${path}`)
                form_msg(`${input}_final`, `${elements.length} results in the web page`)
                form_set(`${input}_final`, `${path} (${elements.length})`, "title")
            } else  {
                form_msg(`${input}_final`, content.value)
                form_set(`${input}_final`, `${content.name}="${content.value}"`, "title")
            }
            if(typeof(data.regex) !== "undefined") {
                if(data.type_regex === "string" && data.regex !== null) {
                    let reg = data.regex
                    let res = µ.regextract(µ.drop_escaped_quotes(reg[0]), content.value, reg[1])
                    form_msg(`${input}_final`, res)
                    form_set(`${input}_final`, `${content.name}="${content.value}"`, "title")
                    form_msg(`${input}_final_reg`, `${reg[0]}`)
                    form_set(`${input}_final_reg`, `["${reg[0]}", "${reg[1]}"]`, "title")
                } else if(data.type_regex === "list" && data.regex.length !== 0) {
                    let reg = data.regex[0]
                    let res = mµ.parse_dt_str(
                        content.value, 
                        fns_timezone.data.tags_tz.value,
                        1, 
                        {
                            r_dt_fmt_1 : reg
                        }, 
                        fns_global.month_nb_json)
                    form_msg(`${input}_final`, res)
                    form_set(`${input}_final`, `${content.name}="${content.value}"`, "title")
                    let length = data.regex.length
                    if(length > 1) {
                        form_msg(`${input}_final_reg`, `(${reg[0]})`)
                        form_set(`${input}_final_reg`, `["${reg[0]}", "${reg[1]}"]`, "title")
                    } else  {
                        form_msg(`${input}_final_reg`, `(${reg[0]})`)
                        form_set(`${input}_final_reg`, `["${reg[0]}", "${reg[1]}"] +${length - 1}`, "title")
                    }
                } else {
                    form_msg(`${input}_final_reg`, "")
                    form_set(`${input}_final_reg`, "", "title")
                }
            }
            if(typeof(data.attr) !== "undefined") {
                if(data.attr !== null) {
                    if(content.name !== "textContent") {
                        form_msg(`${input}_final_attr`, `${content.name}`)
                        form_set(`${input}_final_attr`, `${content.name}="${content.value}"`, "title")
                    } else {
                        form_msg(`${input}_final`, content.value)
                        form_set(`${input}_final`, `${content.name}="${content.value}"`, "title")
                    }
                } else {
                    form_msg(`${input}_final_attr`, "") 
                    form_set(`${input}_final_attr`, "", "title")
                }
            }
        }
    }
    /**
     * Get a text from a attribute in the element.
     * @param {string} input Name of the input.
     * @returns {object} object with name and value of attribute.
     */
    function part_selector_sub_dom_get_element_text(input) {
        let data = fns_selector.data[`${input}`]
        let path = µ.triw(data.value)
        if(fns_selector.mode === "get" || fns_selector.mode === "post") {
            let el = form_get("sub_dom").querySelector(`${path}`)
            let attr = data.attr
            attr = µ.triw(attr)
            if(attr === null)
                return {name : "textContent", value: el.textContent}
            else
                return { name : attr, value : el.getAttribute(`${attr}`) }
        } else if ( fns_selector.mode === "json") {
            let el = fns_global.virtual_doc
            path = path.split(".")
            for(let p of path) {
                if(typeof(el.length) === "undefined")
                    el = el[`${p}`]
                else
                    el = el[0][`${p}`]
            }
            return {name : path, value: el}
        }

    }
    /**
     * Show in user the tree of this previous selection in the sub dom.
     * @param {string} json_path json path to show.
     * @returns {HTMLElement} the last element.
     */
    function part_selector_json_show(json_path) {
        let render = form_get("sub_dom").querySelector(".renderjson")
        let el = render.querySelector("span")
        let path = json_path.split(".")
        let count = 0
        while( count < path.length) {
            let child = null
            for(let c of el.childNodes) {
                if(typeof(c.classList) === "undefined")
                    continue
                if(c.classList.contains("object") && c.classList.length === 1){
                    child = c
                    break
                } else if(c.classList.contains("array") && c.classList.length === 1 ) {
                    child = c
                    break
                }
            }
            child.querySelector(".disclosure").dispatchEvent(new Event("click"))
            child = child.nextSibling
            if(child.classList[0] === "object") {
                let key = null
                for(let c of child.childNodes) {
                    if(c.textContent === `"${path[count]}"`) {
                        key = c
                        break
                    }
                }
                el = key.nextSibling.nextSibling
            } else if( child.classList[0] === "array") {
                el = child.childNodes[3]
                continue
            }
            count += 1
        }
        return el
    }
    /**
     * Give a good focus to input for selector in sub dom.
     */
    function part_selector_input_focus() {
        let index = null
        for(let key of fns_selector.form_input) {
            let inp = fns_selector.data[key]
            if(!inp.methode.includes(fns_selector.mode))
                continue
            if(inp.test_passed)
                continue
            if(inp.type === "optionnal" && inp.is_passed)
                continue
            index = fns_selector.form_input.indexOf(key)
            break
        }
        if(index !== null) {
            if(fns_selector.old_index === null) {
                fns_selector.old_index = fns_selector.current_index
            } else {
                fns_selector.old_index = fns_selector.old_index === index + 1 ? null : fns_selector.old_index
            }
            fns_selector.current_index = index + 1
            let input = fns_selector.form_input[fns_selector.current_index - 1]
            let data = fns_selector.data[input]
            part_selector_new_sub_dom_listener(input, data)
        }
    }
    /**
     * Add a event listener for the sub dom.
     * @param {*} input input focused.
     * @param {*} data data of input.
     */
    function part_selector_new_sub_dom_listener(input, data) {
        let sub_dom_event = (evt) => { 
            let el = evt.target
            part_selector_clear_selection()
            let path 
            if(fns_selector.mode === "get" || fns_selector.mode === "post") {
                if(!el.classList.contains(`${data.css_class}`))
                    el.classList.add(`${data.css_class}`)
                path = part_selector_get_css_path(el, data.css_class)
            } else if( fns_selector.mode === "json")
                path = part_selector_get_path_json(el)
			let re = '^.*(#[^#]*)$'
			path = µ.regextract(re, path)
            form_set(`${input}_input`, path, "value")
        }
        part_selector_remove_sub_dom_listener(sub_dom_event)
        form_get("sub_dom").onclick = sub_dom_event
    }
    /**
     * Remove previous event listener
     */
    function part_selector_remove_sub_dom_listener(sub_dom_event) {       
        form_get("sub_dom").removeEventListener("click", sub_dom_event)
        /** 
        let old_element = form_get("sub_dom");
        let new_element = old_element.cloneNode(true);
        old_element.parentNode.replaceChild(new_element, old_element);
        if(fns_selector.mode === "json")
            part_selector_set_sub_dom(fns_global.virtual_doc)
        */
    }
    /**
     * Remove all class from fns_selector. 
     */
    function part_selector_clear_selection() {
        for(let cl of fns_selector.class_list) {
            let old_el = document.getElementsByClassName(`${cl}`)[0]
            if(old_el !== undefined)
                old_el.classList.remove(`${cl}`)
        }
    }
    /**
     * Find a CSS path for a specified node in sub_dom.
     * @param {HTMLElement} element node to determine a path 
     * @param {string} css_class class to ignore
     * @returns {string} CSS path.
     */
    function part_selector_get_css_path(element, css_class){
        let path = []
        let el = element
        while(el.id !== "sub_dom") {
            let tmp = ""
            if(el.id){
                tmp = `#${el.id}`
            } else if(el.classList.length !== 0 && el.classList[0] !== css_class && el.classList[0] !== fns_selector.sub_dom.class_show_div ) {
                tmp = `.${el.classList[0]}`
            } else {
                tmp = `${el.localName}`
            }
            path.push(tmp)
            el = el.parentNode
        }
        path = path.reverse()
        return path.join(" > ")
    }
        /**
     * Find a JSON path for a specified node in sub_dom.
     * @param {HTMLElement} el node to determine a path 
     * @returns {string} JSON path.
     */
    function part_selector_get_path_json(el) {
        let path = []
        path.push(el.textContent.replace(/"/gi, ''))
        while(el.classList[0] !== "renderjson") {
            el = el.parentNode
            if(el.classList[0] === "object" || el.classList[0] === "array") {
                el = el.parentNode
                if(el.parentNode.classList[0] !== "renderjson") {
                    let pre = el
                    while( el !== null && el.classList[0] !== "key" ) {
                        pre = el
                        el = el.previousSibling
                    }
                    if(el === null)
                        el = pre
                }
            }
            if(el.classList[0] === "key")
                path.push(el.textContent.replace(/"/gi, ''))
        }
        path = path.reverse()
        return path.join(".")
    }
    /**
     * Return a list of attributes for a element in sub dom.
     * @param {object} data input data in fns_selector data.
     * @returns {array} list of attributes.
     */
    function part_selector_get_content(data) {
        let el = form_get("sub_dom").querySelector(data.value)
        let black_list = [/^id/, /^class/]
        let attrs = []
        if(el.textContent !== "")
            attrs.push({name: "textContent", value: el.textContent})
        for(let attr of el.attributes) {
            let ok = true
            for(let reg of black_list) {
                if(attr.localName.match(reg) !== null)
                    ok = false
            }
            if(ok)
                attrs.push({name: attr.localName, value: attr.value})
        }
        return attrs
    }
    /**
     * Init a attr selection part in selector part from dynamic form.
     * @param {array} contents lists of attributes. 
     * @param {*} input input in selector sub dom link to this attributes. 
     */
    function part_selector_init_attr(contents, input) {
        let list = [
            {node: "div", id:`${input}_attr`},
                {node: "ul", id: `${input}_attr_list`, parent:0 , classList: ["form_list"]},
                    {node: "div", id:`${input}_attr_placholder`, parent: 1, classList: ["form_item_placeholder"], textContent: "Choose a good value"}
        ]
        let builder = form_builder(list)
        let parent = form_get(`${input}_id`)
        parent.appendChild(builder.dom.firstChild)
        let count = 0
        for(let content of contents) {
            let item = [
                {node: "li", id: `${input}_attr_item_${count}`, classList: ["form_item_list"], title: `${content.name}="${content.value}"`},
                {node: "span", id: `value_attr_item_${count}`, parent: 0, textContent: content.value, classList: ["form_item_text"]}
            ]
            builder = form_builder(item)
            parent = form_get(`${input}_attr_list`)
            parent.appendChild(builder.dom.firstChild)
            console.log('parent2',parent.outerHTML)
            parent.querySelector(`#${input}_attr_item_${count}`).onclick = (evt) => {
                if(content.name !== "textContent") {
                    let data = fns_selector.data[`${input}`]
                    data.attr = content.name
                    add_property(fns_global.new_source_user, `${input}_attr`, data.attr)
                } else {
                    remove_property(fns_global.new_source_user, `${input}_attr`)
                }
                part_selector_destroy_attr(input)
                form_get(`${input}_btn_confirm`).dispatchEvent(new Event("click"))
            }
            count += 1
        }
       
    }
    /**
     * Destroy a attr part in selector part from dynamic form.
     * @param {string} input input in selector sub dom.
     */
    function part_selector_destroy_attr(input) {
        if(form_is_exist(`${input}_attr`)) {
            let child = form_get(`${input}_attr`)
            child.parentNode.removeChild(child)
        }

    }
    /**
     * Init a regex part in selector part from dynamic form.
     * @param {string} input input in selector sub dom.
     * @param {object} data input data from fns_selector data.
     * @param {string} preview text to parse with regex. 
     */
    function part_selector_init_regex(input, data, preview) {
        part_selector_destroy_regex(input)
        let template = [
            {node: "div", id: `${input}_reg`, classList: ["form_list"]},
                {node: "div", id : `${input}_reg_placeholder`, classList: ["form_item_placeholder"], 
                textContent: "Created a regex", parent: 0},
                {node: "ul", id: `${input}_reg_list`, parent: 0},
                {node: "p", id: `${input}_reg_preview_text`, parent: 0},
                    {node: "span", textContent: `Current selected text : `, parent: 3},
                    {node: "span", id: `${input}_reg_preview`, textContent: `${preview}`, parent: 3},
                {node: "div", id: `${input}_reg_input_part`, classList: ["input_container"], parent: 0},
                    {node: "input", id: `${input}_reg_input`, type:"text", 
                    classList:["form_input_text", "form_input_text_reg"], 
                    placeholder:fns_selector.text.MSG_TEXT_REG_PLACEHOLDER, parent: 6},
                    {node: "input", id: `${input}_reg_input_token`, type:"text", 
                    classList:["form_input_text", "form_input_text_reg"], 
                    placeholder:fns_selector.text.MSG_TEXT_TOKEN_PLACEHOLDER, value:"$1", parent: 6},
                    {node: "button", id: `${input}_reg_input_btn_add`, classList: ["btn", "a", "form_selection_input_btn"], 
                    textContent: "Add", parent: 6},
                    {node: "button", id: `${input}_reg_input_btn_reset`, classList: ["btn", "a", "form_selection_input_btn"], 
                    textContent: "RAZ", parent: 6},
                {node: "p", id: `${input}_reg_err`, classList:["form_input_error"], parent: 0},
                {node: "p", id: `${input}_reg_result`, parent: 0}
        ]
        let builder = form_builder(template)
        form_get(`${input}_err`).parentElement.appendChild(builder.dom.firstChild)
        form_get(`${input}_reg_input_token`).addEventListener("input", (evt) => {
            form_get(`${input}_reg_input`).dispatchEvent(new Event("input"))
        })
        form_get(`${input}_reg_input`).addEventListener("input", (evt) => {
            let val = evt.target.value
            val = µ.drop_escaped_quotes(val)
            if(val === "") {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let token = µ.triw(form_get(`${input}_reg_input_token`).value)
            if(token === "") {
                form_input_err(`${input}_reg_input_token`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            try {
                new RegExp(val)
            } catch(ex) {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let res
            if(input === "r_dt") {
                try {
                    res = mµ.parse_dt_str(
                        preview, 
                        fns_timezone.data.tags_tz.value,
                        1, 
                        {
                            r_dt_fmt_1 : [
                                val,
                                token
                            ]
                        }, 
                        fns_global.month_nb_json)
                } catch(err) {
                    form_input_err(`${input}_reg_input`, `${input}_reg_err`, fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
                    form_msg(`${input}_reg_result`, "")
                    return
                }
            } else {
                res = µ.regextract(val, preview, token)
            }
            if(res === preview || res === "") {
                form_addclass(`${input}_reg_input`, "form_input_required")
                form_addclass(`${input}_reg_input_token`, "form_input_required")
                form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
                form_msg(`${input}_reg_result`, "")
                return
            }
            form_msg(`${input}_reg_result`, `Result of parse : ${res}`)
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_removeclass(`${input}_reg_input_token`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
        })
        form_get(`${input}_reg_input_btn_add`).onclick = (evt) => {
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
            let reg_val = form_get(`${input}_reg_input`).value
            reg_val = µ.drop_escaped_quotes(reg_val)
            if(reg_val === "") {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let token_val = µ.triw(form_get(`${input}_reg_input_token`).value)
            if(token_val === "") {
                form_input_err(`${input}_reg_input_token`, `${input}_reg_err`,  fns_selector.text.MSG_ERROR_VALUE_EMPTY)
                form_msg(`${input}_reg_result`, "")
                return
            }
            try {
                new RegExp(reg_val)
            } catch(ex) {
                form_input_err(`${input}_reg_input`, `${input}_reg_err`,  fns_selector.text.MSG_ERROR_REGEX_NOT_VALID)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let res
            if(input === "r_dt") {
                try {
                    res = mµ.parse_dt_str(
                        preview, 
                        fns_timezone.data.tags_tz.value,
                        1, 
                        {
                            r_dt_fmt_1 : [
                                reg_val,
                                token_val
                            ]
                        }, 
                        fns_global.month_nb_json)
                } catch(err) {
                    form_input_err(`${input}_reg_input`, `${input}_reg_err`,  err)
                    form_msg(`${input}_reg_result`, "")
                    return
                }
            } else {
                res = µ.regextract(reg_val, preview, token_val)
            }
            if(res === preview) {
                form_addclass(`${input}_reg_input`, "form_input_required")
                form_addclass(`${input}_reg_input_token`, "form_input_required")
                form_msg(`${input}_reg_err`, fns_selector.text.MSG_ERROR_FAIL_TO_PARSE)
                form_msg(`${input}_reg_result`, "")
                return
            }
            let val = [reg_val, token_val]
            if(data.type_regex === "string")
                data.regex = val
            else if (data.type_regex === "list") {
                if(data.index_regex !== -1) {
                    data.regex[data.index_regex] = val
                    data.index_regex = -1
                } else {
                    data.regex.push(val)
                }
            } else
                throw new Exception(`You have forget to define "type_regex" property for "${input}" key in data fns_selector`)
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_removeclass(`${input}_reg_input_token`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
            part_selector_update_item_regex(input, data)
            form_set( `${input}_reg_input`, "", "value")
            form_set( `${input}_reg_input_token`, "$1", "value")
            if(data.type_regex === "string")
                part_selector_destroy_regex(input)
            if(input === "r_dt") {
                part_selector_set_dt_fmt(data.regex)
            } else {
                add_property(fns_global.new_source_user, `${input}_re`, data.regex)
            }
            part_selector_update_data_form(input)
            form_get(`${input}_btn_confirm`).dispatchEvent(new Event("click"))
        }
        form_get(`${input}_reg_input_btn_reset`).onclick = (evt) => {
            form_removeclass(`${input}_reg_input`, "form_input_required")
            form_removeclass(`${input}_reg_input_token`, "form_input_required")
            form_msg(`${input}_reg_err`, "")
            form_set( `${input}_reg_input`, "", "value")
            form_set( `${input}_reg_input_token`, "$1", "value")
            fns_selector.data[`${input}`].index_regex = -1
        }
        part_selector_update_item_regex(input, data)
    }
    /**
     * Destroy a regex part in selector part from dynamic form.
     * @param {*} input input in selector sub dom.
     */
    function part_selector_destroy_regex(input) {
        if(form_is_exist(`${input}_reg`)) {
            let child = form_get(`${input}_reg`)
            child.parentNode.removeChild(child)
        }
    }
    /**
     * Create a list item regex.
     * @param {string} input input in selector sub dom.
     * @param {array} reg the list with regex and token defintion.
     * @param {number} number index of the item.
     * @param {HTMLElement} list node to add the item build.
     */
    function part_selector_create_item_regex(input, reg, number, list) {
        let template_item = [
            {node: "li", id: `${input}_reg_item_${number}`, classList: ["form_item_list"]},
            {node: "span", id: `${input}_reg_value_${number}`, textContent: `${reg[0]}`, 
            parent: 0, classList: ["form_item_text"], title: `Regex : "${reg[0]}", Token: "${reg[1]}"`},
            {node: "button", id: `${input}_reg_btn_edit_${number}`, classList: ["form_item_btn"], 
            innerHTML: fns_global.icons.pen, parent:0},
            {node: "button", id: `${input}_reg_btn_delete_${number}`, classList: ["form_item_btn"],
            innerHTML: fns_global.icons.cross, parent:0}
        ]
        let builder = form_builder(template_item)
        let item = builder.dom.firstChild
        list.appendChild(item)
        form_get(`${input}_reg_btn_edit_${number}`).onclick = (evt) => {
            let data = fns_selector.data[`${input}`]
            let reg = null
            if(data.type_regex === "string") {
                reg = data.regex
            } else if(data.type_regex === "list") {
                let index = Number.parseInt(evt.target.id.split("_").pop())
                data.index_regex = index
                reg = data.regex[index]
            }
            if(input === "r_dt") {
                part_selector_set_dt_fmt(data.regex)
            } else {
                add_property(fns_global.new_source_user, `${input}_re`, data.regex)
            }
            form_set( `${input}_reg_input`,  reg[0], "value")
            form_set( `${input}_reg_input_token`, reg[1], "value")
        }
        form_get(`${input}_reg_btn_delete_${number}`).onclick = (evt) => {
            let data = fns_selector.data[`${input}`]
            if(data.type_regex === "string") {
                data.regex = null
                remove_property(fns_global.new_source_user, `${input}_re`)
            } else if(data.type_regex === "list") {
                let index = Number.parseInt(evt.target.id.split("_").pop())
                if(index === data.index_regex)
                    data.index_regex = -1
                else if (index < data.index_regex)
                    data.index_regex -= 1
                data.regex = data.regex.filter( (val, i) => {return i !== index})
                part_selector_set_dt_fmt(data.regex)
            }
            part_selector_update_item_regex(input, data)
        }
    }
    /**
     * Update a item regex.
     * @param {} input input in selector sub dom.
     * @param {*} data input data from fns_selector data.
     */
    function part_selector_update_item_regex(input, data) {
        let list = form_get(`${input}_reg_list`)
        list.innerHTML = ""
        if(data.regex !== null && data.regex !== []) {
            let count = 0
            if(data.type_regex === "string") {
                part_selector_create_item_regex(input, data.regex, count, list)
            } else if(data.type_regex === "list") {
                for(let reg of data.regex) {
                    part_selector_create_item_regex(input, reg, count, list)
                    count += 1
                }
            } else
                throw new Exception(`You have forget to define "type_regex" property for "${input}" key in data fns_selector`)
        }
    }
    /**
     * Set all dates formats definitions in new user object.
     * @param {array} regex list of regex definitions.
     */
    function part_selector_set_dt_fmt(regex) {
        let count = 1
        fns_global.new_source_user[``]
        while( typeof(fns_global.new_source_user[`r_dt_fmt_${count}`]) !== "undefined" ) {
            remove_property(fns_global.new_source_user, `r_dt_fmt_${count}`)
            count += 1
        }
        count = 1
        for(let dt of regex) {
            add_property(fns_global.new_source_user, `r_dt_fmt_${count}`, dt)
            count += 1
        }
    }
    /**
     * Build the object with all dates formats.
     * @param {object} data input data in fns_selector data.
     * @returns {object} object with the all dates formats and the number of them.
     */
    function part_selector_build_fmt_date_obj(data) {
        let fmts = {}
        let count = 0
        for(let reg of data.regex) {
            count += 1
            add_property(fmts, `r_dt_fmt_${count}`, reg)
        }
        return {
            fmts : fmts,
            count: count
        }
    }
    /**
     * Get the list of elements with a specified path.
     * @param {string} path path of the element. 
     * @returns {array} list of elemnts in sub dom.
     */
    function part_selector_get_sub_dom_element(path) {
        let sub_dom = form_get("sub_dom")
        if(fns_selector.mode === "get" || fns_selector.mode === "post") {
            return sub_dom.querySelectorAll(path)
        } else if(fns_selector.mode === "json") {
            path = path.split(".")
            let json = fns_global.virtual_doc
            for(let p of path) {
                json = json[p]
            }
            return json
        }
    }
    /**
     * Set the new HTML tree in the sub dom.
     * @param {string} src the new HTML tree to set. 
     */
    function part_selector_set_sub_dom(src) {
        let sub_dom = form_get("sub_dom")
        sub_dom.innerHTML = ""
        if(fns_selector.mode === "get" || fns_selector.mode === "post") {
            sub_dom.innerHTML = src.innerHTML
            let links = sub_dom.querySelectorAll("a")
            for(let link of links) {
                link.setAttribute("disabled", "true")
                link.disabled = true
                link.onclick = (evt) => {
                    return false
                 }
            }
        } else if(fns_selector.mode === "json") {
            sub_dom.appendChild( renderjson(src) ) 
        }
    }
    /**
     * Init the tags part of dynamic form.
     */
    function part_tags_init() {
        fns_tags.doc.fieldset = form_get("part_tags_id")
        fns_tags.doc.legend = form_get("legend_part_tags") 
        form_msg("legend_part_tags_icon", `${fns_global.icons.triangular}`)
        form_msg("legend_part_tags_title", `${fns_tags.title}`)
        part_tags_create_btn_save()
        part_tags_create_tech_accuracy()
        part_tags_create_tech_search_system()
        part_tags_create_tech_search_speed()
        part_tags_create_themes()
        part_tags_create_src_type()
        part_tags_create_res_type()
        part_tags_event()
        form_display(fns_tags.doc.fieldset.id, true, "block")
    }
    /**
     * Return a list with all unique values of specfied key from the file source.json.
     * @param {string} key name of the property in object from the file source.json. 
     * @returns {array} array with all unique values of this property.
     */
    function part_tags_init_list(key) {
        let items = []
        let sources = fns_global.source_json
        for(let [k, obj] of Object.entries(sources)){
            if(obj.tags[key] !== undefined){
                if(typeof(obj.tags[key]) === "string")
                !items.includes(obj.tags[key]) ? items.push(obj.tags[key]) : null
            else
                items = [...items, ...obj.tags[key].filter((v) => !items.includes(v))]
            }
        }
        return items.sort()
    }
    /**
     * Return a list of values from input choice in fns_tags.
     * @param {string} key name of input choice in fns_tags. 
     * @returns {array} List of values from input choice.
     */
    function part_tags_get_list(key) {
        let items = fns_tags.choice[key].choiceList.element
        let list = []
        for(let item of items.childNodes) {
            item = item.getAttribute("data-value")
            if(!list.includes(item))
                list.push(item)
        }
        return list.sort()
    }
    /**
     * Add to value in the input choice.
     * @param {string} key name of input choice in fns_tags. 
     * @param {string} val value to add.
     */
    function part_tags_update(key, val) {
        fns_tags.choice[key].setValue([
            {
                label : val.charAt(0).toUpperCase() + val.substr(1).toLowerCase(), 
                value: val
            }
        ])
    }
    /**
     * Create event to tags part.
     */
    function part_tags_event() {
    		form_toggle(fns_tags.doc.fieldset)
        form_toggle_event(fns_tags)
    }
    /**
     * Create a template for choice list for themes of the source.
     */
    function part_tags_create_themes() {
        let template = [
            {node : "div", id : "part_tags_themes_id"},
                {node : "div", id : "part_tags_themes_choice", classList: ["input_container"], parent: 0},
                    {node : "h3", id:"part_tags_themes_title", textContent : fns_tags.text.MSG_TEXT_TITLE_THEMES, parent : 1},
                    {node : "select", id : "part_tags_themes_select", multiple:"", parent : 1},
                {node : "div", id : "part_tags_themes_new", parent : 0},
                    {node : "button", id : "part_tags_themes_btn_new", 
                    textContent : fns_tags.text.MSG_TEXT_THEMES_BTN_USER_INPUT, classList: ["btn", "a"], parent: 4}, 
                {node : "div", id : "part_tags_themes_add", style:"display:none;", parent: 0},
                    {node : "input", id : "part_tags_themes_input_add", type:"text", 
                    placeholder:fns_tags.text.MSG_TEXT_PLACHOLDER_THEMES_INPUT, classList: ["form_input_text"], parent : 6},
                    {node : "button", id : "part_tags_themes_btn_add", textContent : "ADD", classList: ["btn", "a"], parent: 6},
                    {node:"p", id:`part_tags_themes_tips`, classList: ["form_input_tips"], 
                    textContent : fns_tags.text.MSG_TIPS_THEMES_INPUT_ADD, parent:6},
                    {node:"button", id:`part_tags_themes_display_tips`, innerHTML: fns_global.icons.question, 
                    classList: ["btn", "a", "form_selection_input_btn"], parent:6},
                {node:"p", id:`part_tags_themes_err`, classList: ["form_input_error"], parent:0}

        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        form_insert(el, fns_tags.doc.fieldset)
        part_tags_build_themes()
        part_tags_event_themes()
        for( let theme of fns_tags.data.themes.value ) {
            fns_tags.choice.themes.setChoiceByValue(theme)
        }
    }
    /**
     * Build a choice list for themes of the source with choice.js.
     */
    function part_tags_build_themes() {
        let themes = part_tags_init_list("themes")
        let element = form_get("part_tags_themes_select")
        for(let theme of themes) {
            let option = document.createElement('option')
            option.text = theme.charAt(0).toUpperCase() + theme.substr(1).toLowerCase()
            option.value = theme
            element.add(option)
        }
        let select_opt = {
            removeItemButton: true,
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : true
        }
        fns_tags.choice.themes = new Choices(element, Object.assign(select_opt))
    }
    /**
     * Create a event for themes part in tags part.
     */
    function part_tags_event_themes() {
        fns_tags.choice.themes.passedElement.element.addEventListener("addItem", (evt) => {
            let theme
            try {
                theme = evt.detail.value
                if(theme === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.error(ex)
                return
            }
            if(!part_tags_get_list("themes").includes(theme)) {
                fns_tags.data.themes.value.push(theme)
                fns_tags.data.themes.test_passed = fns_tags.data.themes.value.length !== 0
            }
            add_property(fns_global.new_source_tags_user, "themes", fns_tags.data.themes.value)
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        fns_tags.choice.themes.passedElement.element.addEventListener("removeItem", (evt) => {
            let theme
            try {
                theme = evt.detail.value
                if(theme === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.error(ex)
                return
            }
            fns_tags.data.themes.value = fns_tags.data.themes.value.filter( (th) => {return th !== theme})
            if(fns_tags.data.themes.value.length === 0) {
                fns_tags.data.themes.test_passed = false
                remove_property(fns_global.new_source_tags_user, "themes")
            } else {
                add_property(fns_global.new_source_tags_user, "themes", fns_tags.data.themes.value)
            }
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_themes_btn_new").onclick = (evt) => {
            form_display("part_tags_themes_add", true)
            form_display("part_tags_themes_new", false)
         }
        form_get("part_tags_themes_btn_add").onclick = (evt) => {
            let theme = form_get("part_tags_themes_input_add").value
            if(theme === "") {
                form_input_err("part_tags_themes_input_add", "part_tags_themes_err", fns_tags.text.MSG_ERROR_VALUE_EMPTY)
                return
            }
            theme = theme.toLowerCase()
            part_tags_update("themes", theme)
            form_addclass("part_tags_themes_input_add", "form_input_error")
            form_msg( "part_tags_themes_err", "")
            form_display("part_tags_themes_new", true)
            form_display("part_tags_themes_add", false)
        }
        form_get("part_tags_themes_display_tips").onclick = (evt) => {
            let display = form_get("part_tags_themes_display_tips")
            let tips = form_get("part_tags_themes_tips")
            form_tips(display, tips, fns_start.text.MSG_TIPS_THEMES_INPUT_ADD)
         }
    }
    /**
     * Create a template for tech accuracy part in tags part. 
     */
    function part_tags_create_tech_accuracy() {
        let template = [
            {node : "div", id : "part_tags_tech_accuracy"},
                {node : "div", id : "part_tags_tech_accuracy_checkbox", parent : 0},
                    {node : "h3", id:"part_tags_tech_accuracy_title", textContent : fns_tags.text.MSG_TEXT_TITLE_TECH_ACCURACY, parent : 1},
                    {node : "input", id: "part_tags_tech_accuracy_one_word_input", name : "checkbox_one_word",
                    type: "checkbox", value: "one word", parent: 1},
                    {node : "label", id: "part_tags_tech_accuracy_one_word_label", for: "checkbox_one_word", textContent: "One Word", parent: 1},
                    {node : "input", id: "part_tags_tech_accuracy_many_words_input", name : "checkbox_many_words",
                    type: "checkbox", value: "many words", parent: 1},
                    {node : "label", id: "part_tags_tech_accuracy_many_words_label", for: "checkbox_many_words", textContent: "Many Words", parent: 1},
                    {node : "input", id: "part_tags_tech_accuracy_exact_input", name : "checkbox_exact",
                    type: "checkbox", value: "exact", parent: 1},
                    {node : "label", id: "part_tags_tech_accuracy_exact_label", for: "checkbox_exact", textContent: "Exact", parent: 1},
                    {node : "input", id: "part_tags_tech_accuracy_approx_input", name : "checkbox_approx",
                    type: "checkbox", value: "approx", parent: 1},
                    {node : "label", id: "part_tags_tech_accuracy_approx_label", for: "checkbox_approx", textContent: "Approx", parent: 1}
        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        form_insert(el, fns_tags.doc.fieldset)
        part_tags_event_tech_accuracy()
        form_set("part_tags_tech_accuracy_one_word_input",  fns_tags.data.tech.value.includes("one word"), "checked")
        form_set("part_tags_tech_accuracy_many_words_input",  fns_tags.data.tech.value.includes("many words"), "checked")
        form_set("part_tags_tech_accuracy_exact_input",  fns_tags.data.tech.value.includes("exact"), "checked")
        form_set("part_tags_tech_accuracy_approx_input",  fns_tags.data.tech.value.includes("approx"), "checked")
    }
    /**
     * Create a event for tech accuracy part in tags part.
     */
    function part_tags_event_tech_accuracy() {
        form_get("part_tags_tech_accuracy_one_word_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(evt.target.checked) {
                if(!fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value.push(value)
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            } else {
                if(fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value = fns_tags.data.tech.value.filter( (t) => {return t !== value})
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            }
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_tech_accuracy_many_words_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(evt.target.checked) {
                if(!fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value.push(value)
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            } else {
                if(fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value = fns_tags.data.tech.value.filter( (t) => {return t !== value})
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            }
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_tech_accuracy_exact_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(evt.target.checked) {
                if(fns_tags.data.tech.value.includes("approx"))
                    form_set("part_tags_tech_accuracy_approx_input", false, "checked", "change")
                if(!fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value.push(value)
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            } else {
                if(fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value = fns_tags.data.tech.value.filter( (t) => {return t !== value})
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            }
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_tech_accuracy_approx_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(evt.target.checked) {
                if(fns_tags.data.tech.value.includes("exact"))
                    form_set("part_tags_tech_accuracy_exact_input", false, "checked", "change")
                if(!fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value.push(value)
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            } else {
                if(fns_tags.data.tech.value.includes(value))
                    fns_tags.data.tech.value = fns_tags.data.tech.value.filter( (t) => {return t !== value})
                add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            }
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
    }
    /**
     * Create a template for tech search system part in tags part. 
     */
    function part_tags_create_tech_search_system() {
        let template = [
            {node : "div", id : "part_tags_tech_search_system"},
                {node : "div", id : "part_tags_tech_search_system_radio", parent : 0},
                    {node : "h3", id:"part_tags_tech_search_system_title", textContent : fns_tags.text.MSG_TEXT_TITLE_TECH_SEARCH_SYSTEM, parent : 1},
                    {node : "input", id: "part_tags_tech_search_system_internal_input", name : "radio_tech_search",
                    type: "radio", value: "internal search", parent: 1},
                    {node : "label", id: "part_tags_tech_search_system_internal_label", for: "part_tags_tech_search_system_internal_input", textContent: "Internal", parent: 1},
                    {node : "input", id: "part_tags_tech_search_system_external_input", name : "radio_tech_search",
                    type: "radio", value: "external search", parent: 1},
                    {node : "label", id: "part_tags_tech_search_system_external_label", for: "part_tags_tech_search_system_external_input", textContent: "External", parent: 1}
        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        form_insert(el, fns_tags.doc.fieldset)
        part_tags_event_tech_search_system()
        form_set("part_tags_tech_search_system_internal_input", fns_tags.data.tech.value.includes("internal search"), "checked")
        form_set("part_tags_tech_search_system_external_input", fns_tags.data.tech.value.includes("external search"), "checked")
    }
    /**
     * Create a event for tech search system part in tags part.
     */
    function part_tags_event_tech_search_system() {
        form_get("part_tags_tech_search_system_internal_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(fns_tags.data.tech.value.includes("external search"))
                fns_tags.data.tech.value = fns_tags.data.tech.value.filter((t) => {return t !== "external search"})
            fns_tags.data.tech.value.push(value)
            add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_tech_search_system_external_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(fns_tags.data.tech.value.includes("internal search"))
                fns_tags.data.tech.value = fns_tags.data.tech.value.filter((t) => {return t !== "internal search"})
            fns_tags.data.tech.value.push(value)
            add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
    }
    /**
     * Create a template for src type list for src type of the source.
     */
    function part_tags_create_src_type() {
        let template = [
            {node : "div", id : "part_tags_src_type_id"},
                {node : "div", id : "part_tags_src_type_choice", classList: ["input_container"], parent: 0},
                    {node : "h3", id:"part_tags_src_type_title", textContent : fns_tags.text.MSG_TEXT_TITLE_SRC_TYPE, parent : 1},
                    {node : "select", id : "part_tags_src_type_select", multiple:"", parent : 1},
                {node : "div", id : "part_tags_src_type_new", parent : 0},
                    {node : "button", id : "part_tags_src_type_btn_new", 
                    textContent : fns_tags.text.MSG_TEXT_SRC_TYPE_BTN_USER_INPUT, classList: ["btn", "a"], parent: 4}, 
                {node : "div", id : "part_tags_src_type_add", style:"display:none;", parent: 0},
                    {node : "input", id : "part_tags_src_type_input_add", type:"text", 
                    placeholder:fns_tags.text.MSG_TEXT_PLACHOLDER_SRC_TYPE_INPUT, classList: ["form_input_text"], parent : 6},
                    {node : "button", id : "part_tags_src_type_btn_add", textContent : "ADD", classList: ["btn", "a"], parent: 6},
                    {node:"p", id:`part_tags_src_type_tips`, classList: ["form_input_tips"],
                    textContent : fns_tags.text.MSG_TIPS_SRC_TYPE_INPUT_ADD, parent:6},
                    {node:"button", id:`part_tags_src_type_display_tips`, innerHTML: fns_global.icons.question, 
                    classList: ["btn", "a", "form_selection_input_btn"], parent:6},
                {node:"p", id:`part_tags_src_type_err`, classList: ["form_input_error"], parent:0}
        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        form_insert(el, fns_tags.doc.fieldset)
        part_tags_build_src_type()
        part_tags_event_src_type()
        for(let type of fns_tags.data.src_type.value ) {
            fns_tags.choice.src_type.setChoiceByValue(type)
        }
    }
    /**
     * Build a choice list for src type of the source with choice.js.
     */
    function part_tags_build_src_type() {
        let src_type = part_tags_init_list("src_type")
        let element = form_get("part_tags_src_type_select")
        for(let type of src_type) {
            let option = document.createElement('option')
            option.text = type.charAt(0).toUpperCase() + type.substr(1).toLowerCase()
            option.value = type
            element.add(option)
        }
        let select_opt = {
            removeItemButton: true,
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : true
        }
        fns_tags.choice.src_type = new Choices(element, Object.assign(select_opt))
    }
    /**
     * Create a event for src type part in tags part.
     */
    function part_tags_event_src_type() {
        fns_tags.choice.src_type.passedElement.element.addEventListener("addItem", (evt) => {
            let src_type
            try {
                src_type = evt.detail.value
                if(src_type === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.error(ex)
                return
            }
            if(!part_tags_get_list("src_type").includes(src_type)) {
                fns_tags.data.src_type.value.push(src_type)
                fns_tags.data.src_type.test_passed = fns_tags.data.src_type.value.length !== 0
            }
            add_property(fns_global.new_source_tags_user, "src_type", fns_tags.data.src_type.value)
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        fns_tags.choice.themes.passedElement.element.addEventListener("removeItem", (evt) => {
            let src_type
            try {
                src_type = evt.detail.value
                if(src_type === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.error(ex)
                return
            }
            fns_tags.data.src_type.value = fns_tags.data.src_type.value.filter( (type) => {return type !== src_type})
            if(fns_tags.data.src_type.value.length === 0) {
                fns_tags.data.src_type.test_passed = false
                remove_property(fns_global.new_source_tags_user, "src_type")
            } else {
                add_property(fns_global.new_source_tags_user, "src_type", fns_tags.data.src_type.value)
            }
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_src_type_btn_new").onclick = (evt) => {
            form_display("part_tags_src_type_add", true)
            form_display("part_tags_src_type_new", false)
         }
        form_get("part_tags_src_type_btn_add").onclick = (evt) => {
            let src_type = form_get("part_tags_src_type_input_add").value
            if(src_type === "") {
                form_input_err("part_tags_src_type_input_add", "part_tags_src_type_err", fns_tags.text.MSG_ERROR_VALUE_EMPTY)
                return
            }
            src_type = src_type.toLowerCase()
            part_tags_update("src_type", src_type)
            form_addclass("part_tags_src_type_input_add", "form_input_error")
            form_msg( "part_tags_src_type_err", "")
            form_display("part_tags_src_type_new", true)
            form_display("part_tags_src_type_add", false)
        }
        form_get("part_tags_src_type_display_tips").onclick = (evt) => {
            let display = form_get("part_tags_src_type_display_tips")
            let tips = form_get("part_tags_src_type_tips")
            form_tips(display, tips, fns_tags.text.MSG_TIPS_SRC_TYPE_INPUT_ADD)
         }
    }
    /**
     * Create a template for res type list for res type of the source.
     */
    function part_tags_create_res_type() {
        let template = [
            {node : "div", id : "part_tags_res_type_id"},
                {node : "div", id : "part_tags_res_type_choice", classList: ["input_container"], parent: 0},
                    {node : "h3", id:"part_tags_res_type_title", textContent : fns_tags.text.MSG_TEXT_TITLE_RES_TYPE, parent : 1},
                    {node : "select", id : "part_tags_res_type_select", multiple:"", parent : 1},
                {node : "div", id : "part_tags_res_type_new", parent : 0},
                    {node : "button", id : "part_tags_res_type_btn_new", 
                    textContent : fns_tags.text.MSG_TEXT_PLACHOLDER_RES_TYPE_INPUT, classList: ["btn", "a"], parent: 4}, 
                {node : "div", id : "part_tags_res_type_add", style:"display:none;", parent: 0},
                    {node : "input", id : "part_tags_res_type_input_add", type:"text", 
                    placeholder:fns_tags.text.MSG_TEXT_PLACHOLDER_RES_TYPE_INPUT, classList: ["form_input_text"], parent : 6},
                    {node : "button", id : "part_tags_res_type_btn_add", textContent : "ADD", classList: ["btn", "a"], parent: 6},
                    {node:"p", id:`part_tags_res_type_tips`, classList: ["form_input_tips"],
                    textContent : fns_tags.text.MSG_TIPS_RES_TYPE_INPUT_ADD, parent:6},
                    {node:"button", id:`part_tags_res_type_display_tips`, innerHTML: fns_global.icons.question, 
                    classList: ["btn", "a", "form_selection_input_btn"], parent:6},
                {node:"p", id:`part_tags_res_type_err`, classList: ["form_input_error"], parent:0}
        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        form_insert(el, fns_tags.doc.fieldset)
        part_tags_build_res_type()
        part_tags_event_res_type()
        for(let type of fns_tags.data.res_type.value) {
            fns_tags.choice.res_type.setChoiceByValue(type)
        }
    }
    /**
     * Build a choice list for res type of the source with choice.js.
     */
    function part_tags_build_res_type() {
        let res_type = part_tags_init_list("res_type")
        let element = form_get("part_tags_res_type_select")
        for(let type of res_type) {
            let option = document.createElement('option')
            option.text = type.charAt(0).toUpperCase() + type.substr(1).toLowerCase()
            option.value = type
            element.add(option)
        }
        let select_opt = {
            removeItemButton: true,
            resetScrollPosition: false,
            duplicateItemsAllowed: false,
            searchResultLimit: 8,
            shouldSort : true
        }
        fns_tags.choice.res_type = new Choices(element, Object.assign(select_opt))
    }
    /**
     * Create a event for res type part in tags part.
     */
    function part_tags_event_res_type() {
        fns_tags.choice.res_type.passedElement.element.addEventListener("addItem", (evt) => {
            let res_type
            try {
                res_type = evt.detail.value
                if(res_type === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.error(ex)
                return
            }
            if(!part_tags_get_list("res_type").includes(res_type)) {
                fns_tags.data.res_type.value.push(res_type)
                fns_tags.data.res_type.test_passed = fns_tags.data.res_type.value.length !== 0
            }
            add_property(fns_global.new_source_tags_user, "res_type", fns_tags.data.res_type.value)
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        fns_tags.choice.res_type.passedElement.element.addEventListener("removeItem", (evt) => {
            let res_type
            try {
                res_type = evt.detail.value
                if(res_type === "")
                    throw new Error("Value is empty")
            } catch(ex) {
                console.error(ex)
                return
            }
            fns_tags.data.res_type.value = fns_tags.data.res_type.value.filter( (type) => {return type !== res_type})
            if(fns_tags.data.res_type.value.length === 0) {
                fns_tags.data.res_type.test_passed = false
                remove_property(fns_global.new_source_tags_user, "res_type")
            } else {
                add_property(fns_global.new_source_tags_user, "res_type", fns_tags.data.res_type.value)
            }
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_res_type_btn_new").onclick = (evt) => {
            form_display("part_tags_res_type_add", true)
            form_display("part_tags_res_type_new", false)
         }
        form_get("part_tags_res_type_btn_add").onclick = (evt) => {
            let res_type = form_get("part_tags_res_type_input_add").value
            if(res_type === "") {
                form_input_err("part_tags_res_type_input_add", "part_tags_res_type_err", fns_tags.text.MSG_ERROR_VALUE_EMPTY)
                return
            }
            res_type = res_type.toLowerCase()
            part_tags_update("res_type", res_type)
            form_addclass("part_tags_res_type_input_add", "form_input_error")
            form_msg( "part_tags_res_type_err", "")
            form_display("part_tags_res_type_new", true)
            form_display("part_tags_res_type_add", false)
        }
        form_get("part_tags_res_type_display_tips").onclick = (evt) => {
            let display = form_get("part_tags_res_type_display_tips")
            let tips = form_get("part_tags_res_type_tips")
            form_tips(display, tips, fns_tags.text.MSG_TIPS_RES_TYPE_INPUT_ADD)
         }
    }
    /**
     * Create a template for tech search speed part in tags part. 
     */
    function part_tags_create_tech_search_speed() {
        let template = [
            {node : "div", id : "part_tags_tech_search_speed"},
                {node : "div", id : "part_tags_tech_search_speed_radio", parent : 0},
                    {node : "h3", id:"part_tags_tech_search_speed_title", textContent : fns_tags.text.MSG_TEXT_TITLE_TECH_SEARCH_SPEED(fns_tags.time_fast), parent : 1},
                    {node : "input", id: "part_tags_tech_search_speed_fast_input", name : "radio_tech_search_speed",
                    type: "radio", value: "fast", parent: 1},
                    {node : "label", id: "part_tags_tech_search_speed_fast_label", for: "part_tags_tech_search_speed_fast_input", textContent: "Fast", parent: 1},
                    {node : "input", id: "part_tags_tech_search_speed_slow_input", name : "radio_tech_search_speed",
                    type: "radio", value: "slow", parent: 1},
                    {node : "label", id: "part_tags_tech_search_speed_slow_label", for: "part_tags_tech_search_speed_slow_input", textContent: "Slow", parent: 1},
                    {node:"p", id:`part_tags_tech_search_speed_err`, classList: ["form_input_error"], parent:0}
        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        form_insert(el, fns_tags.doc.fieldset)
        part_tags_event_tech_search_speed()
        if(fns_tags.time_fast <= 2.5)
            form_set("part_tags_tech_search_speed_fast_input", true, "checked")
        else
            form_set("part_tags_tech_search_speed_slow_input", true, "checked")
        if(default_val.tags.tech[0].includes('fast') && form_get("part_tags_tech_search_speed_slow_input").checked)
			form_input_err("part_tags_tech_search_speed_slow_input", "part_tags_tech_search_speed_err", fns_tags.text.MSG_ERROR_SPEED_NOW_SLOW)
        else if(default_val.tags.tech[0].includes('slow') && form_get("part_tags_tech_search_speed_fast_input").checked)
			form_input_err("part_tags_tech_search_speed_fast_input", "part_tags_tech_search_speed_err", fns_tags.text.MSG_ERROR_SPEED_NOW_FAST)
    }
    /**
     * Create a event for tech search speed part in tags part.
     */
    function part_tags_event_tech_search_speed() {
        form_get("part_tags_tech_search_speed_fast_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(fns_tags.data.tech.value.includes("slow"))
                fns_tags.data.tech.value = fns_tags.data.tech.value.filter((t) => {return t !== "slow"})
            fns_tags.data.tech.value.push(value)
            add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
        form_get("part_tags_tech_search_speed_slow_input").addEventListener("change", (evt) => {
            let value = evt.target.value
            if(fns_tags.data.tech.value.includes("fast"))
                fns_tags.data.tech.value = fns_tags.data.tech.value.filter((t) => {return t !== "fast"})
            fns_tags.data.tech.value.push(value)
            add_property(fns_global.new_source_tags_user, "tech", fns_tags.data.tech.value)
            fns_tags.data.tech.test_passed = fns_tags.data.tech.value.length !== 0
            form_part_validator(fns_tags, "part_tags_btn_save_id")
        })
    }
    /**
     * Create a template for button save part in tags part. 
     */
    function part_tags_create_btn_save() {
        let template = [
            {node : "div", id:"part_tags_btn_save_id", style:"display: none;"},
                {node : "button", id:"part_tags_btn_save", classList : ["btn", "a", "anim_btn"], textContent: "Save !", parent: 0}
        ]
        let builder = form_builder(template)
        let el = builder.dom.firstChild
        let part = form_get('save_form_part')
        let ref = form_get('reset_form')
        form_insert(el, part, ref)
        part_tags_event_btn_save()
    }
    /**
     * Create a event for button save part in tags part.
     */
function part_tags_event_btn_save() {
	form_get("part_tags_btn_save").onclick = async(evt) => {
	for(let part of fns_global.fns_parts)
		form_display(part.doc.fieldset.id, false)
		add_property(fns_global.new_source_user, "tags", fns_global.new_source_tags_user)
		let custom_src = fns_global.new_source_user
		let old_custom = await browser.storage.sync.get('custom_src')
		let obj = (old_custom && old_custom.custom_src !== '') ? JSON.parse(old_custom.custom_src) : {}
		obj[`custom_${custom_src.headline_url}`] = custom_src
		browser.storage.sync.set({custom_src: JSON.stringify(obj, null, 2)})
		mµ.exec_in_background('build_src')
		evt.target.parentNode.style.display = 'none'
		//Code for save the custom source
	}
}
    var init = true
    var codeM
    /**
     * Show the new object source construct by the user in DOM.
     */
	async function refresh_result_new_source_area(){ 
        //optimized_json_object_value()
        add_property(fns_global.new_source_user, "tags", fns_global.new_source_tags_user)
        if(init) {            
            document.getElementById("result_area_new_source").textContent = JSON.stringify(fns_global.new_source_user, null, 4)
            codeM = CodeMirror.fromTextArea(
                document.getElementById("result_area_new_source"), {
                    mode: 'application/json',
                    // mode: {name:'javascript', json:true},
                    indentWithTabs: true,
                    screenReaderLabel: 'CodeMirror',
                    readOnly: 'nocursor'
                }
            )
            init = false
        } else {
            let content = JSON.stringify(fns.new_source_user, null, 4)
            codeM.setValue(content)
        }
	}
})()
var default_fns_start = {
    completed: false,
    passed: false,
    title: "Start new source",
    doc : {
        fieldset : null,
        legend: null,
        tags_name: null,
        headline: null,
        favicon: null,
        btn_next: cur_querystring.get('src') !== ''
    },
    text: {
        MSG_ERROR_VALUE_EMPTY: "Sorry but this value is required.",
        MSG_ERROR_NAME_ALREADY_EXIST: "Sorry but this name already exist.",
        MSG_ERROR_URL_NOT_FOUND: "Sorry but this url is not found or not valid.",
        MSG_ERROR_FAVICON_NOT_FOUND: "Sorry but this favicon is not found or not valid",
        MSG_TIPS_TAGS_NAME: "Tips : Just give a name of the source like 'Custom Source'.",
        MSG_TIPS_HEADLINE_URL: "Tips : Give a url of the source like 'https://www.custom-source.eu'.",
        MSG_TIPS_FAVICON: "Tips : Give a url of the favicon source like 'https://www.custom-source.eu/custom/favicon.png'."
    },
    data : {
        tags_name : {type: "required", test_passed : default_val.tags.name[1], value: default_val.tags.name[0]},
        headline : {type: "optionnal", test_passed : default_val.headline_url[1], value: default_val.headline_url[0]},
        favicon : {type: "optionnal", test_passed : default_val.favicon_url[1], value: default_val.favicon_url[0]}
    }
}
var default_fns_timezone = {
    completed: false,
    nb_required : 3,
    nb_optionnal : 0,
    title: "Timezone",
    doc : {
        fieldset : null,
        legend: null,
        tags_lang: null,
        tags_country: null,
        tags_tz: null,
        check_all_timezone: null,
        btn_next: cur_querystring.get('src') !== ''
    },
    text: {
        MSG_ERROR_VALUE_EMPTY: "Sorry but this value is required.",
        MSG_ERROR_VALUE_IS_INCORRECT: "Sorry but this value is incorrect or empty."
    },
    data : {
        tags_lang : {type: "required", test_passed : default_val.tags.lang[1], value: default_val.tags.lang[0]},
        tags_country : {type: "required", test_passed : default_val.tags.country[1], value: default_val.tags.country[0]},
        tags_tz : {type: "required", test_passed : default_val.tags.tz[1], value: default_val.tags.tz[0]}
    },
    choices : {
        lang : null,
        country: null,
        tz: null
    }
}
var default_fns_selector = {
    completed: false,
    title: "Selector",
    title_css: "CSS Selector",
    title_json: "JSON Selector",
    mode: "get",
    doc : {
        fieldset : null,
        legend: null,
        search_url: null,
        sub_dom: null,
        body: null,
        results: null,
        r_h1: null,
        r_url: null,
        r_dt: null,
        res_nb: null,
        btn_next: cur_querystring.get('src') !== ''
    },
    text: {
        MSG_ERROR_VALUE_EMPTY: "Sorry but this value is required.",
        MSG_ERROR_BAD_SEARCH_TERM: "Sorry but we didn't found the search terms in the url. Please verify your search terms or the source may use the POST method.",
        MSG_ERROR_IS_NOT_LINK: "Sorry but this element is not a link.",
        MSG_ERROR_IS_NOT_PATH : "Sorry but this value is not a CSS path.",
        MSG_ERROR_REGEX_NOT_VALID : "Sorry but this regex is not valid",
        MSG_ERROR_TEXT_CONTENT_EMPTY : "Sorry but the text content is empty. Please choose a attribut before use a regex functionnality.",
        MSG_ERROR_NOT_ELEMENT_FOUND : "Sorry but not element found with this path.",
        MSG_ERROR_FAIL_TO_PARSE: "Sorry but this regex doesn't parse a text content of element.",
        MSG_ERROR_DATE_FOUND: "Sorry but a date we doesn't parse is found. Please write this regex.",
        MSG_TEXT_TITLE_BODY: "Body",
        MSG_TEXT_TITLE_RESULTS: "Result",
        MSG_TEXT_TITLE_R_H1: "Title",
        MSG_TEXT_TITLE_R_URL: "URL",
        MSG_TEXT_TITLE_R_DT: "Date",
        MSG_TEXT_TITLE_R_TXT: "Text",
        MSG_TEXT_TITLE_R_IMG: "Image",
        MSG_TEXT_TITLE_R_IMG_SRC: "Image Source",
        MSG_TEXT_TITLE_R_IMG_ALT: "Image Alternative Text",
        MSG_TEXT_TITLE_R_BY: "Author",
        MSG_TEXT_TITLE_RES_NB: "Number of articles",
        MSG_TEXT_PLACEHOLDER_BODY: "Choose a search bar",
        MSG_TEXT_PLACEHOLDER_RESULTS: "Choose a results path",
        MSG_TEXT_PLACEHOLDER_R_H1: "Choose a title path",
        MSG_TEXT_PLACEHOLDER_R_URL: "Choose a link path",
        MSG_TEXT_PLACEHOLDER_R_DT: "Choose a date path",
        MSG_TEXT_PLACEHOLDER_R_TXT: "Choose a text path",
        MSG_TEXT_PLACEHOLDER_R_IMG: "Choose a image path",
        MSG_TEXT_PLACEHOLDER_R_IMG_SRC: "Choose a source image path",
        MSG_TEXT_PLACEHOLDER_R_IMG_ALT: "Choose a alternative text image path",
        MSG_TEXT_PLACEHOLDER_R_BY: "Choose a author path",
        MSG_TEXT_PLACEHOLDER_RES_NB: "Choose a number of article path",
        MSG_TIPS_BODY: "Tips : Click on the input when you see your search terms.",
        MSG_TIPS_RESULTS: "Tips : Click on one result.",
        MSG_TIPS_R_H1: "Tips : Click on title of the result.",
        MSG_TIPS_R_URL: "Tips : Click on link of the result.",
        MSG_TIPS_R_DT: "Tips : Click on date of the result.",
        MSG_TIPS_R_TXT: "Tips : Click on text preview of the result.",
        MSG_TIPS_R_IMG: "Tips : Click on image associate of the result.",
        MSG_TIPS_R_IMG_SRC: "Tips : Click on image source associate of the result.",
        MSG_TIPS_R_IMG_ALT: "Tips : Click on image alternative text associate of the result.",
        MSG_TIPS_R_BY: "Tips : Click on author(s) of the result.",
        MSG_TIPS_RES_NB: "Tips : Click on number of the result.",
        MSG_TEXT_INPUT_DISABLED: "Pass",
        MSG_TEXT_BTN_CONFIRM : "It is a good",
        MSG_TEXT_BTN_BACK : "Change element",
        MSG_TEXT_BTN_SHOW: "See element",
        MSG_TEXT_BTN_REG: "Create/Use a regex",
        MSG_TEXT_BTN_ATTR: "Choose a attribut",
        MSG_TEXT_BTN_NOT_DEFINE : "Not defined for this JSON",
        MSG_TEXT_REG_PLACEHOLDER: "Write your regex here...",
        MSG_TEXT_TOKEN_PLACEHOLDER: "Write tokens here...",
        MSG_TEXT_MODE_TITLE : "Continue with :",
        MSG_TEXT_BTN_METHOD_CSS : "Method CSS",
        MSG_TEXT_BTN_METHOD_JSON : "Method JSON",
        MSG_TEXT_BTN_METHOD_RSS : "Method RSS",
        MSG_TEXT_CONTENT_TYPE_HTML : "We detected your source is a HTML content type. We advise you to click on the button 'Method CSS'.",
        MSG_TEXT_CONTENT_TYPE_JSON : "We detected your source is a JSON content type. We advise you to click on the button 'Method JSON'."
    },
    data : {
        search_url: {type: "required", test_passed : false, value: default_val.search_url[0], hidden_value: ""},
        positive_test_search_term: {type: "required", test_passed : default_val.positive_test_search_term[1], value: default_val.positive_test_search_term[0]},
        search_url_web: {type: "optionnal", test_passed : false, value: default_val.search_url_web[0]},
        body: {type: "required", test_passed: false, value: default_val.body[0], css_class: "form_selection_body", type_content: "text", methode:["post"]},
        results: {type: "required", test_passed: false, value: default_val.results[0], css_class: "form_selection_results", type_content: "text",
        methode:["get", "post", "json"]},
        r_h1: {type: "required", test_passed: false, value: default_val.r_h1[0], css_class: "form_selection_r_h1", type_content: "text",
        type_regex:"string", regex: default_val.r_h1[2], attr: default_val.r_h1[3], nodes: ["h1", "h2", "h3", "h4", "h5"], methode:["get", "post", "json"]},
        r_url: {type: "required", test_passed: false, value: default_val.r_url[0], css_class: "form_selection_r_url", type_content: "link",
        type_regex:"string", regex: default_val.r_url[2], attr: default_val.r_url[3], nodes: ["a"], methode:["get", "post", "json"]},
        r_dt: {type: "required", test_passed: false, value: default_val.r_dt[0], css_class: "form_selection_r_dt", type_content: "text",
        type_regex:"list", regex: default_val.r_dt[2], index_regex: -1, attr: default_val.r_dt[3], nodes: ["time"], methode:["get", "post", "json"]},
        r_txt: {type: "optionnal", test_passed: false, is_passed: default_val.r_txt[0]==='', value: default_val.r_txt[0], css_class: "form_selection_r_txt",
        type_content: "text", type_regex:"string", regex: default_val.r_txt[2], attr: default_val.r_txt[3], nodes: ["p"], methode:["get", "post", "json"]},
        r_img: {type: "optionnal", test_passed: false, is_passed: default_val.r_img[0]==='', value: default_val.r_img[0], css_class: "form_selection_r_img",
        type_content: "image", type_regex:"string", regex: default_val.r_img[2], attr: default_val.r_img[3], nodes: ["img"], methode:["get", "post", "json"]},
        r_img_src: {type: "required", sub: "r_img", test_passed: true, skip: true, value: default_val.r_img_src[0], css_class: "form_selection_r_img",
        type_content: "image", type_regex:"string", regex: default_val.r_img_src[2], attr: default_val.r_img_src[3], nodes: ["img"], methode:["get", "post", "json"]},
        r_img_alt: {type: "required", sub: "r_img", test_passed: true, skip: true, value: default_val.r_img_alt[0], css_class: "form_selection_r_img",
        type_content: "image", type_regex:"string", regex: default_val.r_img_alt[2], attr: default_val.r_img_alt[3], nodes: ["img"], methode:["get", "post", "json"]},
        //rajouter r_img_title
        r_by: {type: "optionnal", test_passed: false, is_passed: default_val.r_by[0]==='', value: default_val.r_by[0], css_class: "form_selection_r_by",
        type_content: "text", type_regex:"string", regex: default_val.r_by[2], attr: default_val.r_by[3], nodes: [""], methode:["get", "post", "json"]},
        res_nb: {type: "optionnal", test_passed: false, is_passed: default_val.res_nb[0]==='', value: default_val.res_nb[0], css_class: "form_selection_res_nb",
        type_content: "text", type_regex:"string", regex: default_val.res_nb[2], attr: default_val.res_nb[3], nodes: [""], methode:["get", "post", "json"]}
    },
    current_index: 0,
    old_index: null,
    form_input : [
        "body",
        "res_nb",
        "results",
        "r_h1",
        "r_url",
        "r_dt",
        "r_txt",
        "r_img",
        "r_img_src",
        "r_img_alt",
        "r_by"
    ],
    class_list : [
        "form_selection_body",
        "form_selection_results",
        "form_selection_r_h1",
        "form_selection_r_url",
        "form_selection_r_dt",
        "form_selection_r_txt",
        "form_selection_r_img",
        "form_selection_r_by",
        "form_selection_res_nb"
    ],
    sub_dom : {
        id_show_element : "sub_dom_tmp_id",
        class_show_div : "sub_dom_show_div",
        container_node : ["div", "article"]
    },
    request : {
        raw : null,
        content_type : null
    }
}
var default_fns_tags = {
    completed: false,
    title: "Tags",
    time_fast : null,
    doc : {
        fieldset : null,
        legend: null,
    },
    text : {
        MSG_ERROR_VALUE_EMPTY : "Sorry but this value is required.",
        MSG_ERROR_SPEED_NOW_FAST : "Speed of this source has change to 'fast'",
        MSG_ERROR_SPEED_NOW_SLOW : "Speed of this source has change to 'slow'",
        MSG_TEXT_TITLE_THEMES : "Choose a themes :",
        MSG_TEXT_TITLE_TECH_ACCURACY : "According to you, compared to you search, the source give a accuracy results :",
        MSG_TEXT_TITLE_TECH_SEARCH_SYSTEM : "The source uses a search system...",
        MSG_TEXT_TITLE_SRC_TYPE : "Choose a source type :",
        MSG_TEXT_TITLE_RES_TYPE : "Choose a result type :",
        MSG_TEXT_PLACHOLDER_THEMES_INPUT : "Write a theme here...",
        MSG_TEXT_PLACHOLDER_SRC_TYPE_INPUT : "Write a new source type here...",
        MSG_TEXT_PLACHOLDER_RES_TYPE_INPUT : "Write a new result type here...",
        MSG_TEXT_THEMES_BTN_USER_INPUT : "I want add new theme",
        MSG_TEXT_SRC_TYPE_BTN_USER_INPUT : "I want add new source type",
        MSG_TIPS_THEMES_INPUT_ADD : "Tips : Just write a new tags.",
        MSG_TIPS_SRC_TYPE_INPUT_ADD : "Tips : Just write a new source type.",
        MSG_TIPS_RES_TYPE_INPUT_ADD : "Tips : Just write a new result type."
    },
    data : {
        themes : {type : "required", test_passed : default_val.tags.themes[1], value : default_val.tags.themes[0]},
        tech : {type : "required", test_passed : default_val.tags.tech[1], value : default_val.tags.tech[0]},
        src_type : {type : "required", test_passed : default_val.tags.src_type[1], value : default_val.tags.src_type[0]},
        res_type : {type : "required", test_passed : default_val.tags.res_type[1], value : default_val.tags.res_type[0]}
    },
    choice : {
        themes : null,
        src_type : null,
        res_type : null
    }
}
