// name    : utils.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals browser */

/**
 * @module utils
 */

// https://stackoverflow.com/questions/3700326/decode-amp-back-to-in-javascript
/**
 * Decode the given string.
 * @example htmlDecode("&lt;img src='myimage.jpg'&gt;") returns "<img src='myimage.jpg'>"
 * @param {string} s The string to decode
 * @returns {string} The decoded string
 */
export function HTML_decode_entities (s) {
	let elt = document.createElement('textarea')
	elt.innerHTML = s
	return elt.value || ''
}
// https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
// &laquo; -> « -> not an XML entity, so decode, recode in XML numerical entities
// <title> Xi & Macron </title> & -> &#038; (not re-encoded…)
// other XML entities are still missing
/**
 * Encode in utf8 the given string s.
 * @param {string} s the string to encode
 * @returns {string} the encoded s
 */
export function HTML_encode_UTF8 (s) {
	s = s.replace(/[\u00A0-\u9999&"]/g, (i) => `&#${i.charCodeAt(0)};`)
	// s = s.replace(/[\u00A0-\u9999&]/g, (i) => {
	// 	console.log(`${i} &#${i.charCodeAt(0)};`); return `&#${i.charCodeAt(0)};` })
	// the following code works only with 1st occurence of fields
	/* s = s.replace(/>([^<]*?)>/g, ">$1&gt;")
	s = s.replace(/<([^>]*?)</g, "<$1&lt;")
	s = s.replace(/>([^.<]*?)'/g, ">$1&apos;")
	s = s.replace(/>([^.<]*?)"/g, ">$1&quot;") */
	return s
}
/**
 * Encode in utf8 the given string.
 * @param {string} s String to encode
 * @returns {string} The encoded string
 */
export function XML_encode_UTF8 (s) {
	return s.replace(/[\u00A0-\u9999]/g, (i) => `&#${i.charCodeAt(0)};`)
}
// https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross
// parser and dom_err_NS could be cached on startup for efficiency
/**
 * The dom parser to use.
 * @type {DOMParser}
 * @constant
 */
export const dom_parser = new DOMParser()
console.group('µtils initialization')
console.info('µtils : The next "XML parse error: tag not closed" at line 1 col 1 is '
	+ 'useful for µtils function "isParserError".')
/**
 * A domerror provoked for get the domerror namespace.
 * @type {Document} 
 * @constant
 */
const dom_err = dom_parser.parseFromString('<', 'text/xml')
/**
 * The namespace to search the dom error.
 * @type {Document} 
 * @constant
 */
const dom_err_NS = dom_err.getElementsByTagName('parsererror')[0].namespaceURI
setTimeout(() => {
	console.info('µtils : New errors are now not intended')
	console.groupEnd()
}, 0)
/**
 * Check if the dom has parser error.
 * @param {string} dom The dom fargment to check
 * @returns {boolean} parser error ?
 */
export function isParserError(dom) {
	/* if (dom_err_NS === 'http://www.w3.org/1999/xhtml') {
		// In PhantomJS the parserirror element doesn't seem to have a special namespace,
		// so we are just guessing here :(
		return dom.getElementsByTagName('parsererror').length > 0
	}*/
	return dom.getElementsByTagNameNS(dom_err_NS, 'parsererror').length > 0
}
// https://developer.mozilla.org/fr/docs/Web/API/TextDecoder/TextDecoder

// https://stackoverflow.com/questions/50840168/how-to-detect-if-the-os-is-in-dark-mode-in-browsers
/**
 * Check if the browser is in dark mode.
 * @returns {boolean} dark mode
 */
export function isDarkMode() {
	return window.matchMedia('(prefers-color-scheme: dark)').matches
}
/* For Local Storage
export function get_stored(key, default_value) {
	let local_data = localStorage.getItem(key)
	return local_data == null ? default_value : local_data
}*/
/**
 * Get the value of key in the browser storage, if key is not defined the key with the default value.
 * @param {string} key The storage key
 * @param {Object} default_value the default value of the key
 * @returns {Object} The value stored
 */
export async function get_stored(key, default_value) {
	let getting = browser.storage.sync.get()
	let browser_data
	await getting.then(elt=>{browser_data = elt[key]})
	if(typeof(browser_data) === 'undefined' || browser_data === null
		|| browser_data === '') {
		let obj_str = {}
		obj_str[key] = default_value
		browser.storage.sync.set(obj_str)
		return default_value
	} else {
		return browser_data
	}
}
/**
 * Upload the data in a filename and submit to the user.
 * @param {string} file_name The filename
 * @param {string} str The data to upload
 */
export function upload_file_to_user(file_name, str) {
	let a = document.createElement('a')
	a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(str)}`
	a.download=file_name
	a.click()
}
/**
 * Make the current date human readable.
 * @returns The current date formated
 */
export function ndt_human_readable() {
	return new Date().toISOString().replace('T', '_').replace(':', 'h').split(':')[0] }
export function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	)
}
/**
 * Encode the string for xml (<, >, &, ', ").
 * @param {string} unsafe The string to encode
 * @returns {string} The encoded string
 */
export function encode_XML(unsafe) {
	unsafe = removeXMLInvalidChars(unsafe, false)
	// if true, all text is removed (removeDiscouragedChars)
	return unsafe.replace(new RegExp('[<>&\'"]', 'g'), (c) => { switch (c) {
	case '<': return '&lt;'
	case '>': return '&gt;'
	case '&': return '&amp;'
	case "'": return '&apos;'
	case '"': return '&quot;'
	} } )
}
/**
 * Removes invalid XML characters from a string
 * From https://gist.github.com/john-doherty/b9195065884cdbfd2017a4756e6409cc
 * @param {string} str - a string containing potentially invalid XML char. (non-UTF8, STX, EOX)
 * @param {boolean} removeDiscouragedChars - should it remove discouraged but valid XML char.
 * @return {string} a sanitized string stripped of invalid XML characters
 */
function removeXMLInvalidChars(str, removeDiscouragedChars=true) {
	// remove everything forbidden by XML 1.0 specifications, plus the unicode replacement
	// character U+FFFD
	let regex = new RegExp('((?:[\0-\x08\x0B\f\x0E-\x1F\uFFFD\uFFFE\uFFFF]|[\uD800-\uDBFF]' +
		'(?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]))', 'g')
	str = str.replace(regex, '')
	if (removeDiscouragedChars) {
		// remove everything discouraged by XML 1.0 specifications
		regex = new RegExp(
			'([\\x7F-\\x84]|[\\x86-\\x9F]|[\\uFDD0-\\uFDEF]|(?:\\uD83F[\\uDFFE\\uDFFF])|' +
		' (?:\\uD87F[\\uDFFE\\uDFFF])|(?:\\uD8BF[\\uDFFE\\uDFFF])|(?:\\uD8FF[\\uDFFE\\uDFFF])|'+
		'(?:\\uD93F[\\uDFFE\\uDFFF])|(?:\\uD97F[\\uDFFE\\uDFFF])|(?:\\uD9BF[\\uDFFE\\uDFFF])|'+
		'(?:\\uD9FF[\\uDFFE\\uDFFF])|(?:\\uDA3F[\\uDFFE\\uDFFF])|(?:\\uDA7F[\\uDFFE\\uDFFF])|'+
		'(?:\\uDABF[\\uDFFE\\uDFFF])|(?:\\uDAFF[\\uDFFE\\uDFFF])|(?:\\uDB3F[\\uDFFE\\uDFFF])|'+
		'(?:\\uDB7F[\\uDFFE\\uDFFF])|(?:\\uDBBF[\\uDFFE\\uDFFF])|(?:\\uDBFF[\\uDFFE\\uDFFF])|'+
		'(?:[\\0-\\t\\x0B\\f\\x0E-\\u2027\\u202A-\\uD7FF\\uE000-\\uFFFF]|[\\uD800-\\uDBFF]'+
		'[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|(?:[^\\uD800-\\uDBFF]|^)'+
		'[\\uDC00-\\uDFFF]))', 'g')
		str = str.replace(regex, '')
	}
	return str
}
/**
 * Tranform the link in a url link for domain part.
 * @param {string} link The link to transform
 * @param {string} domain_part The doamin part of the destination
 * @returns {string} The working url
 */
export function urlify(link, domain_part) { // return URL from any href (even relative links)
	if (!link)  // and we are very happy to test implicitely many cases
		return link
	if (link.startsWith('http')) {
		return link
	} else {
		if (link.startsWith('file')) {	// remove meta-press.es auto-added path
			return link.replace(document.URL.split('/').slice(0,-1).join('/'), domain_part)
		} else if (link.startsWith('//')) {
			//return domain_part + '/' + link.split('/').slice(3).join('/')
			return (domain_part.split('/'))[0] + link
		} else {
			return domain_part + (link.startsWith('/') ? '' : '/') + link
		}
	}
}
/**
 * Found the favicon with the given html and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_favicon_url(html_fragment, domain_part) { // favicon may be implicit
	let favicon = html_fragment.querySelector('link[rel~="icon"]')
	return urlify(favicon && favicon.getAttribute('href') || '/favicon.ico', domain_part)
}
/**
 * Found the rss url with the given html and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_rss_url(html_fragment, domain_part) { // favicon may be implicit
	let rss = html_fragment.querySelector('link[rel~="alternate"][type="application/rss+xml"]')
	return urlify(rss && rss.getAttribute('href') || '/feed/rss/', domain_part)
}
var intlNum = Intl.NumberFormat('fr', {minimumIntegerDigits: 4, useGrouping: 0})
/**
 * The date to add configure timezone.
 * @param {string} dt_str The date to timezone
 * @param {string} tz The timezone
 * @returns {Date}
 */
export function timezoned_date (dt_str, tz='UTC') {
	let dt = dt_str ? new Date(dt_str) : new Date()
	// if (isNaN(dt)) throw new Error(`${dt_str} is an invalid Date`)
	if (isNaN(dt)) return undefined
	if (tz == 'UTC' || tz == 'GMT') return dt
	let parsed_tz = parseInt(tz,10)
	if(isNaN(parsed_tz)) {
		let dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)
		let dt_UTC = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
		let dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		let int_offset = parseInt(dt_repr.split('UTC')[1].replace('−', '-').replace(':', '.') * 100)
		let tz_offset = intlNum.format(int_offset)
		let tz_repr = int_offset >= 0 ? '+' + tz_offset : tz_offset
		return new Date(dt_orig.toISOString().replace(/\.\d{3}Z/, tz_repr))
	}
}
/**
 * Bolden all the search words in the str.
 * @param {string} str The working string
 * @param {string} search The sentence search
 * @returns {string} The string with bolden termes
 */
export function bolden (str, search) {
	if (typeof str != 'string') return ''
	let search_term_list = search.split(' ')
	for (let i of search_term_list)
		str = str.replace(new RegExp(`(${preg_quote(i)})`, 'gi'), '<b>$1</b>')
	return str
}
/**
 * Cut the string to the at size if string is too long.
 * @param {string} str The string
 * @param {number} at The needed size
 * @returns {string} the shorten str
 */
export function shorten(str='', at) { return str.length > at ? `${str.slice(0, at)}…` : str }
/**
 * Remove trailing space before and after the string.
 * @param {string} str Working string
 * @returns {string} The clean string
 */
export function trim(str) { return str && str.replace(/^\s*|\s*$|&/g, '') }
/**
 * Remove trailing space before and after the string and the double space (\s\s) in the string.
 * @param {string} str Working string
 * @returns {string}
 */
export function triw(str) { return str && str.replace(/^\s*|\s*$|(\s\s)+/g, '') }
/**
 * Remove all spaces.
 * @param {string} str Working string
 * @returns {string}
 */
export function dry_triw(str) { return str && str.replace(/\s*/g, '') }
export function drop_low_unprintable_utf8(str) {
	str = str.replace(/[\u0000-\u0009]/g, '')
	str.replace(/[\u000B-\u000C]/g, '')
	return str && str.replace(/[\u000E-\u001F]/g, '')
}
/**
 * Quote the special character in the string.
 * @param {string} str Working string
 * @returns {string} The string prepared
 */
export function preg_quote(str) { // http://kevin.vanzonneveld.net, http://magnetiq.com
	// preg_quote("How many? $40"); -> 'How many\? \$40'
	// preg_quote("\\.+*?[^]$(){}=!<>|:"); -> '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
	// str = String(str).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1")
	str = String(str).replace(/([\\.+*?[^\]$(){}=!<>|:])/g, '\\$1')
	return str.replace(/\s/g, '\\s')  // ensure we match converted &nbsp;
}
/**
 * Quote the special character in the string for a file.
 * @param {string} str Working string
 * @returns {string} The string prepared
 */
export function preg_quote_file(str) {
	if (!str) str = ""
	return str.replace(/([\\"',])/g, '\\$1')
}
export function drop_escaped_quote_file(str) {
	return str.replace(/\\(["',\n])/g, "$1")
}
/**
 * Remove the \' (and \") in a string.
 * @param {string} str The string to clean
 * @returns {string}
 */
export function drop_escaped_quotes(str) {
	return str.replace(/\\'/g, "'")//.replace(/\\"/g, '"')
}
/**
 * Extract the domain of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_part(url) {
	let [htt, , dom] = url.split('/')
	return `${htt}//${dom}`
}
/**
 * Extract the domain name of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_name(url) {
	let [ , , dom] = url.split('/')
	let s_dom = dom.split('.')
	if (s_dom[0] === 'www') s_dom = s_dom.slice(1, s_dom.length)
	return toTitleCase(s_dom.join('.'))
}
/**
 * Return a string title formated 1st letter uppercase and others lowercase.
 * @function
 * @param {string} str string to tranform
 * @returns {string} string title formated
 */
export const toTitleCase = (str) => str.replace(/\w\S*/g, a =>
	`${a.charAt(0).toUpperCase()}${a.substr(1).toLowerCase()}`)
/**
 * Return the first integer greater than or equal to n.
 * @function
 * @param {number} n number to round
 * @returns {number} rounded n
 */
export const rnd = (n) => Math.ceil (Math.random () * Math.pow (10, n))
/**
 * Pick a number between a and b.
 * @function
 * @param {number} a Inferior limit
 * @param {number} b Superior limit
 * @returns {number} The random number
 */
export const pick_between = (a, b) => Math.floor(Math.random() * b) + a
/**
 * Return the content type without rss, atom charset.
 * @function
 * @param {string} str string to clean
 * @returns {string}
 */
export const clean_c_type = (str) => str.split(';')[0].replace(/(rss|atom)\+/, '')
/**
 * Wait the number of time to return promise. Use await and the magic come.
 * @function
 * @param {number} duration The sleep duration
 * @returns {Promise}
 */
export const sleep = (duration) => new Promise(resolve => setTimeout(resolve, duration))

// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
/**
 * The xpath evaluator for {@link evaluateXPath}.
 * @constant
 * @type {XPathEvaluator}
 */
const xpe = new XPathEvaluator()
/* let no_namespace = false
 if (no_namespace) {
	nsResolver = xpe.createNSResolver(aNode.ownerDocument == null ?
		aNode.documentElement : aNode.ownerDocument.documentElement)
} else {
*/
/**
 * Contain all the namespace we found. Return the namespace url for a given prefix.
 * @param {string} prefix The prefix
 * @returns {string} the corresponding url norme
 */
const nsResolver = (prefix) => {
	// Extended version of
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_using_XPath_in_JavaScript#Implementing_a_User_Defined_Namespace_Resolver
	let ns = {
		'xhtml' : 'http://www.w3.org/1999/xhtml',
		'mathml': 'http://www.w3.org/1998/Math/MathML',
		'content':'http://purl.org/rss/1.0/modules/content/',
		'wfw': 'http://wellformedweb.org/CommentAPI/',
		'dc': 'http://purl.org/dc/elements/1.1/',
		'atom': 'http://www.w3.org/2005/Atom',
		'sy': 'http://purl.org/rss/1.0/modules/syndication/',
		'slash': 'http://purl.org/rss/1.0/modules/slash/',
		'media': 'http://search.yahoo.com/mrss/',
		'image': 'http://www.google.com/schemas/sitemap-image/1.1'
	}
	return ns[prefix] || null
}
/**
 * Search the aExpr path in a Node (queryselectorall for xpath).
 * @param {Node} aNode The node working area
 * @param {string} aExpr The xpath expression
 * @returns {Array} All the match found
 */
export function evaluateXPath(aNode, aExpr) {
	let result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null)
	let found = [], res
	while (res = result.iterateNext()) found.push(res)
	if (found.length === 1) found = found[0]
	return found
}
/**
 * Extract with regex the needed data in re and fill repl with it.
 * @param {string} re Regex expression
 * @param {string} str the string to search
 * @param {string} repl The format of the result default '$1' $i i the group number
 * @returns {string} a string with the data extracted
 */
export function regextract (re, str, repl='$1') {
	return str.replace(new RegExp(`(?:.|\\n)*${re}(?:.|\\n)*`, 'm'), repl) }
/**
 * Extract an attribute attr from an string str with html.
 * @param {string} attr Attribute to extract
 * @param {string} str The string to search
 * @returns {string} The value of attributes
 */
export function extract_val_attr (attr, str) {
	let s = regextract(` ${attr}=["'](.*?)["']`, str)
	return (s === str ? '' : s)}
/**
 * The regex string for extract hour:minutes.
 * @type {string}
 */
export const HH_MM_STR = '(\\d\\d:\\d\\d)'
/**
 * The regex for extract hour:minutes.
 * @type {RegExp}
 */
export const HH_MM_RE = new RegExp(HH_MM_STR, 'i')
/**
 * The regex for extract an string with just hour:minutes.
 * @type {RegExp}
 */
export const HH_MM_ONLY_RE = new RegExp(`^${HH_MM_STR}$`, 'i')
/**
 * The regex string for extract -month-day.
 * @type {string}
 */
export const MM_DD_ONLY_STR = '^-\\d{1,2}-\\d{1,2}'
/**
 * The regex for extract -month-day.
 * @type {RegExp}
 */
export const MM_DD_ONLY_RE = new RegExp(MM_DD_ONLY_STR, 'i')
/**
 * Check if v is an Array.
 * @param {*} v An value
 * @returns {boolean} array ?
 */
export const is_array = (v) => v && typeof(v) === 'object' && typeof(v.length) !== 'undefined'
// export const is_array = (v) => (typeof v).startsWith('obj')
/**
 * Get the selected value in the object in sel.options and sel.selectedIndex is the selection.
 * @param {Object} sel The object
 * @returns The selected value
 */
export function get_select_value(sel) { return sel.options[sel.selectedIndex].value }
/**
 * Get the value in json_obj from the paths describe in json_path.
 * @param {string} json_path The json path like sources.invidius.tags.name
 * @param {Object} json_obj The object to search
 * @returns The found value
 */
export function deref_json_path(json_path, json_obj) {
	let val = json_obj
	if (json_path)
		for (let json_path_elt of json_path.split('.')) {
			try {
				val = val[json_path_elt]
			} catch (exc) {
				return undefined
			}
		}
	return val
}
/**
 * Format the string with the token $1 -> $i.
 * @example str_fmt('$1-ok', ["ko"]) return 'ko-ok'
 * @param {string} a The format
 * @param {Array} tokens The tokens to uses
 * @returns {string} The formated string
 */
export function str_fmt(a, tokens) {
	for (let k in tokens){
		a = a.replace(new RegExp(`\\$${Number(k)+1}`, 'g'), HTML_encode_UTF8(String(tokens[k])))
		//a = a.replace(new RegExp(`\\$${Number(k)+1}`, 'g'), HTML_encode_UTF8(tokens[k]))
		// for '' in string of an attribute
	}
	return a
}
/**
 * Set the language in the HTML document.
 * @param {string} lg The language
 */
export function set_html_lang(lg) { document.body.parentElement.lang = lg }
//export const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)
/**
 * Remove an anchor for a given URL.
 * @example  rm_href_anchor('olivier.fr#tree') return 'olivier.fr'
 * @param {URL} permalink The link
 * @returns The link without the anchor
 */
export function rm_href_anchor(permalink) { return permalink.href.split('#')[0] }
/**
 * Check if elt is in overflow.
 * @param {Node} elt The elt to check
 * @returns {boolean} overflow ?
 */
export function isOverflown(elt) {
	return elt.scrollHeight > elt.clientHeight || elt.scrollWidth > elt.clientWidth
}
/**
 * Extract a property of a website.
 * @example get_html_property(url, get_favicon_url) get_favicon_url -> Node, string in parameter
 * @param {string} url The url of the website
 * @param {Function} property_extract The function who extract the data in parsed html fragment
 * @returns The property or an empty string
 */
 export function get_html_property(url, property_extract) {
	let requete = new XMLHttpRequest()
	let property = "unresolved"
	requete.open('GET', url, false)
	try {
		requete.send()
		if (requete.status === 200 || requete.status === 404 || requete.status === 304) {
			let ctype = requete.getResponseHeader("content-type")
			ctype = ctype.split(";")[0] // remove charset
			let rep = dom_parser.parseFromString(requete.responseText, ctype)
			property = property_extract(rep, domain_part(url))
		}
		if(property !== "unresolved") {
			try {
				requete.open('GET', property, false)
				requete.send()
				if (requete.status !== 200 && requete.status !== 304)
					return ""
			} catch (exec) {
				console.error(exec)
				property = "unresolved"
			}
		}
	} catch (exec) {
		console.error(exec)
		property = "unresolved"
	}
	if (property === "unresolved") {
		console.log(url, "unresolved")
		return ""
	}
	return property
}
/**
 * Extract the favicon of a website.
 * @param {string} url - The url of the website
 * @returns The favicon url or an empty string
 */
export function get_favicon(url) {
	return get_html_property(url, get_favicon_url)
}
/**
 * Extract the rss of a website.
 * @param {string} url - The url of the website
 * @returns The rss url or an empty string
 */
 export function get_rss(url) {
	return get_html_property(url, get_rss_url)
}
/**
 * Give the timezone in the given country code.
 * @param {string} country_code - The country code
 * @returns {Array} the timeszones found or an empty string
 */
export async function get_timezone(country_code) { // Marin
	try {
		let file = await fetch("json/timezones.json")
		let obj = await file.json()
		return Object.getOwnPropertyDescriptor(obj, country_code).value
	} catch (exc) {
		console.error(exc)
		return ""
	}
}

/**
 * Check if an word in a array is present in a string.
 * @param {string} str string to work on
 * @param {Array} words Array of words which can hit
 * @returns {boolean}
 */
export function string_includes_array(str, words) {
	return words.some(word => str.includes(word.toLowerCase()));
}

/**
 * Remove the 1st ocurence of the value in the given array.
 * @param {Array} array work space
 * @param {*} value The value to remove
 * @returns {Array} without value
 */
export function remove_array_value(array, value) {
	let index = array.indexOf(value)
	return index > -1 ? array.splice(index, 1) : array
}
/**
 * Remove duplicata in a array.
 * @param {Array} array The working array
 * @returns A new clean array
 */
export function remove_duplicate_in_array(array) {
	return [...new Set(array)]
}
export function copy_of_object(obj){
	return JSON.parse(JSON.stringify(obj))
}