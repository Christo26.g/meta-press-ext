// SPDX-FileCopyrightText: 2021 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
import * as mµ from './mp_utils.js'
import * as g from './gettext_html_auto.js/gettext_html_auto.js'

mµ.set_theme();

(async () => {
	'use strict'
	await g.xgettext_html()
	const userLang = await mµ.get_wanted_locale()
	/*const mp_i18n = */await g.gettext_html_auto(userLang)
})()
