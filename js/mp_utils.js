// name		 : mp_utils.js
// SPDX-FileCopyrightText: 2017-2021 Simon Descarpentres <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals browser */
import * as µ from './utils.js'
import {month_nb} from './month_nb/month_nb.js'

/**
 * @module mp_utils
 */

/**
 * Return the actual themes.
 * @async
 * @returns The wanted theme
 */
export async function get_wanted_theme() {
	return await get_stored_theme() || (µ.isDarkMode() && 'dark' || 'light')
}
/**
 * Set the theme wanted.
 * @async
 */
export async function set_theme() { if (await get_wanted_theme() === 'light') set_light_mode() }
/**
 * Set the ligth mode color scheme.
 */
export function set_light_mode() {
	var html = document.getElementsByTagName('html')[0]
	html.style.cssText += '--turquoise-background: var(--mp-turquoise)'
	html.style.cssText += '--background: var(--light-normal-background)'
	html.style.cssText += '--foreground: black'
	html.style.cssText += '--frame-background: var(--light-frame-background)'
	html.style.cssText += '--a-color: var(--mp-dark-turquoise)'
	html.style.cssText += '--mp-gray: var(--dark-gray)'
	var imgs = document.querySelectorAll('img[src$="_dark.svg"]')
	for (var img of imgs)
		img.src = img.src.replace('_dark', '')
}
/**
 * Give the language we need to use.
 * @async
 * @returns The language wanted (navigator or stored)
 */
export async function get_wanted_locale() {
	return await get_stored_locale() || navigator.language || navigator.userLanguage
}
/**
 * Get the browser data for entry 'locale'.
 * @function
 * @returns The local browser data
 */
export const get_stored_locale = async() => await µ.get_stored('locale', '')
/**
 * Get the browser data for entry 'tz'.
 * @function
 * @returns The tz browser data
 */
export const get_stored_tz = async() => await µ.get_stored(
	'tz', (new Date().getTimezoneOffset()).toString())
/**
 * Get the browser data for entry 'theme'.
 * @function
 * @returns The theme browser data
 */
export const get_stored_theme = async() => await µ.get_stored('dark_background', '')
/**
 * Get the browser data for entry 'live_search_reload'.
 * @function
 * @returns If the search need to be reload after sentence modification
 */
export const get_stored_live_search_reload = async() => await µ.get_stored(
	'live_search_reload', '0')
/**
 * Get the browser data for entry 'sentence_search'.
 * @function
 * @returns Search in the sources in one word or many word
 */
export const get_stored_sentence_search = async() => await µ.get_stored('sentence_search', '1')
/**
 * Get the browser data for entry 'collections'.
 * @function
 * @returns The collection of source for search
 */
 export const get_stored_collections = async() => await µ.get_stored('collections', 
 	{
		"provided" : {},
		"created": {}
	})
/**
 * Get the browser data for entry 'undup_results'.
 * @function
 * @returns If we can have duplicate result 
 */
export const get_stored_undup_results = async() => await µ.get_stored('undup_results', '1')
/**
 * Get the browser data for entry 'load_photos'
 * @function
 * @returns If we want picture to be loaded
 */
export const get_stored_load_photos = async() => await µ.get_stored('load_photos', '1')
/**
 *  Get the browser data for entry 'max_res_by_src'.
 * @function
 * @returns The maximum of response for a source
 */
export const get_stored_max_res_by_src = async() => await µ.get_stored('max_res_by_src', 20)
/**
 * Get the browser data for entry 'max_time_before_abort'.
 * @function
 * @returns The search timeout
 */
export const get_stored_max_time_before_abort = async() => await µ.get_stored('max_time_before_abort', 30)
/**
 * Get the browser data for entry 'headline_loading'.
 * @function
 * @returns If we want to load the news
 */
export const get_stored_headline_loading = async() => await µ.get_stored(
	'headline_loading', '0')
/**
 * Get the browser data for entry 'live_headline_reload'.
 * @function
 * @returns If we reload if the filters change
 */
export const get_stored_live_headline_reload = async() => await µ.get_stored(
	'live_headline_reload', '1')
/**
 * Get the browser data for entry 'keep_host_perm'.
 * @function
 * @returns We keep the permission for cors
 */
export const get_stored_keep_host_perm = async() => await µ.get_stored('keep_host_perm', '1')
/**
 * Get the browser data for entry 'max_headline_loading'.
 * @function
 * @returns The number of maximum result for news
 */
export const get_stored_max_headline_loading = async() => await µ.get_stored(
	'max_headline_loading', 30)
/**
 * Get the browser data for entry 'headline_page_size'.
 * @function
 * @returns The number of result in the headline page
 */
export const get_stored_headline_page_size = async() => await µ.get_stored(
	'headline_page_size', 5)
/**
 * Try to create user notification.
 * @param {string} title title of the notification
 * @param {string} body the body of the notification
 */
export function notifyUser(title, body) {
	// Let's check if the browser supports notifications
	if (!('Notification' in window)) console.warn('Browser don\'t support desktop notification')
	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === 'granted') createNotification(title, body)
	// If it's okay let's create a notification
	else if (Notification.permission !== 'denied')
		// The Notification permission may only be requested from inside a short running
		// user-generated event handler.
		Notification.requestPermission().then(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === 'granted') createNotification(title, body)
		})
	// At last, if the user has denied notifications, and you
	// want to be respectful there is no need to bother them any more.
}
/**
 * Instantiate the notification.
 * @see {@link notifyUser}
 * @param {string} title title of the notification
 * @param {string} body the body of the notification
 */
function createNotification(title, body) {
	new Notification(title, {
		body: body,
		icon: 'img/favicon-metapress-v2.png',
		image: 'img/logo-metapress_sq.svg',
		badge: 'img/favicon-metapress-v2.png',
		actions: []
	})
}
/**
 * Execute an function in background and return a promise or false if not tound.
 * @param {string} function_name the name
 * @param {Array} params The parameter in order
 * @returns {Promise}
 */
export function exec_in_background(function_name, params=[]) {
	return browser.runtime.sendMessage(browser.runtime.id,
		{'function': function_name, 'params': params})
} 
/**
 * Remove all the cors permissoin given.
 * @async
 */
export async function drop_host_perm () {
	let perm = await browser.permissions.getAll()
	// console.log(`Host permissions to drop : ${perm.origins.join(', ')}`)
	// console.log(`API permissions to drop : ${perm.permissions.join(', ')}`)
	browser.permissions.remove({origins: perm.origins})
}
/**
 * Check if the cors permission for current_sources exists.
 * @param {Array} current_sources the url of sources to check
 * @param {Object} sources_objs The builded sources
 * @returns {boolean} Permission exists or not
 */
export async function check_host_perm(current_sources, sources_objs) {
	let bool_perm = true
	let perm = await browser.permissions.getAll()
	let current_host_perm  = get_current_source_headline_hosts(current_sources, sources_objs)
	current_host_perm = current_host_perm.concat(get_current_source_search_hosts(current_sources, sources_objs))
	current_host_perm = µ.remove_duplicate_in_array(current_host_perm)
	for(let val of current_host_perm) {
		bool_perm = perm.origins.includes(val)
	}
	return bool_perm
}
/**
 * Check if the number of not given cors permission for current_sources exists.
 * @param {Array} current_sources the url of sources to check
 * @param {Object} sources_objs The builded sources
 * @returns {number} Number of needed permission
 */
 export async function get_nb_needed_host_perm(current_sources, sources_objs) {
	let nb_needed = 0
	let needed = []
	let perm = await browser.permissions.getAll()
	let current_host_perm  = get_current_source_headline_hosts(current_sources, sources_objs)
	current_host_perm = current_host_perm.concat(get_current_source_search_hosts(current_sources, sources_objs))
	current_host_perm = µ.remove_duplicate_in_array(current_host_perm)
	for(let val of current_host_perm) {
		if (!perm.origins.includes(val)) {
			nb_needed++
			needed.push(val)
		}
	}
	return {'not_perm': nb_needed, 'names':needed}
}
/**
 * Get the search url for the current_source_selection.
 * @param {Array} current_source_selection The sources url
 * @param {Object} sources_objs The builded sources
 * @returns {Array} The search url
 */
export function get_current_source_search_hosts (current_source_selection, sources_objs) {
	let source_hosts = []
	for (let s of current_source_selection) {
		if (typeof(sources_objs[s]) === 'undefined') continue
		let s_surl = sources_objs[s].search_url
		if(s_surl && s_surl !== "") {
			if (s !== µ.domain_part(s_surl))	{
				source_hosts.push(`${µ.domain_part(sources_objs[s].search_url)}/*`)
				//source_hosts.push(`${s}/*`)
			} else
				source_hosts.push(`${s}/*`)
			if (sources_objs[s].redir_url) {
				let redirs = sources_objs[s].redir_url
				if (µ.is_array(redirs))
					for (const i of redirs) 
						source_hosts.push(`${i}/*`)
				else
					source_hosts.push(`${µ.domain_part(sources_objs[s].redir_url)}/*`)
			}
		} else
			source_hosts.push(`${s}/*`)
	}
	return source_hosts
}
/**
 * Get the headline url for the current_source_selection.
 * @param {Array} current_source_selection The sources URL
 * @param {Object} sources_objs The builded sources
 * @returns {Array} The headline urls
 */
export function get_current_source_headline_hosts (current_source_selection, sources_objs) {
	let source_hosts = []
	for (let s of current_source_selection) {
		if (typeof(sources_objs[s]) === 'undefined') continue
		let s_hurl = sources_objs[s].headline_url
		if(s_hurl && s_hurl !== "" && s_hurl !== "https://www.custom-source.eu"){
			source_hosts.push(`${µ.domain_part(s_hurl)}/*`) }
	}
	return source_hosts
}
/**
 * Request to the user the cors permission for the sources. 
 * @param {Object} sources The builded sources
 */
export function request_sources_perm (sources) {
	let s_surl = get_current_source_search_hosts(Object.keys(sources), sources)
	let perm = {origins : new Set(s_surl)}
	let s_hurl = get_current_source_headline_hosts(Object.keys(sources), sources)
	for (let sh of s_hurl) {
		perm.origins.add(sh)
	}
	perm.origins.add("https://www.meta-press.es/*")
	perm.origins = Array.from(perm.origins)
	return browser.permissions.request(perm)
}
/**
 * Create an img balise in a string filled with the given args.
 * @param {string} src The image url
 * @param {string} alt The image alt
 * @param {string} title The image title
 * @returns {string} The img balise filled
 */
export function img_tag(src, alt, title) {
	return `<img src="${µ.encode_XML(src)}" alt="${alt}" title="${title}"/>`
}
/**
 * Demand and read the user file and pass the Reader onload event. 
 * @param {Function} treatment function who treat the evt 
 */
export function file_open_treat(treatment) {
	let input_elt = document.createElement('input')
		input_elt.type = 'file'
		input_elt.click()
		input_elt.addEventListener('change', () => {
			//console.profile('Profile 1')
			document.body.style.cursor = 'wait'
			let file = input_elt.files[0]
			if (file) {
				let reader = new FileReader()
				reader.readAsText(file, 'UTF-8')
				reader.onload = treatment
				reader.onerror = () => console.error('error importing JSON file')
			}
		}, false)
}
/**
 * Fill the url with the given parameter.
 * @param {URL} url The url to fill
 * @param {*} mp_i18n The traduction module (gettext)
 * @returns {URL} The filled url
 */
export async function set_text_params(url, mp_i18n) {
	var params = ''
	var list_p = {
		'q':``,
		'src_type':`${mp_i18n.gettext('Type:')} `,
		'lang':`${mp_i18n.gettext('Language:')} `,
		'res_type':`${mp_i18n.gettext('Result type:')} `,
		'themes':`${mp_i18n.gettext('Themes:')} `,
		'tech':`${mp_i18n.gettext('Technical criterion:')} `,
		'country':`${mp_i18n.gettext('Country:')} `,
		'scope':`${mp_i18n.gettext('Scope:')} `,
		'name':`${mp_i18n.gettext('Cheery-pick sources:')} `
	}
	for(let i of Object.keys(list_p)) {
		if(url.searchParams.get(i) && url.searchParams.get(i) !== '') {
			if (i === 'q') {list_p[i] += url.searchParams.get(i)}
			else {list_p[i] += url.searchParams.getAll(i).join(', ')}
		} else {
			if (i === 'q') {list_p[i] = mp_i18n.gettext('No search terms')}
			else {list_p[i] = ''}
		}
		if(list_p[i] !== '') {
			if(i !== 'q') {params += list_p[i] + '\n'}
		}
	}
	return {list_p, params}
}
/**
 * Initialise the tab div and the interaction with is button.
 * @param {string} id_manager The id of the div who contain the tab
 * @param {Function} treatment The function called when tab change the tab id is passed in parameter 
 */
export async function init_tabs(id_manager, treatment) {
	let manager = document.getElementById(id_manager)
	for (const i of manager.children) {
		i.addEventListener('click', evt => {
			let id = evt.target.getAttribute('id')
			if (typeof(id) === 'undefined')
				throw new Error('no id on tab ' + evt.target.toString())
			manager.setAttribute('tab', id)
			if (evt.target.classList.contains('select'))
				return
			for (const j of manager.children)
				j.classList.remove('select')
			evt.target.classList.add('select')
		})
	}
	let stalker = new MutationObserver(mutation => {
		mutation = mutation[0]
		if (mutation.type === 'attributes' && mutation.attributeName === 'tab')
			treatment(manager.getAttribute('tab'))
	})
	stalker.observe(manager, {attributes: true})
}
/**
 * This function format the sources.json in the given order. Furthermore r_dt_fmt describe all 
 * the r_dt_fmt and all attributes except tags have an _xpath, _attr, _re, _tpl, _com dynamically 
 * added in the function The non found attributes in order are push to the end of the 
 * corresponding src.
 * @function reformat_sources
 * @param {Object} raw_src - The sources.json unbuild Object
 * @param {Array} order - The order of the element in the sources
 * @returns {Object} - The well formated raw sources
 */
export function reformat_sources(raw_src, order) {
	let start = new Date()
	let new_src = {}
	for (const i of Object.keys(raw_src)) {
		// console.group(i)
		new_src[i] = {}
		let raw_i = raw_src[i]
		let new_i = new_src[i]
		for (const attr of order) {
			if (attr.startsWith("tags.")) {
				let tag_name = attr.replace("tags.", "")
				new_i["tags"][tag_name] = raw_i["tags"][tag_name]
			}
			else if (attr === "tags")
				new_i[attr] = {}
			else if (attr === "r_dt_fmt_")
				for (const j of Object.keys(raw_i)) {
					if (j.startsWith("r_dt_fmt_"))
						new_i[j] = raw_i[j]
				}
			else {
				if (raw_i[attr])
					new_i[attr] = raw_i[attr]
				if (raw_i[attr + "_xpath"])
					new_i[attr + "_xpath"] = raw_i[attr + "_xpath"]
				if (raw_i[attr + "_attr"])
					new_i[attr + "_attr"] = raw_i[attr + "_attr"]
				if (raw_i[attr + "_re"])
					new_i[attr + "_re"] = raw_i[attr + "_re"]
				if (raw_i[attr + "_tpl"])
					new_i[attr + "_tpl"] = raw_i[attr + "_tpl"]
				if (raw_i[attr + "_com"])
					new_i[attr + "_com"] = raw_i[attr + "_com"]
			}
		}
		// console.groupEnd()
		for (let j of Object.keys(raw_i)) {
			if (!new_src[j] && j != "tags")
				new_i[j] = raw_i[j]
		}
		for (let j of Object.keys(raw_i["tags"])) {
			if (new_src["tags"] && !new_src["tags"][j])
				new_i["tags"][j] = raw_i["tags"][j]
		}
		// console.log("formant end in ", new Date - start)
	}
	return new_src
}
/**
 * @constant The enumeration of the error type.
 */
export const error_code = {
	ERROR: '0',						// not recognized error
	INVALID_URL: 'url_0',			// regex url
	NETWORK_ERROR: 'url_1',			// network
	NO_RESULT: 'result_0',			// no result
	BAD_VALUE: 'value_0',			// malformed value
	INVALID_VALUE: 'value_1', 		// invalid, no referenced value
	FATHER_NOT_FOUND: 'value_2',	// father (extends) not found
	DEPRECATED_ATTR: 'attr_0',		// attribute deprecated
	NOT_FOUND_ATTR: 'attr_1',		// attribute not found
	CONFLICTS_ATTR: 'attr_2',		// conflict with attributes
	NAME_ALREADY_TAKEN: 'attr_3',	// src name is taken
	RSS_POSSIBLE: 'rss_0',			// If rss is possibly found
}
/**
 * @constant The enumeration of the error status.
 */
export const error_status = {
	ERROR: 'error',
	WARNING: 'warn',
	INFO: 'info',
	LOG: 'log'
}
/**
 * Create an error objected fitted for add_error.
 * @param {string} error_code The code of the error 
 * @param {string} src_name The name of the source 
 * @param {string} text Error description
 * @param {string} level the error level : warn, error
 * @param {Object} other The other attribute data to add to object
 */
export function create_error(error_code, src_name, text, level, caller, other={}) {
	let trace =  new Error()
	let n_error = {
		'code': error_code,
		'src_name': src_name,
		'text': text,
		'level': level,
		'caller': caller,
		'stack_trace': trace.stack
	}
	console[level](src_name, text)
	Object.assign(n_error, other)
	return n_error
}
/**
 * The boolean describe if an source is correct by testing one attributes {@link test_attribute}.
 * @memberof test
 */
var def_attr_is_complete = true
/**
 * The list of the all error found by {@link test_attribute}.
 * @memberof test
 */
var error_attr = []
/**
 * Test if an url of src is correct and reachable.
 * @function test_url
 * @memberof test
 * @param {Object} src - the src to test (for get the name)
 * @param {string} url - url to testget_current_source_search_hosts
 * @param {string} attr_name - name, key of the attribute
 * @param {Object} status - object who contain code in key and true in value {200: true,300: true}
 * @returns none
 */
export function test_url(src, url, attr_name, status) {
	if (typeof src[attr_name] !== 'undefined' && src[attr_name] !== '') {
		let requete = new XMLHttpRequest()
		requete.open('GET', url, false)
		try {
			requete.send()
			if (!status[requete.status] && requete.status !== 304) {
				def_attr_is_complete = true
				error_attr.push(
					create_error(
						error_code.NETWORK_ERROR,
						src.tags.name,
						`${attr_name} ${url} is not correctly receive ${requete.status} code`,
						"warn",
						'test_integrity',
						{
							test: 'url',
							'url': url,
							'status': status.toString()
						}
					)
				)
			}
			return 200
		} catch (exec) {
			console.error(exec)
			def_attr_is_complete = true
			error_attr.push(
				create_error(
					error_code.ERROR,
					src.tags.name,
					`${attr_name} is not correctly receive... ${requete.status} code`,
					"warn",
					'test_integrity',
					{
						test: 'url',
						'url': url,
						'status': status.toString()
					}
				)
			)
		}
	}
	return 
}
/**
 * Test the regex for the attr_name of curr_src.
 * @function test_re
 * @memberof test
 * @param {Object} test_integrity_data - The object who contain the description of the format (test_integrity.json)
 * @param {Object} curr_src - The object who conaint the source
 * @param {string} attr_name - The name, key of the attribute
 * @param {string} regex - The regex to test in a string
 * @returns {Boolean} If the regex match
 */
function test_re(test_integrity_data, curr_src, attr_name, regex) {
	let attr = test_integrity_data[attr_name]
	let error_level = "error"
	if (!regex.test(curr_src[attr_name])) {
		def_attr_is_complete = false
		if (attr["test_level"]) {
			def_attr_is_complete = true
			error_level = attr["test_level"]
		}
		error_attr.push(
			create_error(
				error_code.INVALID_VALUE,
				curr_src.tags.name,
				`${curr_src[attr_name]} is an invalid ${attr_name} (${regex})`,
				error_level,
				'test_integrity',
				{
					test: 'regex',
					'regex': regex.toString(),
					data: curr_src[attr_name]
				}
			)
		)
		return false
	}
	return true
}
/**
 * This function if the attributes is well formed with the given file described format.
 * @function test_attribut
 * @memberof test
 * @param {Object} sources - The sources.json build object
 * @param {Object} test_integrity_data - The test_integrity.json build object
 * @param {Object} src - The source object to test
 * @param {string} attr_name - The name, key of attribute to test
 * @param {boolean} slow - Test with request
 * @param {string} dependent - The string who describe the dependecies of an attributes
 * @returns {Array} Who contain in first a Boolean the validity of the attribute, and the error array [[error, name, level], ...]
 */
export function test_attribute(sources, test_integrity_data, src, attr_name, slow, dependent = 'none') {
	error_attr = []
	def_attr_is_complete = true
	let func_src = src
	let attr = test_integrity_data[attr_name]
	let has_type = false
	let regex = null
	let error_level = "error"
	let test = true
	if (attr_name.startsWith('tags.')) {
		attr_name = attr_name.replace('tags.', '')
		func_src = src["tags"]
	}
	if (!func_src[attr_name] && func_src[attr_name] !== "") {
		// other attributes for replace attr
		if (attr["conflicts"])
			for (let i of attr["conflicts"])
				if (func_src[i])
					return
		def_attr_is_complete = false
		error_attr.push(
			create_error(
				error_code.NOT_FOUND_ATTR,
				src.tags.name,
				`${attr_name} is not present and needed (${dependent} dependencies)`
					+ ` please see doc for further information`,
				"error",
				'test_integrity',
				{
					'test': 'search attribute alternative'
				}
			)
		)
		return [def_attr_is_complete, error_attr]
	}

	error_level = "error"
	if (attr["conflicts"]) {
		for (let i of attr["conflicts"]) {
			let conflict_lvl = src
			if (i.startsWith('.')) {
				i = i.replace('.', '')
				conflict_lvl = conflict_lvl["tags"]
			}
			if (conflict_lvl[i]) {
				def_attr_is_complete = false
				error_attr.push(create_error(
						error_code.CONFLICTS_ATTR,
						src.tags.name
						`${i} is in conflict with attribute ${attr_name}`,
						"error",
						'test_integrity',
						{'test': 'search conflicts'}
					)
				)
				return [def_attr_is_complete, error_attr]
			}
		}
	}
	// value test
	error_level = "error"
	if (attr['test']) {
		if (attr['test_conflict']) {
			for (let i of attr['test_conflict']) {
				if (src[i]) {
					test = false
				}
			}
		}
		if (test)
			for (const type of attr['test']) {
				if (slow && type === 'url') {
					// https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
					regex = new RegExp("https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&\\/\\/=]*)")
					if (!test_re(test_integrity_data, func_src, attr_name, regex))
						return [def_attr_is_complete, error_attr]
					test_url(func_src, func_src[attr_name], attr_name, attr['url_code'])
				}
				else if (type === 'oneofvalues') {
					for (let i of attr['test_oneofvalues']) {
						has_type = func_src[attr_name] === i || has_type
					}
					if (!has_type) {
						def_attr_is_complete = false
						if (attr["test_level"]) {
							def_attr_is_complete = true
							error_level = attr["test_level"]
						}
						error_attr.push(create_error(
								error_code.BAD_VALUE,
								src.tags.name
								`${attr_name} source without correct value : ${src[attr_name]}`,
								error_level,
								'test_integrity',
								{'test': 'oneofvalues', 'attr': attr_name}
							)
						)
						return [def_attr_is_complete, error_attr]
					}
				}
				else if (type === 'values') {
					if (attr['test_values'] !== func_src[attr_name]) {
						def_attr_is_complete = false
						if (attr["test_level"]) {
							def_attr_is_complete = true
							error_level = attr["test_level"]
						}
						error_attr.push(create_error(
								error_code.BAD_VALUE,
								src.tags.name
								`source without correct ${attr_name} : ${src['xml_type']}`,
								error_level,
								'test_integrity',
								{'test': 'values', 'attr': attr_name}
							)
						)
						return [def_attr_is_complete, error_attr]
					}
				}
				else if (type === 'key') {
					if (!attr["test_object"][func_src[attr_name]]) {
						def_attr_is_complete = false
						if (attr["test_level"]) {
							def_attr_is_complete = true
							error_level = attr["test_level"]
						}
						error_attr.push(create_error(
								error_code.BAD_VALUE,
								src.tags.name
								`${attr_name} source without correct value : ${src[attr_name]}`,
								error_level,
								'test_integrity',
								{'test': 'key', 'attr': attr_name}
							)
						)
						return [def_attr_is_complete, error_attr]
					}/*  else if (attr["test_value_object"] && attr["test_object"][func_src[attr_name]] === "error") {
						error_attr.push(create_error(
								error_code.BAD_VALUE,
								src.tags.name
								`${attr_name} is found but the corresponding value `
									+ `is error : ${src[attr_name]}`,
								error_level,
								'test_integrity',
								{'test': 'key', 'attr': attr_name}
							)
						)
						return [def_attr_is_complete, error_attr]
					} */
				}
				else if (type === 're') {
					regex = new RegExp(attr['test_re'])
					if (test_re(test_integrity_data, func_src, attr_name, regex))
						return [def_attr_is_complete, error_attr]
				} else if (type === 'inSource') {
					if (!sources[func_src[attr_name]]) {
						def_attr_is_complete = false
						if (attr["test_level"]) {
							def_attr_is_complete = true
							error_level = attr["test_level"]
						}
						error_attr.push(create_error(
								error_code.FATHER_NOT_FOUND,
								src.tags.name
								`${attr_name} is found but the value is not in sources : ${src[attr_name]}`,
								error_level,
								'test_integrity',
								{'test': 'inSource', 'attr': attr_name}
							)
						)
						return [def_attr_is_complete, error_attr]
					}
				}
			}
	}
	if (attr['dependencies'])
		for (let j of attr.dependencies)
			test_attribute(sources, test_integrity_data, src, j, slow, attr_name)
	return [def_attr_is_complete, error_attr]

}
/**
 * Search in the src_url
 * @param {string} src_url The url of the source
 * @param {string} src_name The name of the source
 * @param {Object} test_integrity_data The test_integrity.json build Object 
 * @returns {Array} Who contain the warn if an rss search exists
 */
export async function search_rss(src_url, src_name, test_integrity_data) {
	let error = []
	const header = new Headers()
	header.append('Content-Type', 'application/rss+xml')
	const init = {
		method: 'GET',
		headers: header,
		mode: 'cors',
		cache: 'default'
	};
	let res
	try {
		for (const i of test_integrity_data["rss_url"]) {
			res = fetch(src_url + i, init).then(
				function (response) {
					if (response.ok) {
						let contentType = response.headers.get("content-type");
						if (contentType && contentType.indexOf("application/rss+xml") !== -1)
							error.push(create_error(
									error_code.RSS_POSSIBLE,
									src.tags.name
									`${src_url + i} can be xml type`,
									'warn',
									'test_integrity',
									{'test': 'search rss'}
								))
					}
				})
			await res
		}
	} catch (exec) {
		console.log(exec + ' rss ' + src_name)
	}
	return error
}
/**
 * Contain the already encounter sources names.
 * @memberof test
 * @type {Object}
 */
var reserved_name = {}
/**
 * reserved_name content is dup and replace by an empty Object.
 */
export function clear_reserved_name() {
	reserved_name = {}
}
/**
 * Add the name to the source name found.
 * @param {string} name The source name
 */
export function add_reserved_name(name) {
	reserved_name[name] = true
}
/**
 * Load the given name sources in reserved_name.
 * @param {Object} src The sources object
 */
export function load_src_in_reserved_name(src) {
	for (const i of Object.keys(src))
		reserved_name[src[i].tags.name] = true
}
/**
 * Check if the name has already been found in the src tested.
 * @param {string} name The name of the source
 * @returns {boolean}
 */
export function test_name_unicity(name) {
	return !reserved_name[name]
}
/**
 * This function tests if the src is correct for the standard given test_integrity_data.
 * @function test_src_integrity
 * @memberof test
 * @param {Object} sources - The sources.json build Object
 * @param {Object} src - The source object to test
 * @param {Object} test_integrity_data - The test_integrity.json build Object 
 * @param {Function} call_back - The callback parameter is the return array if not given return classic
 * @returns {Array} Who contain in first a Boolean the validity of the src, and the error array [[error, name, level], ...]
 */
export async function test_src_integrity(sources, src_key, test_integrity_data, call_back=undefined, slow=false) {
	// let start = new Date()
	let src = sources[src_key]
	let error = []
	let def_src_is_complete = true
	let deprecated = ["r_dt_re"]
	let needed = test_integrity_data["base"]
	let no_css = ["search_url_web"]
	let localization_kit = ["results", "r_h1", "r_h1_xpath", "r_url", "r_url_xpath",
		"r_dt", "r_dt_xpath", "tags.tz"]
	let type = "GLOBAL or "
	for (let i of deprecated) {
		if (src[i]) {
			def_src_is_complete = true
			error.push([`${i} is deprecated please see doc for further information`, src.tags.name, "warn"])
		}
	}
	if (src.type === 'XML'){
		type = type.concat("XML")
		needed = needed.concat(no_css)
		needed = needed.concat(["type", "xml_type"])
	} else if (src.type === 'JSON') {
		type = type.concat("JSON")
		needed = needed.concat(no_css)
		needed = needed.concat(localization_kit)
		needed.push("type")
	} else {
		type = type.concat("CSS")
		needed = needed.concat(localization_kit)
	}

	if (slow && src.type !== "XML") {
		error = error.concat(await search_rss(src_key, src.tags.name, test_integrity_data))
	}

	if (src["method"] === "POST") {
		type = type.concat(" POST")
		needed = needed.concat(["method", "body"])
	}
	
	for (let i of needed) {
		try {
			test_attribute(sources, test_integrity_data, src, i, slow, type)
			def_src_is_complete = def_attr_is_complete && def_src_is_complete
			error = error.concat(error_attr)
		} catch (exec) {
			console.error(i, exec)
		}
	}

	if (test_name_unicity(src.tags.name))
		add_reserved_name(src.tags.name)
	else {
		def_src_is_complete = false
		error.push(create_error(
				error_code.NAME_ALREADY_TAKEN,
				src.tags.name,
				`${src.tags.name} is already used`,
				'error',
				'test_integrity'
			)
		)
	}
	// console.log("tested in ", new Date() - start, " i'm inevitable")
	if (typeof call_back === 'function')
		call_back([def_src_is_complete, error])
	else
		return [def_src_is_complete, error]
}
/**
 * Request the cors permission for the given URLS.
 * @param {Array} get_current_source_hosts The url to get the permission
 * @returns {Promise} The response value contain true or False
 */
export function request_permissions(get_current_source_hosts) {
	return browser.permissions.request(
		{origins: get_current_source_hosts})
}
/**
 * Parse a date from a website in a Date object with the given regex and format.
 * The format is in general years-months-days hours:minutes if the months is not
 * a number but a word use {} to translate to number : $years-{$month}-$days $hours:minutes.
 * Another taken format is $number sec|min|hour|today|day|yesterday|month|year for the date
 * like 5 minute ago.
 * @param {string} dt_str The date to parse
 * @param {string} tz The timezone of the date
 * @param {Array} f_nb The format "$4-{$3}-$2 $1" 
 * @param {Array} n The regex to extract the data
 * @returns {Date}
 */
export function parse_dt_str(dt_str, tz, f_nb, n, month_nb_json) {
	var d = undefined
	var i = 1
	var cur_fmt

	if (cur_fmt = n[`r_dt_fmt_${i}`]) {
		//console.log('before', dt_str)
		do {
			if(typeof(dt_str) === "number")
				dt_str = `${dt_str}`
			dt_str = µ.regextract(cur_fmt[0], dt_str, cur_fmt[1])
			d = sub_parse_dt_str(dt_str, tz, f_nb, month_nb_json)
			i += 1
		} while ((cur_fmt = n[`r_dt_fmt_${i}`]) && isNaN(d))
	} else {
		d = sub_parse_dt_str(dt_str, tz, f_nb)
	}
	if (isNaN(d))
		throw new Error(`'${dt_str}' (${tz}) is an invalid new Date()`)
	// console.log('after', dt_str, 'dt', d.toLocaleString())
	return d
}
/**
 * With an formated date try to create the date object.
 * @param {string} dt_str the date formated
 * @param {string} tz the timezone
 * @param {string} f_nb the format
 * @returns {Date} The foud date
 */
function sub_parse_dt_str(dt_str, tz, f_nb, month_nb_json) {
	// console.log(dt_str)
	var d = undefined
	if (typeof(dt_str) === 'string') // JSON timestamps are integers
		dt_str = dt_str.replace(/{(.*)}/, (_, $1) => month_nb($1, month_nb_json))
	// console.log("month found : ", dt_str)
	if (µ.MM_DD_ONLY_RE.test(dt_str)) {
		let today = µ.timezoned_date('', tz)
		dt_str = `${today.getFullYear()}${dt_str}`
	} else if (µ.HH_MM_ONLY_RE.test(dt_str) ) {
		let today = µ.timezoned_date('', tz)
		dt_str = `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()} ${dt_str}`
	}
	d = µ.timezoned_date(dt_str, tz)
	if(isNaN(d) && /\d{12,20}/i.test(dt_str))
		d = new Date(Number(dt_str))
	if(isNaN(d) && µ.string_includes_array(dt_str,['sec','min','hour','today','day','yesterday','week','month','year'])) {
		d = µ.timezoned_date('', tz)
		let regex_dt = /[^\d]*(\d+)[^\d]*/
		if (/sec/i.test(dt_str)) {
			d.setSeconds(d.getMinutes() - Number(dt_str.replace(/sec?.*/, '')))
		} else if (/min/i.test(dt_str)) {
			d.setMinutes(d.getMinutes() - Number(dt_str.replace(/min?.*/, '')))
		} else if (/hour/i.test(dt_str)) {
			d.setHours(d.getHours() - Number(dt_str.replace(/hour/, '')))
		} else if (/day/i.test(dt_str) ) {
			d.setDate(d.getDate() - Number(dt_str.replace(regex_dt, '$1')))
		} else if (/today/i.test(dt_str)) {
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/yesterday/i.test(dt_str)) {
			d.setDate(d.getDate() - 1)
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/week/i.test(dt_str) ) {
			d.setDate(dt.getDate() - (Number(dt_str.replace(regex_dt, '$1')) * 7))
		} else if (/month/i.test(dt_str) ) {
			d.setMonth(d.getMonth() - Number(dt_str.replace(regex_dt, '$1')))
		} else if (/year/i.test(dt_str) ) {
			d.setFullYear(d.getFullYear() - Number(dt_str.replace(regex_dt, '$1')))
		}
	}
	if (d && !d.getMinutes() && !d.getSeconds()) {  // no time set ? put an ordered one
		if (µ.HH_MM_RE.test(dt_str)) {
			let d_split = µ.regextract(µ.HH_MM_STR, dt_str).split(':')
			d.setHours(Number(d_split[0]))
			d.setMinutes(Number(d_split[1]))
		} else {
			d.setHours(0)
			d.setMinutes(f_nb % 60)
		}
	}
	return d
}
/**
 * Create a style link with href or a style node.
 * https://stackoverflow.com/a/524798
 * @param {string} href The stylesheet to reach if undefined create style
 * @returns {StyleSheet} The created or loaded stylesheet
 */
export function createStyleSheet(href) {
	if(typeof href !== 'undefined') {
		var element = document.createElement('link');
		element.type = 'text/css';
		element.rel = 'stylesheet';
		element.href = href;
	}
	else
		var element = document.createElement('style');
	// element.setAttribute('generated', true)
	document.getElementsByTagName('head')[0].appendChild(element);
	var sheet = document.styleSheets[document.styleSheets.length - 1];
	if(typeof sheet.addRule === 'undefined')
		sheet.addRule = addRule;
	if(typeof sheet.removeRule === 'undefined')
		sheet.removeRule = sheet.deleteRule;
	return sheet;
}