import sys, os, json

JSONFILE__RESULT = "json/lang_names.json"
LOST_FILE = "lost_lang_code.log"

def parse():
    with open(JSONFILE__RESULT, "r", encoding="utf-8") as f:
        parsed_json = json.loads(f.read())

    with open(ISOFILE_TO_PARSE, "r", encoding="utf-8") as f:
        file_str = f.read()

    lines = file_str.split("\n")
    lines.pop(0)
    for i in lines:
        data = i.split("\t")
        if not data[0] in parsed_json:
            parsed_json[data[0]] = data[6]
        
        if parsed_json[data[0]] == "error":
            parsed_json[data[0]] = data[6]

        if data[3] != '' and not data[3] in parsed_json:
            parsed_json[data[3]] = data[6]

    remove = ""
    for j in parsed_json:
        old = False
        for i in lines:
            if j in i[0] or j in i[3]:
                old = True

        if not old:
            remove += j + parsed_json[j] + " has been removed\n"
            del parsed_json[j]

    with open(JSONFILE__RESULT, "w") as f:
        f.write(json.dumps(parsed_json, indent="\t"))
    
    if remove != "":
        with open(LOST_FILE, "w") as f:
            f.write(remove)

if __name__ == '__main__':
    if len(sys.argv) <= 1:
        raise Exception("Error : Need file in argument")
    ISOFILE_TO_PARSE = sys.argv[1]
    parse()